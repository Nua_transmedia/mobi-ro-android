package com.myappbuilder.wgstream;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by nua-android on 30/5/16.
 * This code and all components (c) Copyright 2016-2017, NuaTransMedia,. All rights reserved.
 */

public class DataBaseHelper extends SQLiteOpenHelper
{
    public DataBaseHelper(Context context, String name, CursorFactory factory, int version)
    {
        super(context, name, factory, version);
    }
    // Called when no database exists in disk and the helper class needs
    // to create a new one.
    @Override
    public void onCreate(SQLiteDatabase _db)
    {
        _db.execSQL(LoginDataBaseAdapter.DATABASE_CREATE);
        _db.execSQL(LoginDataBaseAdapter.DATABASE_CREATE_CUSTOM);


    }
    // Called when there is a database version mismatch meaning that the version
    // of the database on disk needs to be upgraded to the current version.
    @Override
    public void onUpgrade(SQLiteDatabase _db, int _oldVersion, int _newVersion)
    {

        _db.execSQL("DROP TABLE IF EXISTS"+ " CUSTOMPROFILE");
        _db.execSQL("DROP TABLE IF EXISTS"+ " CATEGORY");

        onCreate(_db);
    }

}