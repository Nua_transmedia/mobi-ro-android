package com.myappbuilder.wgstream;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import db.TagsManager;
import me.gujun.android.taggroup.TagGroup;

/**
 * Created by nua-android on 30/5/16.
 * This code and all components (c) Copyright 2016-2017, NuaTransMedia,. All rights reserved.
 */
public class Videoedit extends Activity implements View.OnClickListener {

    private EditText title,description,tag;
    private String vidid,vidtitle,viddesc,vidtag,vidtype;
    private Spinner spinner;


    private RelativeLayout tag_cntin;
    String[] category = {

            "Movie",
            "Music",
                    "Serial",
            "Show",
            "Sports"


    };

    private ProgressDialog prg_bar;

    private TagsManager mTagsManager;

    private Button save,cancel,back_edit;
    private TagGroup mTagGroup;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.videoedit);

        findViewById();
        declaration();






        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, R.layout.spinnertext, category);


        Intent intent = getIntent();
        vidid = intent.getExtras().getString("videoid");
        vidtitle=intent.getExtras().getString("vidtitle");
        viddesc=intent.getExtras().getString("viddesc");
        vidtag=intent.getExtras().getString("vidtag");
        vidtype=intent.getExtras().getString("vidtype");

        title.setText(vidtitle);
        description.setText(viddesc);
        tag.setText(vidtag);
        tag.setVisibility(View.GONE);

        //  title.setFocusable(true);
      /*  title.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);


        title.setSelected(true);*/



        mTagsManager = TagsManager.getInstance(getApplicationContext());
        String[] tags = mTagsManager.getTags();


        mTagGroup = (TagGroup) findViewById(R.id.tag_group);
        mTagGroup.setTags(tags);

        mTagGroup.setFocusable(false);
        mTagGroup.setFocusableInTouchMode(false);
        mTagGroup.setSelected(false);
        mTagGroup.setEnabled(false);

        mTagGroup.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                mTagGroup.setFocusable(false);
                mTagGroup.setFocusableInTouchMode(false);
            }
        });

        // mTagGroup.setFocusable(false);
        //  mTagGroup.setFocusableInTouchMode(true);



        int counter = 0;
        int arraycount=0;
        int temp=0;

        if(vidtag.contains(",")) {

            // int occurance = StringUtils.countOccurrencesOf("a.b.c.d", ".");
            int count= (vidtag.split(",").length - 1);

            String[]  array = vidtag.split(",");


            for(int i=0;i<array.length;i++)
            {
                System.out.println(array[i]);
                Log.d("values"+String.valueOf(i),String.valueOf(array[i]));
            }



            Log.d("camacount",String.valueOf(count));
            Log.d("arraycount",String.valueOf(arraycount));
            Log.d("temp", String.valueOf(temp));
            mTagGroup.setTags(array);

        }else{
            if(vidtag.length()!=0) {
                if(vidtag.contentEquals("(null)")){

                }else {
                    mTagGroup.setTags(vidtag);
                }
            }else{

            }
        }

        tag_cntin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTagGroup.submitTag();
            }
        });

        mTagGroup.setFocusable(false);


        title.callOnClick();
        title.performClick();






        /*cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //   Constant.hideSoftKeyboard(Videoedit.this, v);
                //Videoedit.this.finish();
                finish();
                *//*prg_bar.show();
                Constant.hideSoftKeyboard(Videoedit.this,v);
                Constant.cd = new ConnectionDetector(getApplicationContext());
                Constant.isInternetPresent = Constant.cd.isConnectingToInternet();
                if (Constant.isInternetPresent) {
                    getvalues();
                }else{
                    finish();
                }*//*
            }
        });
*/
        prg_bar = new ProgressDialog(Videoedit.this);
        prg_bar.setMessage("Please Wait");
        prg_bar.setIndeterminate(true);
        prg_bar.setCancelable(false);
        prg_bar.setMax(100);
        prg_bar.setProgressStyle(ProgressDialog.STYLE_SPINNER);


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Vedio_list.getInstance().finish();
                String newtitle,newdesc;

                mTagGroup.submitTag();

                newtitle=title.getText().toString();

                if(newtitle.length()==0){
                    newtitle=vidtitle;
                }

                newdesc=description.getText().toString();
                if(newdesc.length()==0){
                    newdesc=viddesc;
                }
                String newtag=tag.getText().toString();

                String[] tags= mTagGroup.getTags();

                Log.d("taglength",String.valueOf(tags.length));
                String finalvalue="";
                for(int it=0;it<=tags.length-1;it++){

                    String tagvalue="";

                    if(finalvalue.length()==0){
                        tagvalue=String.valueOf(tags[it]);
                    }else{
                        tagvalue=finalvalue+","+String.valueOf(tags[it]);
                    }

                    finalvalue=tagvalue;

                    Log.d("finalvalue",finalvalue);

                }

                Log.d("tagfinal", finalvalue);

                //String finalvalue=String.valueOf(tags[0]);
                if(finalvalue.length()!=0) {
                    newtag = finalvalue;
                }else{
                    newtag="";
                }

                //  Log.d("tagvalue",tagvalue);

                /*if(newtitle.length()!=0){
                    if(newdesc.length()!=0){
                        if(newtag.length()!=0){*/
                //  sendedited(newtitle,newdesc,newtag,vidid,vidtitle,viddesc,vidtag);
                Constant.cd = new ConnectionDetector(getApplicationContext());
                Constant.isInternetPresent = Constant.cd.isConnectingToInternet();
                if (Constant.isInternetPresent) {
                    sending(newtitle, newdesc, newtag, vidid, vidtitle, viddesc, vidtag);
                }else{
                    toastsettext("No Internet Connection");
                }


                        /*}else{

                            toastsettext("Please enter Tag");
                         //   Toast.makeText(getApplicationContext(),"Please enter Tag",Toast.LENGTH_SHORT).show();
                        }

                    }else{
                        toastsettext("Please enter description");
                       // Toast.makeText(getApplicationContext(),"Please enter description",Toast.LENGTH_SHORT).show();

                    }

                }else{

                    toastsettext("Please enter Title");
                  //  Toast.makeText(getApplicationContext(),"Please enter Title",Toast.LENGTH_SHORT).show();

                }*/

            }
        });




        back_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Constant.hideSoftKeyboard(Videoedit.this, v);

                //Videoedit.this.finish();
                finish();

                /*Constant.cd = new ConnectionDetector(getApplicationContext());
                Constant.isInternetPresent = Constant.cd.isConnectingToInternet();
                if (Constant.isInternetPresent) {
                    prg_bar.show();
                    getvalues();
                }else{
                    finish();
                }*/
               /* Intent intent = new Intent(Videoedit.this, Vedio_list.class);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                System.exit(0);
                finish();*/
                //Videoedit.this.finish();

            }
        });



        spinner.setBackgroundResource(R.drawable.btn_dropdown);

        spinner.setAdapter(adapter);

        if(vidtype.length()!=0){
            if(vidtype.contentEquals("Movie")){
                spinner.setSelection(0);
            }else if(vidtype.contentEquals("Show")){
                spinner.setSelection(3);
            }else if(vidtype.contentEquals("Sports")){
                spinner.setSelection(4);
            }else if(vidtype.contentEquals("Serial")){
                spinner.setSelection(2);
            }else if(vidtype.contentEquals("Music")){
                spinner.setSelection(1);
            }else{
                spinner.setSelection(0);
            }

        }else{
            spinner.setSelection(0);
        }



        spinner.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> arg0, View arg1,
                                               int arg2, long arg3) {

                        int position = spinner.getSelectedItemPosition();
                        //  Toast.makeText(getApplicationContext(), "You have selected " + category[+position], Toast.LENGTH_LONG).show();

                        if (position == 0) {
                            Constant.category=category[0];
                        } else if (position == 1) {
                            Constant.category=category[1];
                        } else if (position == 2) {
                            Constant.category=category[2];
                        } else if (position == 3) {
                            Constant.category=category[3];
                        } else if (position == 4) {
                            Constant.category=category[4];
                        }

                        // TODO Auto-generated method stub
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {
                        // TODO Auto-generated method stub

                    }

                }
        );


    }

    private void declaration() {
        title.setOnClickListener(this);
    }


    public void toastsettext(String string1) {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.altdialog_toast,
                (ViewGroup) findViewById(R.id.toast_rl));
        TextView txt = (TextView) layout.findViewById(R.id.toast_txt);
        txt.setText(string1);
        Toast tst = new Toast(getApplicationContext());
        tst.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        tst.setDuration(Toast.LENGTH_SHORT);
        tst.setView(layout);
        tst.show();
    }


    private void getvalues() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    WebClient webClient = new WebClient();
                    String getRes = null;
                    try {
                        getRes = webClient.doGetReq("https://u2mrf96skg.execute-api.ap-southeast-1.amazonaws.com/mobiro/videolist?fname="+Constant.username);
                        Log.d("Home_page_Array :", getRes);

                        Log.e("post response", getRes);


                        try {
                            JSONArray respons = new JSONArray(getRes);


                            for (int i = 0; i < respons.length(); i++) {
                                HashMap<String, String> map = new HashMap<String, String>();
                                JSONObject jsonobject = respons.getJSONObject(i);
                                String name = jsonobject.getString("name");
                                String url = jsonobject.getString("stream_url");
                                String bitrate = jsonobject.getString("bit_rate");
                                String ratio = jsonobject.getString("aspect_ratio");
                                String thumb = jsonobject.getString("thumb_loc");
                                String videoid = jsonobject.getString("video_id");
                                String timevalue=jsonobject.getString("time_showing");

                                String date=jsonobject.getString("created_at");

                                String size=jsonobject.getString("size");
                                String duration=jsonobject.getString("duration");
                                String longitudee=jsonobject.getString("longitude");
                                String latitudee=jsonobject.getString("latitude");

                                String videotitle=null;
                                String videodescription=null;
                                String videotype=null;
                                String videotag="";

                                if(jsonobject.has("Tags")){
                                    videotag=jsonobject.getString("Tags");
                                }

                                if(jsonobject.has("Title"))
                                {
                                    videotitle=jsonobject.getString("Title");
                                }

                                if(jsonobject.has("Description"))
                                {
                                    videodescription = jsonobject.getString("Description");
                                }

                                if(jsonobject.has("VideoType"))
                                {
                                    videotype=jsonobject.getString("VideoType");
                                }




                                //  String videodescription=jsonobject.getString("Description");
                                // String videotag=jsonobject.getString("VideoType");
                                map.put("videoid",videoid);
                                map.put("thumb",thumb);
                                map.put("name",name);
                                map.put("vidurl",url);
                                map.put("time",timevalue);
                                map.put("date",date);

                                map.put("vidsize",size);
                                map.put("viddur",duration);
                                map.put("vidlat",latitudee);
                                map.put("vidlong",longitudee);

                                map.put("vidtitle",videotitle);
                                map.put("viddesc",videodescription);
                                map.put("vidtype",videotype);
                                map.put("vidtag",videotag);

                                Constant.videolist.add(map);


                                //Dataobject obj = new Dataobject(usernme, usrphone, userlt, userid, userproimg, pincode, "");
                                // Constant.results.add(obj);

                                // Toast.makeText(getApplicationContext(), "Data Found", Toast.LENGTH_LONG).show();
                                // mAdapter.notifyDataSetChanged();
                            }

                            // setList();
                            Intent inten=new Intent(getApplicationContext(),Vedio_list.class);
                            startActivity(inten);
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    if(prg_bar.isShowing()) {
                                        prg_bar.dismiss();
                                        prg_bar.cancel();
                                    }else{

                                    }
                                }
                            });
                            Videoedit.this.finish();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                } catch (Exception ex) {
                    ex.printStackTrace();

                }
            }
        }).start();


    }

    private void findViewById() {

        title=(EditText) findViewById(R.id.title_edit);
        description=(EditText) findViewById(R.id.desc_edit);
        tag=(EditText) findViewById(R.id.tag_edit);
        save=(Button) findViewById(R.id.save_btn);
       // cancel=(Button) findViewById(R.id.btn_cancel);

        back_edit=(Button) findViewById(R.id.back_edit);
        spinner=(Spinner) findViewById(R.id.spinner);
        tag_cntin=(RelativeLayout) findViewById(R.id.tagcntin);

    }


    /*@Override
    public void onBackPressed() {
        super.onBackPressed();

        *//*Constant.cd = new ConnectionDetector(getApplicationContext());
        Constant.isInternetPresent = Constant.cd.isConnectingToInternet();
        if (Constant.isInternetPresent) {
            //prg_bar.show();
            getvalues();
        }else{
            finish();
        }*//*

        finish();


        *//*Intent intent = new Intent(Videoedit.this, Vedio_list.class);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        System.exit(0);*//*
        //finish();

       *//* String newtitle=title.getText().toString();
        String newdesc=description.getText().toString();
        String newtag=tag.getText().toString();

        if(newtitle!=null){
            if(newdesc!=null){
                if(newtag!=null){
                    sendedited(newtitle,newdesc,newtag,vidid,vidtitle,viddesc,vidtag);


                }else{
                    Toast.makeText(getApplicationContext(),"Please enter Tag value",Toast.LENGTH_SHORT).show();
                }

            }else{
                Toast.makeText(getApplicationContext(),"Please enter description",Toast.LENGTH_SHORT).show();

            }

        }else{
            Toast.makeText(getApplicationContext(),"Please enter Title",Toast.LENGTH_SHORT).show();

        }*//*


    }*/


    private void sending(final String newtitle, final String newdesc, final String newtag, final String vidid, String titletxt, String desctxt, String tagtxt){

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    WebClient webClient = new WebClient();
                    String getRes = null;
                    try {
                        String urls="https://u2mrf96skg.execute-api.ap-southeast-1.amazonaws.com/mobiro/customupdate?vid="+vidid+"&metadataname1=Title&metadatavalue1="+newtitle+"&metadataname2=Description&metadatavalue2="+newdesc+"&metadataname3=VideoType&metadatavalue3="+Constant.category+"&metadataname4=Tags&metadatavalue4="+newtag;
                        getRes = webClient.doGetReq(urls);
                        Log.d("Home_page_Array :", getRes);

                        Log.e("post response send", getRes);

                        runOnUiThread(new Runnable() {
                            public void run() {
                                prg_bar.show();
                            }
                        });

                        Constant.videolist.clear();
                        getvalues();






                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Log.d("namadhu top Size : ", "" + Constant.namadhu_top_news_Array.size());


                } catch (Exception ex) {
                    ex.printStackTrace();

                }
            }
        }).start();


    }


    private void sendedited(String newtitle, String newdesc, String newtag, String vidid, String titletxt, String desctxt, String tagtxt) {
        AsyncHttpClient clien = new AsyncHttpClient();
        String url="https://u2mrf96skg.execute-api.ap-southeast-1.amazonaws.com/mobiro/customupdate?vid="+vidid+"&metadataname1=Title&metadatavalue1="+newtitle+"&metadataname2=Description&metadatavalue2="+newdesc+"&metadataname3=VideoType&metadatavalue3=music&metadataname4="+newtag;

        Log.d("vidurl",url);
        clien.get(url, new AsyncHttpResponseHandler()

        {


            @Override
            public void onSuccess(String arg0) {
                super.onSuccess(arg0);


                Log.e("post response", arg0);


                Toast.makeText(getApplicationContext(),"Edited Succes fully",Toast.LENGTH_SHORT).show();





            }


            @Override
            public void onFinish() {
                super.onFinish();
                //-- Toast.makeText(getApplicationContext(), "finish", Toast.LENGTH_SHORT).show();
                Log.d("post response", "Finnish");
                Intent intent = new Intent(Videoedit.this, Vedio_list.class);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                System.exit(0);
                finish();

            }

            @Override
            public void onFailure(Throwable arg0, String arg1) {
                super.onFailure(arg0, arg1);
                //Toast.makeText(getApplicationContext(), "failure", Toast.LENGTH_SHORT).show();
                Log.d("post response", "failure");
            }

        });


    }

    @Override
    public void onClick(View v) {
        if(v==title){
            title.setFocusable(true);
            title.requestFocus();
        }
    }
}
