package com.myappbuilder.wgstream;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by nua-android on 30/5/16.
 * This code and all components (c) Copyright 2016-2017, NuaTransMedia,. All rights reserved.
 */
public class Neweditprofile extends Activity{

    private EditText profile_name,sample_rate,audio_bit;/*,custom_width,custom_height,video_bit,frame_rate,keyframe,*/
    private Spinner audiotype;

    private Spinner custom_width,custom_height,video_bit,frame_rate,keyframe;
    private Button add,cancel,back_btn;

    private ProgressDialog prg_bar;

    private NumberPicker frameratepicker,keypicker;

    public static Typeface type;

    private TextView titletxt,resolutiontxt,videobittxt,frameratetxt,keyframetxt;

    private String profilename,width,height,videobitrate,framerate,keyframeinterval,samplerate,audiovalue,audiobitrate;

    private LoginDataBaseAdapter loginDataBaseAdapter;

    String[] audio = {
            "Mono",
            "Stereo"
    };
    String[] widthvalues = {
            "160x120",
            "176x144","320x240","352x288","640x360","720x480","800x600","800x480","864x480","960x960","1088x1088","1280x960",
            "1280x720","1440x1080","1920x1080"

    };

    String[] heightvalues = {
            "Mono",
            "Stereo"

    };

    String[] bitratevalues = {
            "1000 (1 kbps)",
            "1500 (1.5 kbps)"

    };

    String[] frameratevalues = {
            /*"1","2","3","4","5","6","7","8","9","10",
            "11","12","13","14",*/"15","16","17","18","19","20",
            "21","22","23","24","25","26","27","28","29","30"

    };

    String[] keyintervals = {
           /* "1","2","3","4","5","6","7","8","9","10",
            "11","12","13","14",*/"15","16","17","18","19","20",
            "21","22","23","24","25","26","27","28","29","30"

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.neweditprofile);
        findViewbyid();

        loginDataBaseAdapter = new LoginDataBaseAdapter(this);
        loginDataBaseAdapter = loginDataBaseAdapter.open();


        profile_name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                Constant.hideSoftKeyboard(Neweditprofile.this,v);
                profile_name.setFocusable(false);
                profile_name.setFocusableInTouchMode(false);
            }
        });


        custom_width.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                Constant.hideSoftKeyboard(Neweditprofile.this,v);
                custom_width.setFocusable(false);
                custom_width.setFocusableInTouchMode(false);
            }
        });


        custom_width.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                Constant.hideSoftKeyboard(Neweditprofile.this,v);
                /*InputMethodManager imm=(InputMethodManager)getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(profile_name.getWindowToken(), 0);*/
                return false;
            }
        }) ;

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, R.layout.spinnertext, audio);

        audiotype.setBackgroundResource(R.drawable.btn_dropdown);

        audiotype.setAdapter(adapter);

        ArrayAdapter<String> widthadapter = new ArrayAdapter<String>(
                this, R.layout.spinnertext, widthvalues);

        custom_width.setBackgroundResource(R.drawable.btn_dropdown);

        custom_width.setAdapter(widthadapter);

        ArrayAdapter<String> bitadapter = new ArrayAdapter<String>(
                this, R.layout.spinnertext, bitratevalues);

        video_bit.setBackgroundResource(R.drawable.btn_dropdown);

        video_bit.setAdapter(bitadapter);

        ArrayAdapter<String> frameadapter = new ArrayAdapter<String>(
                this, R.layout.spinnertext, frameratevalues);

        frame_rate.setBackgroundResource(R.drawable.btn_dropdown);

        frame_rate.setAdapter(frameadapter);

        ArrayAdapter<String> keyadapter = new ArrayAdapter<String>(
                this, R.layout.spinnertext, keyintervals);

        keyframe.setBackgroundResource(R.drawable.btn_dropdown);

        keyframe.setAdapter(keyadapter);


        prg_bar = new ProgressDialog(Neweditprofile.this);
        prg_bar.setMessage("Please Wait");
        prg_bar.setIndeterminate(true);
        prg_bar.setCancelable(false);
        prg_bar.setMax(100);
        prg_bar.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        String values= String.valueOf(loginDataBaseAdapter.getprofilename("5"));

        if(loginDataBaseAdapter.getcountofval(6)>0) {
            //   Toast.makeText(getApplicationContext(), String.valueOf(loginDataBaseAdapter.getcountofval(6))+values, Toast.LENGTH_SHORT).show();
        }else{

        }



        frameratepicker.setMinValue(1);
        frameratepicker.setMaxValue(30);
        frameratepicker.setWrapSelectorWheel(true);

        keypicker.setMinValue(1);
        keypicker.setMaxValue(30);
        keypicker.setWrapSelectorWheel(true);


        frameratepicker.setOnValueChangedListener( new NumberPicker.
                OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int
                    oldVal, int newVal) {
               /* numberView.setText("Selected number is "+
                        newVal);*/
            }
        });


        frameratepicker.setOnValueChangedListener( new NumberPicker.
                OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int
                    oldVal, int newVal) {
               /* numberView.setText("Selected number is "+
                        newVal);*/
            }
        });

        type = Typeface.createFromAsset(getAssets(),"fonts/Trebuchet MS.ttf");
        titletxt.setTypeface(type);
        resolutiontxt.setTypeface(type);
        videobittxt.setTypeface(type);
        frameratetxt.setTypeface(type);
        keyframetxt.setTypeface(type);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Neweditprofile.this.finish();
               /* Intent inten=new Intent(getApplicationContext(),Livestream.class);
                startActivity(inten);*/
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                profilename=profile_name.getText().toString();
              String widthheight=widthvalues[custom_width.getSelectedItemPosition()];

                String[] separated = widthheight.split("x");



                width=separated[0];//custom_width.getText().toString();
                height=separated[1];//custom_height.getText().toString();
                String videobit=bitratevalues[video_bit.getSelectedItemPosition()];
                String[] separatedd = videobit.split(" ");
                //video_bit.getText().toString();
                videobitrate=separatedd[0];
                framerate=frameratevalues[frame_rate.getSelectedItemPosition()];//frame_rate.getText().toString();
                keyframeinterval=keyintervals[keyframe.getSelectedItemPosition()];//keyframe.getText().toString();
                /*samplerate=sample_rate.getText().toString();
                audiobitrate= audio_bit.getText().toString();
                audiovalue="sterio";*/

                samplerate="41000";
                audiobitrate= "64000";
                audiovalue="sterio";


                Log.d("valuesadded",width+height+videobitrate+framerate+keyframeinterval);

                if(profilename.length()!=0){
                    if(width.length()!=0){

                        if(height.length()!=0){

                            if(videobitrate.length()!=0){

                                if(framerate.length()!=0){

                                    if(keyframeinterval.length()!=0){


                                        loginDataBaseAdapter = new LoginDataBaseAdapter(Neweditprofile.this);
                                        loginDataBaseAdapter = loginDataBaseAdapter.open();


                                        loginDataBaseAdapter.insertCustomprof(1,profilename,width,height,videobitrate,framerate,keyframeinterval,samplerate,audiovalue,audiobitrate);

                                        toastsettext("Profile Added");
                                        Neweditprofile.this.finish();
                                       /* Intent inten=new Intent(getApplicationContext(),Livestream.class);
                                        startActivity(inten);*/


                                    }else{
                                        toastsettext("Please enter Key Frame Interval");
                                    }

                                }else{
                                    toastsettext("Please enter Framerate");
                                }

                            }else{
                                toastsettext("Please enter Video bitrate");
                            }

                        }else{
                            toastsettext("Please enter Height");
                        }

                    }else{
                        toastsettext("Please enter Width");
                    }

                }else {
                    toastsettext("Please enter Profile name");
                }


            }
        });

      /*  cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });*/






    }


    @Override
    public void onBackPressed() {
        Neweditprofile.this.finish();
        /*Intent inten=new Intent(getApplicationContext(),Livestream.class);
        startActivity(inten);*/
    }

    public void toastsettext(String string1) {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.altdialog_toast,
                (ViewGroup) findViewById(R.id.toast_rl));
        TextView txt = (TextView) layout.findViewById(R.id.toast_txt);
        txt.setText(string1);
        Toast tst = new Toast(getApplicationContext());
        tst.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        tst.setDuration(Toast.LENGTH_SHORT);
        tst.setView(layout);
        tst.show();
    }


    private void findViewbyid() {
        profile_name=(EditText) findViewById(R.id.profilename_edit);
        custom_width=(Spinner) findViewById(R.id.slct_width);
        custom_height=(Spinner) findViewById(R.id.slct_height);
        video_bit=(Spinner) findViewById(R.id.slct_bitrate);
        frame_rate=(Spinner) findViewById(R.id.slct_framerate);
        keyframe=(Spinner) findViewById(R.id.key_frameinterval);
        sample_rate=(EditText) findViewById(R.id.samplerate_edit);
        audio_bit=(EditText) findViewById(R.id.audiobitrate_edit);
        audiotype=(Spinner) findViewById(R.id.spinner_sterip);
        back_btn=(Button) findViewById(R.id.back_edit);

        frameratepicker=(NumberPicker) findViewById(R.id.numberPicker1);
        keypicker=(NumberPicker) findViewById(R.id.numberPicker2);

        add=(Button) findViewById(R.id.save_btn);
        titletxt=(TextView) findViewById(R.id.prof_title);
        resolutiontxt=(TextView) findViewById(R.id.prof_wh);
        videobittxt=(TextView) findViewById(R.id.prof_vbr);
        frameratetxt=(TextView) findViewById(R.id.prof_fr);
        keyframetxt=(TextView) findViewById(R.id.prof_kfi);




        //  cancel=(Button) findViewById(R.id.btn_cancel);
    }
}
