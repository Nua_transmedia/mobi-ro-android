package com.myappbuilder.wgstream;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.HashMap;

/**
 * Created by nua-android on 30/5/16.
 * This code and all components (c) Copyright 2016-2017, NuaTransMedia,. All rights reserved.
 */


public class LoginDataBaseAdapter
{
    static final String DATABASE_NAME = "Mobirocustom.db";
    static final int DATABASE_VERSION = 1;
    public static final int NAME_COLUMN = 1;
    // TODO: Create public field for each column in your table.
    // SQL Statement to create a new database.
    static final String DATABASE_CREATE = "create table "+"CUSTOMPROFILE"+
            "( "+"ID"+" INTEGER PRIMARY KEY,"+"PROFILEID"+" INTEGER,"+"PROFILENAME"+" TEXT NOT NULL,"+"WIDTH"+"  TEXT NOT NULL,"+"HEIGHT"+" TEXT NOT NULL,"+"BITRATE"+" TEXT NOT NULL,"+"FRAMERATE"+" TEXT NOT NULL,"+"KEYINTERVAL"+" TEXT NOT NULL,"+"SAMPLERATE" + " TEXT NOT NULL,"+"AUDIOTYPE" + " TEXT NOT NULL,"+"AUDIOBITRATE" + " TEXT NOT NULL); ";

    static final String DATABASE_CREATE_CUSTOM = "create table "+"CATEGORY"+
            "( " +"ID"+" INTEGER PRIMARY KEY,"+"CATEGORYNAME"+"  TEXT NOT NULL,"+"CATEGORY_IMAGE" + " BLOB); ";



    // Variable to hold the database instance
    public SQLiteDatabase db;
    // Context of the application using the database.
    private final Context context;
    // Database open/upgrade helper
    private DataBaseHelper dbHelper;
    public  LoginDataBaseAdapter(Context _context)
    {
        context = _context;
        dbHelper = new DataBaseHelper(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public  LoginDataBaseAdapter open() throws SQLException
    {
        db = dbHelper.getWritableDatabase();
        return this;
    }
    public void close()
    {
        db.close();
    }

    public SQLiteDatabase getDatabaseInstance()
    {
        return db;
    }


    public void insertCustomprof(int i, String profilename, String width, String height, String videobit, String framerate, String keyframe, String samplerate, String sterio, String audiobit){
        ContentValues updatedValues=new ContentValues();

        updatedValues.put("PROFILEID",1);
        updatedValues.put("PROFILENAME",profilename);
        updatedValues.put("WIDTH",width);
        updatedValues.put("HEIGHT",height);
        updatedValues.put("BITRATE",videobit);
        updatedValues.put("FRAMERATE",framerate);
        updatedValues.put("KEYINTERVAL",keyframe);
        updatedValues.put("SAMPLERATE", samplerate);
        updatedValues.put("AUDIOTYPE",sterio);
        updatedValues.put("AUDIOBITRATE",audiobit);

        // "INSERT INTO table1 (field1, field2) " +
        //  "VALUES (" + field_one + ", " + field_two + ")";

        db.insert("CUSTOMPROFILE", null, updatedValues);
    }








    public String getallids() {



        String selectQuery = "SELECT * FROM `CUSTOMPROFILE` ";
        Cursor c = db.rawQuery(selectQuery, new String[]{});
        HashMap<String, String> map = null;
        Constant.New_database.clear();
        String width = null, name = null, height = null, bitrate = null, framerate = null, samplerate = null, keyframe = null, description = null, category = null, time = null;
        if (c.moveToFirst()) {


            do {
                //newsid = c.getString(c.getColumnIndex("newsid"));
                //Toast.makeText(context, "retrive value : "+newsid, Toast.LENGTH_LONG).show();
                name = c.getString(c.getColumnIndex("PROFILENAME"));
                width = c.getString(c.getColumnIndex("WIDTH"));
                height = c.getString(c.getColumnIndex("HEIGHT"));
                bitrate = c.getString(c.getColumnIndex("BITRATE"));
                framerate = c.getString(c.getColumnIndex("FRAMERATE"));
                samplerate = c.getString(c.getColumnIndex("SAMPLERATE"));
                keyframe = c.getString(c.getColumnIndex("KEYINTERVAL"));


                map = new HashMap<String, String>();

                map.put("name", name);
                map.put("width", width);
                map.put("height", height);
                map.put("bitrate", bitrate);
                map.put("framerate", framerate);
                map.put("samplerate", samplerate);
                map.put("keyframe", keyframe);

                Constant.New_database.add(map);
            /* temp_news = c.getString(c.getColumnIndex("NEWSID"));
             map.put("catid",temp_news);
             Constant.othernewsid.add(map);*/

            } while (c.moveToNext());


        }
        c.close();
        return null;
    }






    public int getcountofval(int id){
        Cursor mCount= db.rawQuery("select count(*) from CUSTOMPROFILE", null);
        mCount.moveToFirst();
        int count= mCount.getInt(0);
        mCount.close();

        return  count;
    }









    public int get(){
        Cursor curse= db.rawQuery("SELECT * FROM"+ " AXIS"+ " WHERE"+ " ID="+3,null);
        if(curse.getCount()<1) // UserName Not Exist
        {
            curse.close();

        }
        curse.moveToFirst();
        int same=curse.getInt(curse.getColumnIndex("SAME"));
        curse.close();
        return same;
    }









    public String getSinlgeEntryIcici1_same()
    {
        Cursor cursor=db.query("ICICI1", null, " ID=1", new String[]{}, null, null, null);
        if(cursor.getCount()<1) // UserName Not Exist
        {
            cursor.close();
            return "NOT EXIST";
        }
        cursor.moveToFirst();
        String password= cursor.getString(cursor.getColumnIndex("SAME"));
        cursor.close();
        return password;
    }

    public String getSinlgeEntryICICI1_other()
    {
        Cursor cursor=db.query("ICICI1", null, " ID=1", new String[]{}, null, null, null);
        if(cursor.getCount()<1) // UserName Not Exist
        {
            cursor.close();
            return "NOT EXIST";
        }
        cursor.moveToFirst();
        String password= cursor.getString(cursor.getColumnIndex("OTHER"));
        cursor.close();
        return password;
    }

    public String getSinlgeEntryaxis1_same()
    {
        Cursor cursor=db.query("AXIS1", null, " ID=1", new String[]{}, null, null, null);
        if(cursor.getCount()<1) // UserName Not Exist
        {
            cursor.close();
            return "NOT EXIST";
        }
        cursor.moveToFirst();
        String password= cursor.getString(cursor.getColumnIndex("SAME"));
        cursor.close();
        return password;
    }

    public String getSinlgeEntryaxis1_other()
    {
        Cursor cursor=db.query("AXIS1", null, " ID=1", new String[]{}, null, null, null);
        if(cursor.getCount()<1) // UserName Not Exist
        {
            cursor.close();
            return "NOT EXIST";
        }
        cursor.moveToFirst();
        String password= cursor.getString(cursor.getColumnIndex("OTHER"));
        cursor.close();
        return password;
    }




    // UPDATE Customers
    // SET ContactName='Alfred Schmidt', City='Hamburg';

    public void inserCategory(String categoryname, byte[] catimage){
        ContentValues categoryvalues=new ContentValues();

        categoryvalues.put("CATEGORYNAME",categoryname);
        categoryvalues.put("CATEGORY_IMAGE",catimage);

        db.insert("CATEGORY", null, categoryvalues);
    }


    public String categorylist(int id)
    {
        Cursor cursor=db.query("CATEGORY", null, " ID="+id, new String[]{}, null, null, null);
        if(cursor.getCount()<1) // UserName Not Exist
        {
            cursor.close();
            return " ";
        }
        cursor.moveToFirst();
        String password= cursor.getString(cursor.getColumnIndex("CATEGORYNAME"));
        cursor.close();
        return password;
    }

    public String getnewsidother(int id, int count) {
        String selectQuery = "SELECT `NEWSID` FROM NEWSOTHER WHERE `CATID` ="+ id+" AND `ID` ="+ count;
        Cursor c = db.rawQuery(selectQuery, new String[]{});
        String temp_news=null;
        if (c.moveToFirst()) {
            temp_news = c.getString(c.getColumnIndex("NEWSID"));
        }
        c.close();
        return temp_news;
    }

    public String getnewstitleother(int id, int count) {
        String selectQuery = "SELECT `TITLE` FROM NEWSOTHER WHERE `CATID` ="+ id+" AND `ID` ="+ count;
        Cursor c = db.rawQuery(selectQuery, new String[]{});
        String temp_news=null;
        if (c.moveToFirst()) {
            temp_news = c.getString(c.getColumnIndex("TITLE"));
        }
        c.close();
        return temp_news;
    }

    public String getnewsdetailother(int id, int count) {
        String selectQuery = "SELECT `DETAIL` FROM NEWSOTHER WHERE `CATID` ="+ id+" AND `ID` ="+ count;
        Cursor c = db.rawQuery(selectQuery, new String[]{});
        String temp_news=null;
        if (c.moveToFirst()) {
            temp_news = c.getString(c.getColumnIndex("DETAIL"));
        }
        c.close();
        return temp_news;
    }

    public String getnewsdateother(int id, int count) {
        String selectQuery = "SELECT `DATE` FROM NEWSOTHER WHERE `CATID` ="+ id+" AND `ID` ="+ count;
        Cursor c = db.rawQuery(selectQuery, new String[]{});
        String temp_news=null;
        if (c.moveToFirst()) {
            temp_news = c.getString(c.getColumnIndex("DATE"));
        }
        c.close();
        return temp_news;
    }

    public String getnewstimeother(int id, int count) {
        String selectQuery = "SELECT `TIME` FROM NEWSOTHER WHERE `CATID` ="+ id+" AND `ID` ="+ count;
        Cursor c = db.rawQuery(selectQuery, new String[]{});
        String temp_news=null;
        if (c.moveToFirst()) {
            temp_news = c.getString(c.getColumnIndex("TIME"));
        }
        c.close();
        return temp_news;
    }

    public String getnewscategoryother(int id, int count) {
        String selectQuery = "SELECT `CATID` FROM NEWSOTHER WHERE `CATID` ="+ id+" AND `ID` ="+ count;
        Cursor c = db.rawQuery(selectQuery, new String[]{});
        String temp_news=null;
        if (c.moveToFirst()) {
            temp_news = c.getString(c.getColumnIndex("CATID"));
        }
        c.close();
        return temp_news;
    }

    public String getnewsbreifother(int id, int count) {
        String selectQuery = "SELECT `BREIF` FROM NEWSOTHER WHERE `CATID` ="+ id+" AND `ID` ="+ count;
        Cursor c = db.rawQuery(selectQuery, new String[]{});
        String temp_news=null;
        if (c.moveToFirst()) {
            temp_news = c.getString(c.getColumnIndex("BREIF"));
        }
        c.close();
        return temp_news;
    }



    public String getprofileaudiobitrate(String newsid){
        String selectQuery = "SELECT AUDIOBITRATE FROM CUSTOMPROFILE WHERE ID="+newsid;
        Cursor c = db.rawQuery(selectQuery, new String[]{});
        String temp_news=null;
        if (c.moveToFirst()) {

            temp_news =  c.getString(c.getColumnIndex("AUDIOBITRATE"));//c.getBlob(c.getColumnIndex("NEWS_IMAGE"));
        }
        c.close();
        return temp_news;
    }

    public String getprofilesamplerate(String newsid){
        String selectQuery = "SELECT SAMPLERATE FROM CUSTOMPROFILE WHERE ID="+newsid;
        Cursor c = db.rawQuery(selectQuery, new String[]{});
        String temp_news=null;
        if (c.moveToFirst()) {

            temp_news =  c.getString(c.getColumnIndex("SAMPLERATE"));//c.getBlob(c.getColumnIndex("NEWS_IMAGE"));
        }
        c.close();
        return temp_news;
    }

    public String getprofilekeyinterval(String newsid){
        String selectQuery = "SELECT KEYINTERVAL FROM CUSTOMPROFILE WHERE ID="+newsid;
        Cursor c = db.rawQuery(selectQuery, new String[]{});
        String temp_news=null;
        if (c.moveToFirst()) {

            temp_news =  c.getString(c.getColumnIndex("KEYINTERVAL"));//c.getBlob(c.getColumnIndex("NEWS_IMAGE"));
        }
        c.close();
        return temp_news;
    }

    public String getprofileframerate(String newsid){
        String selectQuery = "SELECT FRAMERATE FROM CUSTOMPROFILE WHERE ID="+newsid;
        Cursor c = db.rawQuery(selectQuery, new String[]{});
        String temp_news=null;
        if (c.moveToFirst()) {

            temp_news =  c.getString(c.getColumnIndex("FRAMERATE"));//c.getBlob(c.getColumnIndex("NEWS_IMAGE"));
        }
        c.close();
        return temp_news;
    }

    public String getprofilebitrate(String newsid){
        String selectQuery = "SELECT BITRATE FROM CUSTOMPROFILE WHERE ID="+newsid;
        Cursor c = db.rawQuery(selectQuery, new String[]{});
        String temp_news=null;
        if (c.moveToFirst()) {

            temp_news =  c.getString(c.getColumnIndex("BITRATE"));//c.getBlob(c.getColumnIndex("NEWS_IMAGE"));
        }
        c.close();
        return temp_news;
    }

    public String getprofilewidth(String newsid){
        String selectQuery = "SELECT WIDTH FROM CUSTOMPROFILE WHERE ID="+newsid;
        Cursor c = db.rawQuery(selectQuery, new String[]{});
        String temp_news=null;
        if (c.moveToFirst()) {

            temp_news =  c.getString(c.getColumnIndex("WIDTH"));//c.getBlob(c.getColumnIndex("NEWS_IMAGE"));
        }
        c.close();
        return temp_news;
    }

    public String getprofileheight(String newsid){
        String selectQuery = "SELECT HEIGHT FROM CUSTOMPROFILE WHERE ID="+newsid;
        Cursor c = db.rawQuery(selectQuery, new String[]{});
        String temp_news=null;
        if (c.moveToFirst()) {

            temp_news =  c.getString(c.getColumnIndex("HEIGHT"));//c.getBlob(c.getColumnIndex("NEWS_IMAGE"));
        }
        c.close();
        return temp_news;
    }


    public String getprofilename(String newsid){
        String selectQuery = "SELECT PROFILENAME FROM CUSTOMPROFILE WHERE ID="+newsid;
        Cursor c = db.rawQuery(selectQuery, new String[]{});
        String temp_news=null;
        if (c.moveToFirst()) {

            temp_news =  c.getString(c.getColumnIndex("PROFILENAME"));//c.getBlob(c.getColumnIndex("NEWS_IMAGE"));
        }
        c.close();
        return temp_news;
    }

    public byte[] getcatbyteother(String newsid){
        String selectQuery = "SELECT CATIMG FROM NEWSIMAGE WHERE NEWSID="+newsid;
        Cursor c = db.rawQuery(selectQuery, new String[]{});
        byte[] temp_news=null;
        if (c.moveToFirst()) {

            temp_news =  c.getBlob(c.getColumnIndex("CATIMG"));
        }
        c.close();
        return temp_news;
    }

    //othernews
    //samecat
    public String getnewsidsame(int id, int count) {
        String selectQuery = "SELECT NEWSID FROM NEWS WHERE NEWSID="+id;
        Cursor c = db.rawQuery(selectQuery, new String[]{});
        String temp_news=null;
        if (c.moveToFirst()) {
            temp_news = c.getString(c.getColumnIndex("NEWSID"));
        }
        c.close();
        return temp_news;
    }

    public String getnewstitlesame(int id, int count) {
        String selectQuery = "SELECT TITLE FROM NEWS WHERE NEWSID="+id;
        Cursor c = db.rawQuery(selectQuery, new String[]{});
        String temp_news=null;
        if (c.moveToFirst()) {
            temp_news = c.getString(c.getColumnIndex("TITLE"));
        }
        c.close();
        return temp_news;
    }

    public String getnewsdetailsame(int id, int count) {
        String selectQuery = "SELECT DETAIL FROM NEWS WHERE NEWSID="+id;
        Cursor c = db.rawQuery(selectQuery, new String[]{});
        String temp_news=null;
        if (c.moveToFirst()) {
            temp_news = c.getString(c.getColumnIndex("DETAIL"));
        }
        c.close();
        return temp_news;
    }

    public String getnewsdatesame(int id, int count) {
        String selectQuery = "SELECT DATE FROM NEWS WHERE NEWSID="+id;
        Cursor c = db.rawQuery(selectQuery, new String[]{});
        String temp_news=null;
        if (c.moveToFirst()) {
            temp_news = c.getString(c.getColumnIndex("DATE"));
        }
        c.close();
        return temp_news;
    }

    public String getnewstimesame(int id, int count) {
        String selectQuery = "SELECT TIME FROM NEWS WHERE NEWSID="+id;
        Cursor c = db.rawQuery(selectQuery, new String[]{});
        String temp_news=null;
        if (c.moveToFirst()) {
            temp_news = c.getString(c.getColumnIndex("TIME"));
        }
        c.close();
        return temp_news;
    }

    public String getnewscategorysame(int id, int count) {
        String selectQuery = "SELECT CATID FROM NEWS WHERE NEWSID="+id;
        Cursor c = db.rawQuery(selectQuery, new String[]{});
        String temp_news=null;
        if (c.moveToFirst()) {
            temp_news = c.getString(c.getColumnIndex("CATID"));
        }
        c.close();
        return temp_news;
    }

    public String getnewsbreifsame(int id, int count) {
        String selectQuery = "SELECT BREIF FROM NEWS WHERE NEWSID="+id;
        Cursor c = db.rawQuery(selectQuery, new String[]{});
        String temp_news=null;
        if (c.moveToFirst()) {
            temp_news = c.getString(c.getColumnIndex("BREIF"));
        }
        c.close();
        return temp_news;
    }

    //samecat

    public String getnewsothercnfm(int id) {

        Cursor cursor=db.query("NEWSOTHER", null, " NEWSID="+ id, new String[]{}, null, null, null);
        if(cursor.getCount()<1) // UserName Not Exist
        {
            cursor.close();
            return null;
        }
        cursor.moveToFirst();
        String title= cursor.getString(cursor.getColumnIndex("TITLE"));
        cursor.close();
        return title;

    }


    public String getnewstitlecnfm(int id) {

        Cursor cursor=db.query("NEWS", null, " NEWSID="+ id, new String[]{}, null, null, null);
        if(cursor.getCount()<1) // UserName Not Exist
        {
            cursor.close();
            return null;
        }
        cursor.moveToFirst();
        String title= cursor.getString(cursor.getColumnIndex("TITLE"));
        cursor.close();
        return title;

    }


    public String getnewsid(int id) {
        String selectQuery = "SELECT NEWSID FROM NEWS WHERE ID="+id;
        Cursor c = db.rawQuery(selectQuery, new String[]{});
        String temp_news=null;
        if (c.moveToFirst()) {
            temp_news = c.getString(c.getColumnIndex("NEWSID"));
        }
        c.close();
        return temp_news;
    }

    public String getnewstitle(int id) {
        String selectQuery = "SELECT TITLE FROM NEWS WHERE ID="+id;
        Cursor c = db.rawQuery(selectQuery, new String[]{});
        String temp_news=null;
        if (c.moveToFirst()) {
            temp_news = c.getString(c.getColumnIndex("TITLE"));
        }
        c.close();
        return temp_news;
    }

    public String getnewsdetail(int id) {
        String selectQuery = "SELECT DETAIL FROM NEWS WHERE ID="+id;
        Cursor c = db.rawQuery(selectQuery, new String[]{});
        String temp_news=null;
        if (c.moveToFirst()) {
            temp_news = c.getString(c.getColumnIndex("DETAIL"));
        }
        c.close();
        return temp_news;
    }

    public String getnewsdate(int id) {
        String selectQuery = "SELECT DATE FROM NEWS WHERE ID="+id;
        Cursor c = db.rawQuery(selectQuery, new String[]{});
        String temp_news=null;
        if (c.moveToFirst()) {
            temp_news = c.getString(c.getColumnIndex("DATE"));
        }
        c.close();
        return temp_news;
    }

    public String getnewstime(int id) {
        String selectQuery = "SELECT TIME FROM NEWS WHERE ID="+id;
        Cursor c = db.rawQuery(selectQuery, new String[]{});
        String temp_news=null;
        if (c.moveToFirst()) {
            temp_news = c.getString(c.getColumnIndex("TIME"));
        }
        c.close();
        return temp_news;
    }

    public String getnewscategory(int id) {
        String selectQuery = "SELECT CATID FROM NEWS WHERE ID="+id;
        Cursor c = db.rawQuery(selectQuery, new String[]{});
        String temp_news=null;
        if (c.moveToFirst()) {
            temp_news = c.getString(c.getColumnIndex("CATID"));
        }
        c.close();
        return temp_news;
    }

    public String getnewsbreif(int id) {
        String selectQuery = "SELECT BREIF FROM NEWS WHERE ID="+id;
        Cursor c = db.rawQuery(selectQuery, new String[]{});
        String temp_news=null;
        if (c.moveToFirst()) {
            temp_news = c.getString(c.getColumnIndex("BREIF"));
        }
        c.close();
        return temp_news;
    }

    public byte[] getnewsbyte(String newsid){
        String selectQuery = "SELECT NEWS_IMAGE FROM NEWSIMAGE WHERE NEWSID="+newsid;
        Cursor c = db.rawQuery(selectQuery, new String[]{});
        byte[] temp_news=null;
        if (c.moveToFirst()) {

            temp_news =  c.getBlob(c.getColumnIndex("NEWS_IMAGE"));
        }
        c.close();
        return temp_news;
    }

    public byte[] getcatbyte(String newsid){
        String selectQuery = "SELECT CATIMG FROM NEWSIMAGE WHERE NEWSID="+newsid;
        Cursor c = db.rawQuery(selectQuery, new String[]{});
        byte[] temp_news=null;
        if (c.moveToFirst()) {

            temp_news =  c.getBlob(c.getColumnIndex("CATIMG"));
        }
        c.close();
        return temp_news;
    }


    //othernews


    public byte[] getcategorybyte(String catid){
        String selectQuery = "SELECT CATEGORY_IMAGE FROM CATIMAGE WHERE CATID="+catid;
        Cursor c = db.rawQuery(selectQuery, new String[]{});
        byte[] temp_news=null;
        if (c.moveToFirst()) {

            temp_news =  c.getBlob(c.getColumnIndex("CATEGORY_IMAGE"));
        }
        c.close();
        return temp_news;
    }

    public void updateNewsimage(String newsid, String catid, byte[] image, byte[] catimg){
        ContentValues updatedValues=new ContentValues();

        updatedValues.put("NEWSID",newsid);
        updatedValues.put("CATID",catid);

        updatedValues.put("NEWS_IMAGE",image);
        updatedValues.put("CATIMG",catimg);

        db.insert("NEWSIMAGE", null, updatedValues);
    }

    public void updateCatimage(String categoryname, String catid, byte[] image){
        ContentValues updatedValues=new ContentValues();

        updatedValues.put("NEWSID",categoryname);
        updatedValues.put("CATID",catid);

        updatedValues.put("CATEGORY_IMAGE", image);

        db.insert("CATIMAGE", null, updatedValues);
    }


    public void updateNewsother(int newsid, int catid, String date, String time, String title, String detail, String breif, byte[] image){
        ContentValues updatedValues=new ContentValues();

        updatedValues.put("NEWSID",newsid);
        updatedValues.put("CATID",catid);
        updatedValues.put("DATE",date);
        updatedValues.put("TIME",time);
        updatedValues.put("TITLE",title);
        updatedValues.put("DETAIL",detail);
        updatedValues.put("BREIF", breif);
        updatedValues.put("NEWS_IMAGE",image);

        // "INSERT INTO table1 (field1, field2) " +
        //  "VALUES (" + field_one + ", " + field_two + ")";

        db.insert("NEWSOTHER", null, updatedValues);

        // "VALUES (" + field_one + ", " + field_two + ")";

        //   db.rawQuery("INSERT INTO"+" NEWS (NEWSID,DATE,TIME,TITLE,BREIF)"+"VALUES (" + newsid + ", " + date + ", " + time + ", " + title + ", " + breif + ")";

        /*INSERT INTO NEWS (ID,NAME,AGE,ADDRESS,SALARY)
        VALUES (2, 'Allen', 25, 'Texas', 15000.00 );*/
        // db.rawQuery("INSERTINTO")


    }

    public void updateNews(int newsid, int catid, String date, String time, String title, String detail, String breif, String image, String catimg){
        ContentValues updatedValues=new ContentValues();

        updatedValues.put("NEWSID",newsid);
        updatedValues.put("CATID",catid);
        updatedValues.put("DATE",date);
        updatedValues.put("TIME",time);
        updatedValues.put("TITLE",title);
        updatedValues.put("DETAIL",detail);
        updatedValues.put("BREIF", breif);
        updatedValues.put("NEWS_IMAGE",image);


        // "INSERT INTO table1 (field1, field2) " +
        //  "VALUES (" + field_one + ", " + field_two + ")";

        db.insert("NEWS", null, updatedValues);


        // "VALUES (" + field_one + ", " + field_two + ")";

        //   db.rawQuery("INSERT INTO"+" NEWS (NEWSID,DATE,TIME,TITLE,BREIF)"+"VALUES (" + newsid + ", " + date + ", " + time + ", " + title + ", " + breif + ")";

        /*INSERT INTO NEWS (ID,NAME,AGE,ADDRESS,SALARY)
        VALUES (2, 'Allen', 25, 'Texas', 15000.00 );*/
        // db.rawQuery("INSERTINTO")


    }

    public String getallnews(){
        String selectQuery = "SELECT * FROM `NEWS` ";
        Cursor c = db.rawQuery(selectQuery, new String[]{});
        String newsid=null,image= null,shortest=null,category_img=null,title=null,breif=null,date=null,description=null,catagory=null,time=null;
        if (c.moveToFirst()) {
            do {

               /* create table "+"NEWS"+
                "( "+"ID"+" INTEGER PRIMARY KEY,"+"NEWSID"+" INTEGER,"+"CATID"+" INTEGER,"+"DATE"+"  TEXT NOT NULL,"+"TIME"+" TEXT NOT NULL,"+"TITLE"+" TEXT NOT NULL,"+"DETAIL"+" TEXT NOT NULL,"+"BREIF"+" TEXT NOT NULL,"+"NEWS_IMAGE" + " TEXT NOT NULL); ";
*/

                newsid = c.getString(c.getColumnIndex("NEWSID"));
                //Toast.makeText(context, "retrive value : "+newsid, Toast.LENGTH_LONG).show();
                image = c.getString(c.getColumnIndex("NEWS_IMAGE"));
                catagory = c.getString(c.getColumnIndex("CATID"));
                date = c.getString(c.getColumnIndex("DATE"));
                time = c.getString(c.getColumnIndex("TIME"));
                title = c.getString(c.getColumnIndex("TITLE"));
                shortest = c.getString(c.getColumnIndex("DETAIL"));
               category_img = c.getString(c.getColumnIndex("CAT_IMAGE"));

                breif = c.getString(c.getColumnIndex("BREIF"));

                HashMap<String, String> map = new HashMap<String, String>();
                map.put("news_id", newsid);
                map.put("catagory", catagory);
                map.put("date", date);
                map.put("time", time);
                map.put("title", title);
                map.put("shortest", shortest);
                map.put("image", image);
                map.put("category_img", category_img);
                map.put("image_large", null);
                map.put("brief", breif);
                map.put("video_link", null);

               // Constant.prayertime.add(map);
                   /* temp_news = c.getString(c.getColumnIndex("NEWSID"));
                    map.put("catid",temp_news);
                    Constant.othernewsid.add(map);*/

            } while (c.moveToNext());


        }
        c.close();
        return null;
    }

    public void  updateEntry(String axis_same_val)
    {
        // Define the updated row content.
        ContentValues updatedValues = new ContentValues();
        // Assign values for each row.
        updatedValues.put("SAME", axis_same_val);
        //  WHERE CustomerName='Alfreds Futterkiste'
        Log.d("same", String.valueOf(axis_same_val));
        String where=" ID= 2";
        db.rawQuery("UPDATE"+ " AXIS"+ " SET"+ " SAME="+axis_same_val+ " WHERE"+ where, null);
        //db.update("AXIS", updatedValues, where, new String[]{Integer.toString("1")});
    }


    public void  updateEntryother(String axis_other_val)
    {
        // Define the updated row content.
        ContentValues updatedValues = new ContentValues();
        // Assign values for each row.
        updatedValues.put("OTHER", axis_other_val);
        Log.d("other", String.valueOf(axis_other_val));
        String where=" ID= 1";
        db.rawQuery("UPDATE"+ " AXIS"+ " SET"+ " OTHER="+axis_other_val+ " WHERE"+ where, null);
        // db.update("AXIS",updatedValues, where, new String[]{Integer.toString("1")});
    }


    public void  updateEntry_idbi(String same)
    {
        // Define the updated row content.
        ContentValues updatedValues = new ContentValues();
        // Assign values for each row.
        updatedValues.put("SAME", same);



        String where="ID =1";
        db.update("IDBI", updatedValues, where, new String[]{same});
    }


    public void  updateEntryother_idbi(String other)
    {
        // Define the updated row content.
        ContentValues updatedValues = new ContentValues();
        // Assign values for each row.
        updatedValues.put("OTHER", other);

        String where="ID =1";
        db.update("IDBI",updatedValues, where, new String[]{other});
    }


    public void  updateEntry_icici(String same)
    {
        // Define the updated row content.
        ContentValues updatedValues = new ContentValues();
        // Assign values for each row.
        updatedValues.put("SAME", same);

        String where="ID =1";
        db.update("ICICI",updatedValues, where, new String[]{same});
    }


    public void  updateEntryother_icici(String other)
    {
        // Define the updated row content.
        ContentValues updatedValues = new ContentValues();
        // Assign values for each row.
        updatedValues.put("OTHER", other);

        String where="ID =1";
        db.update("ICICI",updatedValues, where, new String[]{other});
    }


    public Cursor getvalue(){
        Cursor value=db.rawQuery("SELECT"+ " SAME"+ " FROM"+ " AXIS"+ " WHERE"+ " ID="+1,null);
        value.close();
        return value;


    }

    public void delete(){
        String where="ID=2";
        db.delete("AXIS", where, new String[]{}) ;

    }

    public void deleteidbii(){
        String where="ID=2";
        db.delete("IDBI", where, new String[]{}) ;

    }

    public void deleteicicii(){
        String where="ID=2";

        db.delete("ICICI", where, new String[]{}) ;
    }


    public void deleteprofile(String profilename, int position){
       // String where="PROFILENAME="+profilename;
       String where="PROFILENAME LIKE '"+profilename+"%'";
       // String where="ID LIKE '"+position+"%'";
        db.delete("CUSTOMPROFILE", where, new String[]{}) ;
      //  db.execSQL("DELETE FROM COMPANY WHERE PROFILENAME ="+profilename);
    }


    public void deleteall(String profilename, int position){
        // String where="PROFILENAME="+profilename;
        //String where="PROFILENAME LIKE '"+profilename+"%'";
        // String where="ID LIKE '"+position+"%'";
        db.execSQL("delete from "+ "CUSTOMPROFILE");
        //  db.execSQL("DELETE FROM COMPANY WHERE PROFILENAME ="+profilename);
    }


    public void deleteoldnews(String date){
        String data=date;
        if(data.contains("Jan")){
            data= data.replace("Jan","01");
        }else if(data.contains("Feb")){
            data=data.replace("Feb","02");
        }else if(data.contains("Mar")){
            data=data.replace("Mar","03");
        }else if(data.contains("Apr")){
            data= data.replace("Apr","04");
        }else if(data.contains("May")){
            data=data.replace("May","05");
        }else if(data.contains("Jun")){
            data=data.replace("Jun","06");
        }else if(data.contains("Jul")){
            data=data.replace("Jul","07");
        }else if(data.contains("Aug")){
            data=data.replace("Aug","08");
        }else if(data.contains("Sep")){
            data= data.replace("Seb","09");
        }else if(data.contains("Oct")){

            data=data.replace("Oct","10");
        }else if(data.contains("Nov")){
            data=data.replace("Nov","11");
        }else if(data.contains("Dec")){
            data=data.replace("Dec","12");
        }
        String where="DATE NOT LIKE '"+data+"%'";
        // String selectQuery = "SELECT CATIMG FROM NEWSIMAGE WHERE ="+newsid;
        db.delete("NEWS", where, new String[]{});
        db.delete("NEWSOTHER", where, new String[]{});


    }
    public void  updateEntryy(String userName, String password)
    {
        // Define the updated row content.
        ContentValues updatedValues = new ContentValues();
        // Assign values for each row.
        updatedValues.put("SAME", userName);
        updatedValues.put("OTHER",password);
        db.update("AXIS", updatedValues, "ID"+"="+1, null);
        // TableName, cv, "_id "+"="+1, null
 
          /*  String where="ID = 1";
           db.rawQuery("UPDATE AXIS SET SAME ="+"'"+userName+"'"+ " WHERE ID = 1", null);
           db.rawQuery("UPDATE AXIS SET OTHER ="+"'"+password+"'"+ " WHERE ID = 1", null);*/
        //db.update("AXIS",updatedValues, where, new String[]{userName});
    }

    public void  updateEntryy_idbi(String userName, String password)
    {
        // Define the updated row content.
        ContentValues updatedValues = new ContentValues();
        // Assign values for each row.
        updatedValues.put("SAME", userName);
        updatedValues.put("OTHER",password);
        db.update("IDBI", updatedValues, "ID"+"="+1, null);
        // TableName, cv, "_id "+"="+1, null
 
          /*  String where="ID = 1";
           db.rawQuery("UPDATE AXIS SET SAME ="+"'"+userName+"'"+ " WHERE ID = 1", null);
           db.rawQuery("UPDATE AXIS SET OTHER ="+"'"+password+"'"+ " WHERE ID = 1", null);*/
        //db.update("AXIS",updatedValues, where, new String[]{userName});
    }

    public void  updateEntryy_icici(String userName, String password)
    {
        // Define the updated row content.
        ContentValues updatedValues = new ContentValues();
        // Assign values for each row.
        updatedValues.put("SAME", userName);
        updatedValues.put("OTHER",password);
        db.update("ICICI", updatedValues, "ID"+"="+1, null);
        // TableName, cv, "_id "+"="+1, null
 
          /*  String where="ID = 1";
           db.rawQuery("UPDATE AXIS SET SAME ="+"'"+userName+"'"+ " WHERE ID = 1", null);
           db.rawQuery("UPDATE AXIS SET OTHER ="+"'"+password+"'"+ " WHERE ID = 1", null);*/
        //db.update("AXIS",updatedValues, where, new String[]{userName});
    }

    public void  updateEntryy_icici1(String userName, String password)
    {
        // Define the updated row content.
        ContentValues updatedValues = new ContentValues();
        // Assign values for each row.
        updatedValues.put("SAME", userName);
        updatedValues.put("OTHER",password);
        db.update("ICICI1", updatedValues, "ID"+"="+1, null);
        // TableName, cv, "_id "+"="+1, null
 
          /*  String where="ID = 1";
           db.rawQuery("UPDATE AXIS SET SAME ="+"'"+userName+"'"+ " WHERE ID = 1", null);
           db.rawQuery("UPDATE AXIS SET OTHER ="+"'"+password+"'"+ " WHERE ID = 1", null);*/
        //db.update("AXIS",updatedValues, where, new String[]{userName});
    }

    public void  updateEntryy_axis(String userName, String password)
    {
        // Define the updated row content.
        ContentValues updatedValues = new ContentValues();
        // Assign values for each row.
        updatedValues.put("SAME", userName);
        updatedValues.put("OTHER",password);
        db.update("AXIS1", updatedValues, "ID"+"="+1, null);
        // TableName, cv, "_id "+"="+1, null
 
          /*  String where="ID = 1";
           db.rawQuery("UPDATE AXIS SET SAME ="+"'"+userName+"'"+ " WHERE ID = 1", null);
           db.rawQuery("UPDATE AXIS SET OTHER ="+"'"+password+"'"+ " WHERE ID = 1", null);*/
        //db.update("AXIS",updatedValues, where, new String[]{userName});
    }

    public void getprofilename(int i) {


    }
}