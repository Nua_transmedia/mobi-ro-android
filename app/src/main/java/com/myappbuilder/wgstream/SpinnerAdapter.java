package com.myappbuilder.wgstream;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by nua-android on 30/5/16.
 * This code and all components (c) Copyright 2016-2017, NuaTransMedia,. All rights reserved.
 */

public class SpinnerAdapter extends ArrayAdapter<String> {
	Context context;
	ArrayList<String> iName;
	ArrayList<String> iAbout;
	
	TextView spnItemName;
	ImageView spnItemIcon;
	private LoginDataBaseAdapter loginDataBaseAdapter;
	
	public SpinnerAdapter(Context context, int textViewResourceId, ArrayList<String> objects, ArrayList<String> iName){
		super(context,textViewResourceId,objects);
		this.context = context;
		this.iName = iName;
	}
	
	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent){
		return getCustomView(position, convertView, parent);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		return getCustomView(position, convertView, parent);
	}
	
	public View getCustomView(final int position, View convertView, ViewGroup parent){
		LayoutInflater inflater = ((Activity) context).getLayoutInflater();
		View rowView =  inflater.inflate(R.layout.profile_listitem, parent, false);
		
		spnItemName = (TextView) rowView.findViewById(R.id.profile_name);
		//spnItemDel = (TextView) rowView.findViewById(R.id.spnItemDel);
		spnItemIcon=(ImageView) rowView.findViewById(R.id.profile_delete) ;

		loginDataBaseAdapter = new LoginDataBaseAdapter(context);
		loginDataBaseAdapter = loginDataBaseAdapter.open();
		
		spnItemName.setText(iName.get(position)+"");

		Log.d("totalcount",String.valueOf(iName.size()));

		if (position>5){
			if(position==iName.size()-1){
				spnItemIcon.setVisibility(View.VISIBLE);
				spnItemIcon.setBackgroundResource(R.drawable.addprofile);
			}else {
				spnItemIcon.setVisibility(View.VISIBLE);
			}
		}
		else{
			Log.d("pos",String.valueOf(position));
			spnItemIcon.setVisibility(View.GONE);
		}

		spnItemIcon.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {

				if(position==loginDataBaseAdapter.getcountofval(6)) {

				}else {
					new AlertDialog.Builder(context)
							.setTitle("Mobiro")
							.setMessage("Do you want to remove this Profile?")
							.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int which) {
									// continue with delete
									Toast.makeText(context, "Deleted", Toast.LENGTH_SHORT).show();
									//iName[position] = null;
									iName.remove(position);
									notifyDataSetChanged();
									loginDataBaseAdapter = new LoginDataBaseAdapter(context);
									loginDataBaseAdapter = loginDataBaseAdapter.open();
									loginDataBaseAdapter.deleteall(iName.get(position),position);
									//loginDataBaseAdapter.deleteprofile(iName.get(position),position);







								}
							})
							.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int which) {
									// do nothing
									dialog.dismiss();
									dialog.cancel();
								}
							})
							.setIcon(android.R.drawable.ic_dialog_alert)
							.show();
				}





			}
		});
		return rowView;
	}
}