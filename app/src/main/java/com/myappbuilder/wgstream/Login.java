package com.myappbuilder.wgstream;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.myappbuilder.wgstream.config.ConfigPrefs;
import com.wowza.gocoder.sdk.api.broadcast.WZBroadcastConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by nua-android on 30/5/16.
 * This code and all components (c) Copyright 2016-2017, NuaTransMedia,. All rights reserved.
 */
public class Login extends Activity {

    private EditText username,password;
    private Button submit;
   // private MP4Broadcaster      mMP4Broadcaster;
    private WZBroadcastConfig mBroadcastConfig;
    private ProgressDialog prg_bar;

    private CheckBox rememberme;
    private boolean login,remember;

    private String ruser,rpassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loginscreen);
        username=(EditText) findViewById(R.id.usernm_edit);
        password=(EditText) findViewById(R.id.passwrd_edit);
        submit =(Button) findViewById(R.id.login);

        Constant.dayee = getSharedPreferences("dayfun",
                MODE_WORLD_WRITEABLE);

        remember=Constant.dayee.getBoolean("remember",false);

        rememberme=(CheckBox) findViewById(R.id.chck_remember);

      //  mMP4Broadcaster = new MP4Broadcaster();
        mBroadcastConfig = new WZBroadcastConfig();
       // mBroadcastConfig.setVideoBroadcaster(mMP4Broadcaster);
        mBroadcastConfig.setAudioEnabled(false);

        prg_bar = new ProgressDialog(Login.this);
        prg_bar.setMessage("Please Wait");
        prg_bar.setIndeterminate(true);
        prg_bar.setCancelable(false);
        prg_bar.setMax(100);
        prg_bar.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        rememberme.setChecked(remember);

        if(remember==true){

            Constant.dayee = getSharedPreferences("dayfun",
                    MODE_WORLD_WRITEABLE);

            ruser=Constant.dayee.getString("username", "");
            rpassword=Constant.dayee.getString("password","");

            username.setText(ruser);
            password.setText(rpassword);



        }

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String user=username.getText().toString();
                String passwrd=password.getText().toString();


if(user.length()!=0) {
    if(passwrd.length()!=0) {
        Constant.cd = new ConnectionDetector(getApplicationContext());
        Constant.isInternetPresent = Constant.cd.isConnectingToInternet();
        if (Constant.isInternetPresent) {
            validate(user, passwrd);
            prg_bar.show();

        }else{
            toastsettext("No Internet Connection");
        }
    }else{
        password.setFocusable(true);
        password.callOnClick();
        toastsettext("Please Enter Password");
    }
}else{
    toastsettext("Please Enter Username");
}



            }
        });
    }


    public void toastsettext(String string1) {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.altdialog_toast,
                (ViewGroup) findViewById(R.id.toast_rl));
        TextView txt = (TextView) layout.findViewById(R.id.toast_txt);
        txt.setText(string1);
        Toast tst = new Toast(getApplicationContext());
        tst.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        tst.setDuration(Toast.LENGTH_SHORT);
        tst.setView(layout);
        tst.show();
    }

    private void validate(final String user, final String passwrd) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    WebClient webClient = new WebClient();
                    String getRes = null;
                    try {
                        getRes = webClient.doGetReq("https://u2mrf96skg.execute-api.ap-southeast-1.amazonaws.com/mobiro/login?userid="+user+"&password="+passwrd);
                        Log.d("Home_page_Array :", getRes);

                        Log.e("post response", getRes);


                        try {
                            JSONArray respons = new JSONArray(getRes);
                            Log.d("res", String.valueOf(respons));

                            JSONObject obj=respons.getJSONObject(0);


                            String message=obj.getString("Message");


                            //Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();

                            if(message.contentEquals("Success")){
                                String usernamee=obj.getString("username");
                                String streemname=obj.getString("StreamName");
                                String passwordd=obj.getString("Password");
                                String hostaddr=obj.getString("Hostaddress");
                                String portno=obj.getString("Portnumber");

                                Constant.username=usernamee;
                                Constant.allnames=usernamee;
                                Constant.password=passwordd;


                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        toastsettext("Logged in Successfully");
                                        username.setText("");
                                        password.setText("");

                                      //  Toast.makeText(Login.this, "Login Successfully", Toast.LENGTH_SHORT).show();

                                    }
                                });



                                Constant.dayee = getSharedPreferences("dayfun", MODE_PRIVATE);
                                Constant.daycom = Constant.dayee.edit();
                                Constant.daycom.putString("username", usernamee);
                                Constant.daycom.putString("password", passwordd);
                                Constant.daycom.putString("host", hostaddr);
                                Constant.daycom.putString("port", portno);
                                Constant.daycom.putBoolean("remember", rememberme.isChecked());
                                Constant.daycom.putBoolean("login",true);

                                Constant.daycom.commit();



                                //orginalchange
                                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(Login.this);
                                SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
                                prefsEditor.putString("wz_live_host_address",hostaddr);
                                prefsEditor.putString("wz_live_port_number", portno);
                                prefsEditor.putString("wz_live_app_name", usernamee);
                                prefsEditor.putString("wz_live_stream_name", streemname);
                                prefsEditor.putString("wz_live_username", usernamee);
                                prefsEditor.putString("wz_live_password", passwordd);

                                prefsEditor.apply();
                                prefsEditor.commit();
                                ConfigPrefs.updateConfigFromPrefs(PreferenceManager.getDefaultSharedPreferences(Login.this), mBroadcastConfig);

                                //orginalchange




                                Intent inten=new Intent(getApplicationContext(),Livestream.class);
                                startActivity(inten);
                                Login.this.finish();
                                if(prg_bar.isShowing()){
                                    prg_bar.dismiss();
                                    prg_bar.cancel();
                                }else{

                                }

                            }else{

                                if(prg_bar.isShowing()){
                                    prg_bar.dismiss();
                                    prg_bar.cancel();
                                }else{

                                }

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {


                                        Constant.dayee = getSharedPreferences("dayfun", MODE_PRIVATE);
                                        Constant.daycom = Constant.dayee.edit();
                                        Constant.daycom.putString("username", ruser);
                                        Constant.daycom.putString("password", rpassword);
                                        Constant.daycom.putBoolean("remember", rememberme.isChecked());

                                        Constant.daycom.commit();

                                        username.setText(ruser);
                                        password.setText(rpassword);

                                        toastsettext("Invalid user");
                                       // Toast.makeText(Login.this, "Invalid user", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }






                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    //  Log.d("namadhu top Size : ", "" + Constant.namadhu_top_news_Array.size());


                } catch (Exception ex) {
                    ex.printStackTrace();

                }
            }
        }).start();

    }


    public String md5(String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            for (int i=0; i<messageDigest.length; i++)
                hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }
}
