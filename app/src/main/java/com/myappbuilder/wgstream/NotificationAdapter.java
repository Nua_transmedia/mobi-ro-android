package com.myappbuilder.wgstream;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nua-android on 30/5/16.
 * This code and all components (c) Copyright 2016-2017, NuaTransMedia,. All rights reserved.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotifiViewHolder> {

    private List<Notificationitem> listItems, filterList;
    private Context mContext;

    public NotificationAdapter(Context context, List<Notificationitem> listItems) {
        this.listItems = listItems;
        this.mContext = context;
        this.filterList = new ArrayList<Notificationitem>();
        // we copy the original list to the filter list and use it for setting row values
        this.filterList.addAll(this.listItems);
    }

    @Override
    public NotifiViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.notificationitem, null);
        NotifiViewHolder viewHolder = new NotifiViewHolder(view);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(NotifiViewHolder customViewHolder, final int position) {

        Notificationitem listItem = filterList.get(position);
        customViewHolder.tvName.setText(listItem.notification);

        SimpleDateFormat fromUser = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat myFormat = new SimpleDateFormat("dd-MM-yyyy");

        String reformattedStr=null;
        //  Toast.makeText(getActivity().getApplicationContext(),"date"+String.valueOf(reformattedStr),Toast.LENGTH_SHORT).show();

        try {

            reformattedStr = myFormat.format(fromUser.parse(listItem.date));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        if(reformattedStr!=null) {
            customViewHolder.date.setText(reformattedStr);
        }else{
            customViewHolder.date.setText(listItem.date);
        }




        customViewHolder.time.setText(listItem.time);

    }

    @Override
    public int getItemCount() {
        return (null != filterList ? filterList.size() : 0);
    }

    public void filter(final String text) {

        // Searching could be complex..so we will dispatch it to a different thread...
        new Thread(new Runnable() {
            @Override
            public void run() {

                // Clear the filter list
                filterList.clear();

                // If there is no search value, then add all original list items to filter list
                if (TextUtils.isEmpty(text)) {

                    filterList.addAll(listItems);

                } else {
                    // Iterate in the original List and add it to filter list...
                    for (Notificationitem item : listItems) {
                        if (((item.notification).toString()).toLowerCase().contains(text.toLowerCase()) ||
                                ((item.date).toString()).toLowerCase().contains(text.toLowerCase()) || ((item.time).toString()).toLowerCase().contains(text.toLowerCase())) {
                            // Adding Matched items
                            filterList.add(item);
                        }
                    }
                }

                // Set on UI Thread
                ((Activity) mContext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // Notify the List that the DataSet has changed...
                        notifyDataSetChanged();
                    }
                });

            }
        }).start();

    }

}
