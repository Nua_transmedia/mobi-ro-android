package com.myappbuilder.wgstream;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by nua-android on 30/5/16.
 * This code and all components (c) Copyright 2016-2017, NuaTransMedia,. All rights reserved.
 */
public class Notificationlist extends Activity implements  SearchView.OnQueryTextListener {


    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<Notificationitem> allList;
    private SearchView mSearchView;

    private ImageLoader imageLoader;

    private Button back;


    private NotificationAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notificationlist);

        mSearchView=(SearchView) findViewById(R.id.videosearch);
        back=(Button) findViewById(R.id.back_edit);


        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_addnew);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        //getnotification();

        mSearchView.setFocusable(false);

        setList();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String searchword= String.valueOf(mSearchView.getQuery());

                if(searchword.length()!=0){
                    mSearchView.setQuery("",true);

                }else{
                    Notificationlist.this.finish();
                }


            }
        });

        adapter = new NotificationAdapter(Notificationlist.this, allList);
         mRecyclerView.setAdapter(adapter);

        setupSearchView();

        if(Constant.notificationlist.size()==0){
            toastsettext("No Notification");
        }



    }


    @Override
    public void onBackPressed() {

        String searchword= String.valueOf(mSearchView.getQuery());

        if(searchword.length()!=0){
            mSearchView.setQuery("",true);

        }else{
            finish();
        }
    }

    public void toastsettext(String string1) {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.altdialog_toast,
                (ViewGroup) findViewById(R.id.toast_rl));
        TextView txt = (TextView) layout.findViewById(R.id.toast_txt);
        txt.setText(string1);
        Toast tst = new Toast(getApplicationContext());
        tst.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        tst.setDuration(Toast.LENGTH_SHORT);
        tst.setView(layout);
        tst.show();
    }

    private void setupSearchView() {
        mSearchView.setIconifiedByDefault(false);
        mSearchView.setOnQueryTextListener(Notificationlist.this);
        mSearchView.setSubmitButtonEnabled(true);
        mSearchView.setQueryHint("Search Here");
        mSearchView.clearFocus();
    }

    private void getnotification(){

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    WebClient webClient = new WebClient();
                    String getRes = null;
                    try {
                        getRes = webClient.doGetReq("https://u2mrf96skg.execute-api.ap-southeast-1.amazonaws.com/mobiro/notify/list?deviceid=3f45c40541732e3291c00eae733c2cbf70975c96fddeb3671610cbf68af5804b");
                        Log.d("Home_page_Array :", getRes);

                        Log.e("post response", getRes);


                        try {
                            JSONArray respons = new JSONArray(getRes);


                            for (int i = 0; i < respons.length(); i++) {
                                HashMap<String, String> map = new HashMap<String, String>();
                                JSONObject jsonobject = respons.getJSONObject(i);
                                String notification = jsonobject.getString("messsage");
                                String date =jsonobject.getString("date");
                                String time=jsonobject.getString("time");


                                map.put("notification",notification);
                                map.put("date",date);
                                map.put("time",time);


                                Constant.notificationlist.add(map);

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d("post response", "Finnish");

                        setList();

                        adapter = new NotificationAdapter(Notificationlist.this, allList);
                        mRecyclerView.setAdapter(adapter);

                        setupSearchView();


                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Log.d("namadhu top Size : ", "" + Constant.namadhu_top_news_Array.size());


                } catch (Exception ex) {
                    ex.printStackTrace();

                }
            }
        }).start();


    }

    private void getvalues() {
        AsyncHttpClient clien = new AsyncHttpClient();
        clien.get("https://u2mrf96skg.execute-api.ap-southeast-1.amazonaws.com/mobiro/notify/list?deviceid=12345", new AsyncHttpResponseHandler()

        {


            @Override
            public void onSuccess(String arg0) {
                super.onSuccess(arg0);


                Log.e("post response", arg0);


                try {
                    JSONArray respons = new JSONArray(arg0);


                    for (int i = 0; i < respons.length(); i++) {
                        HashMap<String, String> map = new HashMap<String, String>();
                        JSONObject jsonobject = respons.getJSONObject(i);
                        String notification = jsonobject.getString("Notification");

                        map.put("notification",notification);


                        Constant.notificationlist.add(map);


                        //Dataobject obj = new Dataobject(usernme, usrphone, userlt, userid, userproimg, pincode, "");
                        // Constant.results.add(obj);

                        // Toast.makeText(getApplicationContext(), "Data Found", Toast.LENGTH_LONG).show();
                        // mAdapter.notifyDataSetChanged();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


            @Override
            public void onFinish() {
                super.onFinish();
                //-- Toast.makeText(getApplicationContext(), "finish", Toast.LENGTH_SHORT).show();
                Log.d("post response", "Finnish");

                setList();

                adapter = new NotificationAdapter(Notificationlist.this, allList);
                mRecyclerView.setAdapter(adapter);

                setupSearchView();




                // mAdapter = new Insidenewsadapterr();
                // mRecyclerView.setAdapter(mAdapter);

            }

            @Override
            public void onFailure(Throwable arg0, String arg1) {
                super.onFailure(arg0, arg1);
                //Toast.makeText(getApplicationContext(), "failure", Toast.LENGTH_SHORT).show();
                Log.d("post response", "failure");
            }

        });

    }

    public void setList() {

        allList = new ArrayList<Notificationitem>();

        Notificationitem item = new Notificationitem();

        for (int i = 0; i < Constant.notificationlist.size(); i++) {
            item = new Notificationitem();
            item.setData(Constant.notificationlist.get(i).get("notification"),Constant.notificationlist.get(i).get("date"), Constant.notificationlist.get(i).get("time"), Constant.notificationlist.get(i).get("notification"));
            allList.add(item);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    private ArrayList<Dataobject> getDataSet() {
        ArrayList results = new ArrayList<Dataobject>();
        String banktype;

        AsyncHttpClient clien = new AsyncHttpClient();
        clien.get("https://u2mrf96skg.execute-api.ap-southeast-1.amazonaws.com/mobiro/videolist?fname="+Constant.username, new AsyncHttpResponseHandler()

        {


            @Override
            public void onSuccess(String arg0) {
                super.onSuccess(arg0);


                Log.e("post response", arg0);


                try {
                    JSONArray respons = new JSONArray(arg0);


                    for (int i = 0; i < respons.length(); i++) {

                        JSONObject jsonobject = respons.getJSONObject(i);
                        String userid = jsonobject.getString("name");
                        String usernme = jsonobject.getString("stream_url");
                        String usrphone = jsonobject.getString("bit_rate");
                        String userlt = jsonobject.getString("aspect_ratio");
                        String userproimg = jsonobject.getString("thumb_loc");
                        String pincode = jsonobject.getString("video_id");


                        Dataobject obj = new Dataobject(usernme, usrphone, userlt, userid, userproimg, pincode, "");
                        Constant.results.add(obj);

                        // Toast.makeText(getApplicationContext(), "Data Found", Toast.LENGTH_LONG).show();
                        mAdapter.notifyDataSetChanged();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


            @Override
            public void onFinish() {
                super.onFinish();
                //-- Toast.makeText(getApplicationContext(), "finish", Toast.LENGTH_SHORT).show();
                Log.d("post response", "Finnish");

            }

            @Override
            public void onFailure(Throwable arg0, String arg1) {
                super.onFailure(arg0, arg1);
                //Toast.makeText(getApplicationContext(), "failure", Toast.LENGTH_SHORT).show();
                Log.d("post response", "failure");
            }

        });

        return Constant.results;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        adapter.filter(newText);
        return true;
    }


    /*class Insidenewsadapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private int valtab=0;
        // List<EndangeredItem> mItems;
        private RecyclerView.LayoutManager mLayoutManager;
        private RecyclerView.Adapter mAdapter;

        String[] subcategory=new String[]{
                "Tanjavur","sivaganga","chennai","madurai","thiruchy","pudhucherry","thirunelvely"
        };


        String[] titles=new String[]{
                "தமிழகத்தில் மட்டும் அதிகளவு அவதூறு வழக்குகள் தொடரப்பட காரணம் என்ன?: உச்சநீதிமன்றம்",
                "7-வது ஊதியக் குழுவில் காணும் குறைகளை நிவர்த்தி செய்ய ஜி.கே.வாசன் வலியுறுத்தல்",
                "எய்ட்ஸ் தொற்றில்லாத வளமான தமிழகத்தை உருவாக்கிட முதல்வர் ஜெயலலிதா வேண்டுகோள்",
                "டேவிஸ் கோப்பை டென்னிஸ்: 79 ஆண்டுகளுக்கு பின் பிரிட்டன் அணி சாம்பியன்",
                "பிரான்ஸ் புறப்பட்டு சென்றார் பிரதமர் மோடி: பருவநிலை மாநாட்டில் பங்கேற்கிறார்",
                "தமிழக மழை, வெள்ள சேதங்கள் குறித்து ஒரு வாரத்தில் அறிக்கை சமர்ப்பிப்பு: மத்திய குழு",
                "முல்லைப் பெரியாறு அணையின் நீர்மட்டம் 138 அடியை தாண்டியது: துணைக் கண்காணிப்புக்குழு ஆய்வு",
                "தமிழகம் மற்றும் புதுச்சேரியில் அடுத்த 24 மணி நேரத்தில் மழை பெய்ய வாய்ப்பு",
                "மக்காவ் ஓபன் பேட்மிண்டன்: பி.வி. சிந்து இறுதிப் போட்டிக்கு முன்னேற்றம்",
                "சென்னை மெட்ரோ ரயில் திட்டம்: ரூ.1080 கோடி கடன் தருகிறது ஜப்பான்",
                " தலையில் முக்காடு போட்டு தஞ்சை விவசாயிகள் நூதன போராட்டம்",
                "சென்னை போக்குவரத்து நெரிசலுக்கு மெட்ரோ ரயில் பணிகளே காரணம்: மேயர் சைதை துரைசாமி பேச்சு",
                "வைகை அணையில் முதற்கட்ட வெள்ள அபாய எச்சரிக்கை : 4 வருடங்களுக்கு பிறகு 68.21 அடியாக உயர்வு",
                "ஜிஎஸ்டி மசோதாவை நிறைவேற்ற சோனியாவுடன் பிரதமர் மோடி ஆலோசனை ",
                "வங்கிச் சேவை தொடர்பான புகார் 11.2% உயர்வு: ரிசர்வ் வங்கி",
                "ஸ்ரீவைகுண்டம் அணையில் தூர்வாரும் பணி குறித்து அறிக்கை தாக்கல் செய்ய உத்தரவு",
                "கடலூர் மாவட்டத்தில் மத்தியக் குழு ஆய்வு: பாதிக்கப்பட்ட மக்களிடம் குறைகள் கேட்பு",
                "தயாநிதிக்கு சிபிஐ காவல் தேவையில்லை : உச்ச நீதிமன்றம்",
                "3வது டெஸ்டில் இந்திய அணி 124 ரன் வித்தியாத்தில் வெற்றி: 2-0 என தொடரை கைப்பற்றியது",
                "எமர்ஜென்சியை கொண்டு வந்தவர்கள் சகிப்பின்மை குறித்து பேசுவதாக ஜெட்லி விமர்சனம்",
                "வேற்றுமைகள் நிறைந்த இந்தியாவை அரசியல் சாசனமே ஒருங்கிணைக்கிறது: பிரதமர் மோடி",
                "கடலூர் மாவட்டத்தில் மக்கள் நலக்கூட்டியக்கத்தினர் ஆர்ப்பாட்டம்",
                "பாஜக புதிய நிர்வாகிகள் பட்டியல் : மாநில துணை தலைவராக நெப்போலியன் நியமனம்",
                "கருணாநிதியை சந்தித்தார் கோவன் : மது விலக்கிற்கு ஆதரவு தரக் கோரிக்கை",
                "பீகாரில் மது விற்பனைக்குத் தடை: வரும் ஏப்.1 முதல் அமல்"

        };

        String[] urls=new String[]{

                "http://dbkb23tiezgkg.cloudfront.net/1121/supreme_court_fb__small.png",
                "http://dbkb23tiezgkg.cloudfront.net/1119/vasan_fb__small.png",
                "http://dbkb23tiezgkg.cloudfront.net/1117/jaya_fb__small.png",

                "http://dbkb23tiezgkg.cloudfront.net/1050/modi_spech_in_parliment___small.jpg",
                "http://dbkb23tiezgkg.cloudfront.net/1041/makkal_nala_kootiyakkam__small.jpg",
                "http://dbkb23tiezgkg.cloudfront.net/1038/nepoliyan__small.jpg",
                "http://dbkb23tiezgkg.cloudfront.net/1033/kovan_meet__karunanidhi__small.png",
                "http://dbkb23tiezgkg.cloudfront.net/1030/26__small.png",

                "http://dbkb23tiezgkg.cloudfront.net/1095/23__small.png",
                "http://dbkb23tiezgkg.cloudfront.net/1078/metro__small.png",
                "http://dbkb23tiezgkg.cloudfront.net/1084/tanjore_potest__small.png",
                "http://dbkb23tiezgkg.cloudfront.net/1073/corpration_meet_chennai__small.png",
                "http://dbkb23tiezgkg.cloudfront.net/1086/theni_dam__small.png",
                "http://dbkb23tiezgkg.cloudfront.net/1081/modi_sonia_meet__small.png",
                "http://dbkb23tiezgkg.cloudfront.net/1057/rbi__small.jpg",
                "http://dbkb23tiezgkg.cloudfront.net/1058/sri_vaikunadm_dam__small.jpg",
                "http://dbkb23tiezgkg.cloudfront.net/1056/59__small.png",
                "http://dbkb23tiezgkg.cloudfront.net/1054/dayanithi_maran__small.jpg",
                "http://dbkb23tiezgkg.cloudfront.net/1069/india_vs_south_africa_3rd_test___small.jpg",
                "http://dbkb23tiezgkg.cloudfront.net/1053/arun_jaitley_in_parliment__small.jpg",
                "http://dbkb23tiezgkg.cloudfront.net/1050/modi_spech_in_parliment___small.jpg",
                "http://dbkb23tiezgkg.cloudfront.net/1041/makkal_nala_kootiyakkam__small.jpg",
                "http://dbkb23tiezgkg.cloudfront.net/1038/nepoliyan__small.jpg",
                "http://dbkb23tiezgkg.cloudfront.net/1033/kovan_meet__karunanidhi__small.png",
                "http://dbkb23tiezgkg.cloudfront.net/1030/26__small.png"

        };


        public Insidenewsadapter() {
            super();
            *//*mItems = new ArrayList<EndangeredItem>();
            EndangeredItem species = new EndangeredItem();
            species.setName("Amur Leopard");
            species.setThumbnail(R.drawable.ic_cast_dark);
            mItems.add(species);

            species = new EndangeredItem();
            species.setName("Black Rhino");
            species.setThumbnail(R.drawable.ic_cast_dark);
            mItems.add(species);

            species = new EndangeredItem();
            species.setName("Orangutan");
            species.setThumbnail(R.drawable.ic_cast_dark);
            mItems.add(species);

            species = new EndangeredItem();
            species.setName("Sea Lions");
            species.setThumbnail(R.drawable.ic_cast_dark);
            mItems.add(species);

            species = new EndangeredItem();
            species.setName("Indian Elephant");
            species.setThumbnail(R.drawable.ic_cast_dark);
            mItems.add(species);

            species = new EndangeredItem();
            species.setName("Giant Panda");
            species.setThumbnail(R.drawable.ic_cast_dark);
            mItems.add(species);

            species = new EndangeredItem();
            species.setName("Snow Leopard");
            species.setThumbnail(R.drawable.ic_cast_dark);
            mItems.add(species);

            species = new EndangeredItem();
            species.setName("Dolphin");
            species.setThumbnail(R.drawable.ic_cast_dark);
            mItems.add(species);*//*
        }

        class ViewHolderr extends RecyclerView.ViewHolder{

            //  public ImageView image_one,image_two,image_three,image_four;
            // public TextView text_one,text_two,text_three,text_four,sub_cat;
            public TextView opinion_text;
            public ImageView opinion_image;

            public ViewHolderr(View itemView) {
                super(itemView);


            *//*image_one = (ImageView)itemView.findViewById(R.id.image_one);
            image_two = (ImageView)itemView.findViewById(R.id.image_two);
            image_three = (ImageView)itemView.findViewById(R.id.image_three);
            image_four = (ImageView)itemView.findViewById(R.id.image_four);

            text_one = (TextView)itemView.findViewById(R.id.text_one);
            text_two = (TextView)itemView.findViewById(R.id.text_two);
            text_three = (TextView)itemView.findViewById(R.id.text_three);
            text_four = (TextView)itemView.findViewById(R.id.text_four);*//*

                opinion_text=(TextView) itemView.findViewById(R.id.username);
                opinion_image=(ImageView) itemView.findViewById(R.id.profileimg);
            }
        }


        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            *//*View v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.opinionitem, viewGroup, false);
            ViewHolderr viewHolder = new ViewHolderr(v);
            return viewHolder;*//*
            RecyclerView.ViewHolder viewHolderrr = null;


                View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.listusers, viewGroup, false);
                viewHolderrr = new ViewHolderr(v);



            return viewHolderrr;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int i) {
            // EndangeredItem nature = mItems.get(i);
            switch (viewHolder.getItemViewType()) {
                case 1: {


                    ViewHolderr viewHolders = (ViewHolderr) viewHolder;


            *//*viewHolder.sub_cat.setText(Constant.subcategory_list.get(i).get("subcategoryname"));
            //  viewHolder.imgThumbnail.setImageResource(nature.getThumbnail());
            viewHolder.insiderecycler.setHasFixedSize(true);
            // Constant.bitt= imageLoader.getInstance().loadImageSync("http://ptshort.s3.amazonaws.com/android/4020.png");

            // The number of Columns
            mLayoutManager = new GridLayoutManager(getApplicationContext(),2,0,true);


            viewHolder.insiderecycler.setLayoutManager(mLayoutManager);

            mAdapter = new Insidenewsadapter();
            viewHolder.insiderecycler.setAdapter(mAdapter);*//*


                    String opiniontitle=Constant.videolist.get(i).get("videoid");
                    String opinionimage=Constant.videolist.get(i).get("thumb");

                    viewHolders.opinion_text.setText(opiniontitle);
                    Picasso.with(getApplicationContext()).load(opinionimage).into(viewHolders.opinion_image);


                } case 2:

                {
                    break;
                }
            }

        }

        @Override
        public int getItemViewType(int position) {


                return 0;

        }

        @Override
        public int getItemCount() {


            return Constant.videolist.size();
        }


    }*/

   /* public class MyRecyclerViewAdapter extends RecyclerView
            .Adapter<MyRecyclerViewAdapter
            .DataObjectHolder> {
        private String LOG_TAG = "MyRecyclerViewAdapter_new";
        private ArrayList<Dataobject> Dataset;
        String bank_acctype;

        public MyRecyclerViewAdapter(ArrayList<Dataobject> dataSet) {
        }

        @Override
        public DataObjectHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

            Context context;
            View view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.listusers, viewGroup, false);
            DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
            return dataObjectHolder;
            //-- return null;
        }

        @Override
        public void onBindViewHolder(DataObjectHolder dataObjectHolder, int i) {

            dataObjectHolder.proimg1.setId(i);
            dataObjectHolder.proimg1.setTag(i);
            dataObjectHolder.callimg.setTag(i);
            dataObjectHolder.callimg.setId(i);

            dataObjectHolder.username.setText(Dataset.get(i).getmText1());
            dataObjectHolder.username.setTag(i);
            dataObjectHolder.userphone.setText(Dataset.get(i).getmText2());
            dataObjectHolder.username.setTag(i);
            dataObjectHolder.userloan.setText(Dataset.get(i).getmText3());
            dataObjectHolder.userloan.setTag(i);


        }

        @Override
        public int getItemCount() {
            return 0;
        }


        public class DataObjectHolder extends RecyclerView.ViewHolder
                implements View
                .OnClickListener {
            TextView username, userphone, userloan;

            ImageView callimg, proimg1;


            public DataObjectHolder(View View) {
                super(View);
                proimg1 = (ImageView) View.findViewById(R.id.profileimg);
                username = (TextView) View.findViewById(R.id.username);
                callimg = (ImageView) View.findViewById(R.id.callimg);
                // closetxt=(TextView)View.findViewById(R.id.closetxt);


                //--- Log.i(LOG_TAG, "Adding Listener");
                View.setOnClickListener(this);
            }

            public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {

                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.listusers, parent, false);
                DataObjectHolder dataObjectHolder = new DataObjectHolder(view);

                return null;
            }


            public void onBindViewHolder(DataObjectHolder holder, int position) {

                holder.proimg1.setId(position);
                holder.proimg1.setTag(position);
                holder.callimg.setTag(position);
                holder.callimg.setId(position);
                holder.username.setText(Dataset.get(position).getmText1());
                holder.username.setTag(position);
                holder.userphone.setText(Dataset.get(position).getmText2());
                holder.username.setTag(position);
                holder.userloan.setText(Dataset.get(position).getmText3());
                holder.userloan.setTag(position);

                String proimgs1 = Dataset.get(position).getProimg();

                //-- Picasso.with(Itembnk1_activity.this).load(proimgs1).placeholder(R.drawable.agentpro1).error(R.drawable.userpro1).into(holder.proimg1);


            }

            @Override
            public void onClick(View v) {

*//*
                myClickListener.onItemClick(getPosition(), v);*//*

            }

        }

    }
*/

    class Insidenewsadapterr  extends RecyclerView.Adapter<Insidenewsadapterr.ViewHolderr> implements Filterable {

        private int valtab=0;
        // List<EndangeredItem> mItems;
        private RecyclerView.LayoutManager mLayoutManager;
        private RecyclerView.Adapter mAdapter;

        String[] subcategory=new String[]{
                "Tanjavur","sivaganga","chennai","madurai","thiruchy","pudhucherry","thirunelvely"
        };


        String[] titles=new String[]{
                "தமிழகத்தில் மட்டும் அதிகளவு அவதூறு வழக்குகள் தொடரப்பட காரணம் என்ன?: உச்சநீதிமன்றம்",
                "7-வது ஊதியக் குழுவில் காணும் குறைகளை நிவர்த்தி செய்ய ஜி.கே.வாசன் வலியுறுத்தல்",
                "எய்ட்ஸ் தொற்றில்லாத வளமான தமிழகத்தை உருவாக்கிட முதல்வர் ஜெயலலிதா வேண்டுகோள்",
                "டேவிஸ் கோப்பை டென்னிஸ்: 79 ஆண்டுகளுக்கு பின் பிரிட்டன் அணி சாம்பியன்",
                "பிரான்ஸ் புறப்பட்டு சென்றார் பிரதமர் மோடி: பருவநிலை மாநாட்டில் பங்கேற்கிறார்",
                "தமிழக மழை, வெள்ள சேதங்கள் குறித்து ஒரு வாரத்தில் அறிக்கை சமர்ப்பிப்பு: மத்திய குழு",
                "முல்லைப் பெரியாறு அணையின் நீர்மட்டம் 138 அடியை தாண்டியது: துணைக் கண்காணிப்புக்குழு ஆய்வு",
                "தமிழகம் மற்றும் புதுச்சேரியில் அடுத்த 24 மணி நேரத்தில் மழை பெய்ய வாய்ப்பு",
                "மக்காவ் ஓபன் பேட்மிண்டன்: பி.வி. சிந்து இறுதிப் போட்டிக்கு முன்னேற்றம்",
                "சென்னை மெட்ரோ ரயில் திட்டம்: ரூ.1080 கோடி கடன் தருகிறது ஜப்பான்",
                " தலையில் முக்காடு போட்டு தஞ்சை விவசாயிகள் நூதன போராட்டம்",
                "சென்னை போக்குவரத்து நெரிசலுக்கு மெட்ரோ ரயில் பணிகளே காரணம்: மேயர் சைதை துரைசாமி பேச்சு",
                "வைகை அணையில் முதற்கட்ட வெள்ள அபாய எச்சரிக்கை : 4 வருடங்களுக்கு பிறகு 68.21 அடியாக உயர்வு",
                "ஜிஎஸ்டி மசோதாவை நிறைவேற்ற சோனியாவுடன் பிரதமர் மோடி ஆலோசனை ",
                "வங்கிச் சேவை தொடர்பான புகார் 11.2% உயர்வு: ரிசர்வ் வங்கி",
                "ஸ்ரீவைகுண்டம் அணையில் தூர்வாரும் பணி குறித்து அறிக்கை தாக்கல் செய்ய உத்தரவு",
                "கடலூர் மாவட்டத்தில் மத்தியக் குழு ஆய்வு: பாதிக்கப்பட்ட மக்களிடம் குறைகள் கேட்பு",
                "தயாநிதிக்கு சிபிஐ காவல் தேவையில்லை : உச்ச நீதிமன்றம்",
                "3வது டெஸ்டில் இந்திய அணி 124 ரன் வித்தியாத்தில் வெற்றி: 2-0 என தொடரை கைப்பற்றியது",
                "எமர்ஜென்சியை கொண்டு வந்தவர்கள் சகிப்பின்மை குறித்து பேசுவதாக ஜெட்லி விமர்சனம்",
                "வேற்றுமைகள் நிறைந்த இந்தியாவை அரசியல் சாசனமே ஒருங்கிணைக்கிறது: பிரதமர் மோடி",
                "கடலூர் மாவட்டத்தில் மக்கள் நலக்கூட்டியக்கத்தினர் ஆர்ப்பாட்டம்",
                "பாஜக புதிய நிர்வாகிகள் பட்டியல் : மாநில துணை தலைவராக நெப்போலியன் நியமனம்",
                "கருணாநிதியை சந்தித்தார் கோவன் : மது விலக்கிற்கு ஆதரவு தரக் கோரிக்கை",
                "பீகாரில் மது விற்பனைக்குத் தடை: வரும் ஏப்.1 முதல் அமல்"

        };

        String[] urls=new String[]{

                "http://dbkb23tiezgkg.cloudfront.net/1121/supreme_court_fb__small.png",
                "http://dbkb23tiezgkg.cloudfront.net/1119/vasan_fb__small.png",
                "http://dbkb23tiezgkg.cloudfront.net/1117/jaya_fb__small.png",

                "http://dbkb23tiezgkg.cloudfront.net/1050/modi_spech_in_parliment___small.jpg",
                "http://dbkb23tiezgkg.cloudfront.net/1041/makkal_nala_kootiyakkam__small.jpg",
                "http://dbkb23tiezgkg.cloudfront.net/1038/nepoliyan__small.jpg",
                "http://dbkb23tiezgkg.cloudfront.net/1033/kovan_meet__karunanidhi__small.png",
                "http://dbkb23tiezgkg.cloudfront.net/1030/26__small.png",

                "http://dbkb23tiezgkg.cloudfront.net/1095/23__small.png",
                "http://dbkb23tiezgkg.cloudfront.net/1078/metro__small.png",
                "http://dbkb23tiezgkg.cloudfront.net/1084/tanjore_potest__small.png",
                "http://dbkb23tiezgkg.cloudfront.net/1073/corpration_meet_chennai__small.png",
                "http://dbkb23tiezgkg.cloudfront.net/1086/theni_dam__small.png",
                "http://dbkb23tiezgkg.cloudfront.net/1081/modi_sonia_meet__small.png",
                "http://dbkb23tiezgkg.cloudfront.net/1057/rbi__small.jpg",
                "http://dbkb23tiezgkg.cloudfront.net/1058/sri_vaikunadm_dam__small.jpg",
                "http://dbkb23tiezgkg.cloudfront.net/1056/59__small.png",
                "http://dbkb23tiezgkg.cloudfront.net/1054/dayanithi_maran__small.jpg",
                "http://dbkb23tiezgkg.cloudfront.net/1069/india_vs_south_africa_3rd_test___small.jpg",
                "http://dbkb23tiezgkg.cloudfront.net/1053/arun_jaitley_in_parliment__small.jpg",
                "http://dbkb23tiezgkg.cloudfront.net/1050/modi_spech_in_parliment___small.jpg",
                "http://dbkb23tiezgkg.cloudfront.net/1041/makkal_nala_kootiyakkam__small.jpg",
                "http://dbkb23tiezgkg.cloudfront.net/1038/nepoliyan__small.jpg",
                "http://dbkb23tiezgkg.cloudfront.net/1033/kovan_meet__karunanidhi__small.png",
                "http://dbkb23tiezgkg.cloudfront.net/1030/26__small.png"

        };


        public Insidenewsadapterr() {
            super();
            /*mItems = new ArrayList<EndangeredItem>();
            EndangeredItem species = new EndangeredItem();
            species.setName("Amur Leopard");
            species.setThumbnail(R.drawable.ic_cast_dark);
            mItems.add(species);

            species = new EndangeredItem();
            species.setName("Black Rhino");
            species.setThumbnail(R.drawable.ic_cast_dark);
            mItems.add(species);

            species = new EndangeredItem();
            species.setName("Orangutan");
            species.setThumbnail(R.drawable.ic_cast_dark);
            mItems.add(species);

            species = new EndangeredItem();
            species.setName("Sea Lions");
            species.setThumbnail(R.drawable.ic_cast_dark);
            mItems.add(species);

            species = new EndangeredItem();
            species.setName("Indian Elephant");
            species.setThumbnail(R.drawable.ic_cast_dark);
            mItems.add(species);

            species = new EndangeredItem();
            species.setName("Giant Panda");
            species.setThumbnail(R.drawable.ic_cast_dark);
            mItems.add(species);

            species = new EndangeredItem();
            species.setName("Snow Leopard");
            species.setThumbnail(R.drawable.ic_cast_dark);
            mItems.add(species);

            species = new EndangeredItem();
            species.setName("Dolphin");
            species.setThumbnail(R.drawable.ic_cast_dark);
            mItems.add(species);*/
        }

        @Override
        public ViewHolderr onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.listusers, viewGroup, false);
            ViewHolderr viewHolder = new ViewHolderr(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolderr viewHolder, final int i) {
            // EndangeredItem nature = mItems.get(i);



            /*viewHolder.sub_cat.setText(Constant.subcategory_list.get(i).get("subcategoryname"));
            //  viewHolder.imgThumbnail.setImageResource(nature.getThumbnail());
            viewHolder.insiderecycler.setHasFixedSize(true);
            // Constant.bitt= imageLoader.getInstance().loadImageSync("http://ptshort.s3.amazonaws.com/android/4020.png");

            // The number of Columns
            mLayoutManager = new GridLayoutManager(getApplicationContext(),2,0,true);


            viewHolder.insiderecycler.setLayoutManager(mLayoutManager);

            mAdapter = new Insidenewsadapter();
            viewHolder.insiderecycler.setAdapter(mAdapter);*/

            String programmestitle=Constant.videolist.get(i).get("name");
            String programmeimage=Constant.videolist.get(i).get("thumb");


            viewHolder.programme_text.setText(programmestitle);
            Picasso.with(getApplicationContext()).load(programmeimage).into(viewHolder.programme_image);

            viewHolder.listclick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int id=v.getId();
                    String urlvideo= Constant.videolist.get(i).get("vidurl");

                    Intent inten=new Intent(getApplicationContext(),Videoview.class);
                    inten.putExtra("videourl", urlvideo);
                    startActivity(inten);

                }
            });


            viewHolder.edit_video.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String videoid= Constant.videolist.get(i).get("videoid");
                    String videotitle=Constant.videolist.get(i).get("vidtitle");
                    String videodesc=Constant.videolist.get(i).get("viddesc");
                    String videotag=Constant.videolist.get(i).get("vidtag");
                    Intent inten=new Intent(getApplicationContext(),Videoedit.class);
                    inten.putExtra("videoid", videoid);
                    inten.putExtra("vidtitle",videotitle);
                    inten.putExtra("viddesc",videodesc);
                    inten.putExtra("vidtag",videotag);
                    startActivity(inten);

                }
            });



        }



        @Override
        public int getItemCount() {


            return Constant.videolist.size();
        }

        @Override
        public Filter getFilter() {
            return null;
        }

        class ViewHolderr extends RecyclerView.ViewHolder{

            //  public ImageView image_one,image_two,image_three,image_four;
            // public TextView text_one,text_two,text_three,text_four,sub_cat;
            public TextView programme_text;
            public ImageView programme_image;
            public LinearLayout listclick;
            public TextView edit_video;

            public ViewHolderr(View itemView) {
                super(itemView);
            /*image_one = (ImageView)itemView.findViewById(R.id.image_one);
            image_two = (ImageView)itemView.findViewById(R.id.image_two);
            image_three = (ImageView)itemView.findViewById(R.id.image_three);
            image_four = (ImageView)itemView.findViewById(R.id.image_four);

            text_one = (TextView)itemView.findViewById(R.id.text_one);
            text_two = (TextView)itemView.findViewById(R.id.text_two);
            text_three = (TextView)itemView.findViewById(R.id.text_three);
            text_four = (TextView)itemView.findViewById(R.id.text_four);*/

                programme_text=(TextView) itemView.findViewById(R.id.username);
                programme_image=(ImageView) itemView.findViewById(R.id.profileimg);
                listclick=(LinearLayout) itemView.findViewById(R.id.vid_rel);
                edit_video=(TextView) itemView.findViewById(R.id.edit_desc);
            }
        }
    }

}
