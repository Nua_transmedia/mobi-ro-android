/*

package com.myappbuilder.wgstream;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.v4.view.GestureDetectorCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.amazonmobileanalytics.InitializationException;
import com.amazonaws.mobileconnectors.amazonmobileanalytics.MobileAnalyticsManager;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.CreatePlatformEndpointRequest;
import com.amazonaws.services.sns.model.CreatePlatformEndpointResult;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.myappbuilder.wgstream.config.ConfigPrefs;
import com.myappbuilder.wgstream.ui.AutoFocusListener;
import com.myappbuilder.wgstream.ui.MultiStateButton;
import com.myappbuilder.wgstream.ui.TimerView;
import com.wowza.gocoder.sdk.api.devices.WZCamera;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;


public class CameraActivity extends CameraActivityBase {
    private final static String TAG = CameraActivity.class.getSimpleName();

    // UI controls
    protected MultiStateButton mBtnSwitchCamera  = null;
    protected MultiStateButton      mBtnTorch         = null;
    protected TimerView mTimerView        = null;

    private ImageView vediolist, notifilist;
    private ProgressDialog prg_bar;
    private String gcmregisterid;
    private static TextView noti_count;
    private String latitude,longtidude;
    private static AlertDialog alertDialog;


    private String deviceidval, osversion, devicename, imeinumber,osname;

    // Gestures are used to toggle the focus modes
    protected GestureDetectorCompat mAutoFocusDetector = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cameraa);

       // findViewbyid();




        mRequiredPermissions = new String[] {
                Manifest.permission.CAMERA,
                Manifest.permission.RECORD_AUDIO
        };

        // Initialize the UI controls
        mBtnTorch           = (MultiStateButton) findViewById(R.id.ic_torch);
        mBtnSwitchCamera    = (MultiStateButton) findViewById(R.id.ic_switch_camera);
        mTimerView          = (TimerView) findViewById(R.id.txtTimer);


        alertDialog = new AlertDialog.Builder(CameraActivity.this
        ).create();


        */
/*prg_bar = new ProgressDialog(CameraActivity.this);
        prg_bar.setMessage("Please Wait");
        prg_bar.setIndeterminate(true);
        prg_bar.setCancelable(false);
        prg_bar.setMax(100);
        prg_bar.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        String device_id = android.provider.Settings.Secure.getString(getContentResolver(),
                android.provider.Settings.Secure.ANDROID_ID);
        Log.d("device id", device_id);
        deviceidval=device_id;


        int sdkVersion = android.os.Build.VERSION.SDK_INT;

        StringBuilder builder = new StringBuilder();


        Field[] fields = Build.VERSION_CODES.class.getFields();
        for (Field field : fields) {
            String fieldName = field.getName();
            int fieldValue = -1;

            try {
                fieldValue = field.getInt(new Object());
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            if (fieldValue == Build.VERSION.SDK_INT) {
                builder.append(fieldName);
            }
        }


        osname=builder.toString();


        osversion=String.valueOf(sdkVersion);

        devicename = android.os.Build.MANUFACTURER;

        Log.d("osversion",osversion+osname);




        imeinumber="";

*//*


        Constant.cd = new ConnectionDetector(CameraActivity.this);
        Constant.isInternetPresent = Constant.cd.isConnectingToInternet();
        if (Constant.isInternetPresent) {




            //uncommand
          */
/*  try {
                analytics = MobileAnalyticsManager.getOrCreateInstance(
                        CameraActivity.this,
                        "dc6a313d22004de994c1fd81540e9992", //Amazon Mobile Analytics App ID
                        "us-east-1:11a1ae4f-18c0-45af-8528-de03b04760d0" //Amazon Cognito Identity Pool ID
                );
                this.onLevelComplete("Live Streaming Screen", 0, 0);
            } catch(InitializationException ex) {
                Log.e(this.getClass().getName(), "Failed to initialize Amazon Mobile Analytics", ex);
            }*//*

            //uncommand


            //GCMClientManager pushClientManager = new GCMClientManager(this, "961701430859");
            // GCMClientManager pushClientManager = new GCMClientManager(this, "1080106508112");
            */
/*GCMClientManager pushClientManager = new GCMClientManager(this, "378610203051");
            pushClientManager.registerIfNeeded(new GCMClientManager.RegistrationCompletedHandler() {
                @Override
                public void onSuccess(String registrationId, boolean isNewRegistration) {

                    Log.d("Registration id", registrationId);

                    gcmregisterid = registrationId;


                    if(gcmregisterid!=null){
                        senddeviceinfo(gcmregisterid,deviceidval,devicename,osversion,imeinumber,osname);
                        getLocation();
                    }





                    if(gcmregisterid!=null)
                    {

                        AsyncHttpClient clien = new AsyncHttpClient();
                        clien.get("http://nuaworks.com/church/sample/token.php?d_type=1&d_token="+gcmregisterid+"&d_id="+deviceidval, new AsyncHttpResponseHandler()

                        {


                            @Override
                            public void onSuccess(String arg0) {
                                super.onSuccess(arg0);


                                Log.e("post response", arg0);


                            }


                            @Override
                            public void onFinish() {
                                super.onFinish();
                                //-- Toast.makeText(getApplicationContext(), "finish", Toast.LENGTH_SHORT).show();
                                Log.d("post response", "Finnish");

                            }

                            @Override
                            public void onFailure(Throwable arg0, String arg1) {
                                super.onFailure(arg0, arg1);
                                //Toast.makeText(getApplicationContext(), "failure", Toast.LENGTH_SHORT).show();
                                Log.d("post response", "failure");
                            }

                        });

                    }


                }

                @Override
                public void onFailure(String ex) {
                    super.onFailure(ex);
                    //   Toast.makeText(getApplicationContext(),"failure:"+ex,Toast.LENGTH_SHORT).show();
                }
            });*//*

        }else{

        }



        */
/*vediolist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constant.cd = new ConnectionDetector(getApplicationContext());
                Constant.isInternetPresent = Constant.cd.isConnectingToInternet();
                if (Constant.isInternetPresent) {
                    prg_bar.show();
                    Constant.videolist.clear();
                    getvalues();
                }else{
                    toastsettext("No Internet Connection");
                }
            }
        });


        notifilist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Intent inten=new Intent(getApplicationContext(),Notificationlist.class);
                //startActivity(inten);
                Constant.cd = new ConnectionDetector(getApplicationContext());
                Constant.isInternetPresent = Constant.cd.isConnectingToInternet();
                if (Constant.isInternetPresent) {
                    prg_bar.show();
                    getnotification();
                }  else{
                    toastsettext("No Internet Connection");
                }
            }
        });
*//*


    }



    public void onLogout(View v){

        Constant.dayee = getSharedPreferences("dayfun", MODE_PRIVATE);
        Constant.daycom = Constant.dayee.edit();
        Constant.daycom.putBoolean("login",false);

        Constant.daycom.commit();

        Intent inten =new Intent(getApplicationContext(),Login.class);
        startActivity(inten);
        CameraActivity.this.finish();
    }


    private void senddeviceinfo(final String gcmregisteridd, final String deviceidval, final String devicename, final String osversion, final String imeinumber, final String osname) {


        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
//AKIAJQWIJHZX4MASWLHQ
                    //Gpj/qODpd1ShHBi3kHfBk5laAMjENLUnuSD5AkMV

                    //device info update
                    AWSCredentials awsCredentials = new BasicAWSCredentials("AKIAJQWIJHZX4MASWLHQ", "Gpj/qODpd1ShHBi3kHfBk5laAMjENLUnuSD5AkMV");
                    //  AWSCredentials awsCredentials = new BasicAWSCredentials("AKIAI7ITZC6KJYC3SQPA", "gQwl1AEdJKTFQ45jqRsG/asePviFIxWqyHlrV5+s");

                    String platformApplicationArn = "arn:aws:sns:ap-southeast-1:684719585714:app/GCM/serverkey";
                    AmazonSNSClient pushClient = new AmazonSNSClient(awsCredentials);
                    pushClient.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_1));

                    String customPushData = Constant.username;
                    CreatePlatformEndpointRequest platformEndpointRequest = new CreatePlatformEndpointRequest();
                    platformEndpointRequest.setCustomUserData(customPushData);
                    platformEndpointRequest.setToken(gcmregisteridd);

                    platformEndpointRequest.setPlatformApplicationArn(platformApplicationArn);
                    pushClient.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_1));
                    CreatePlatformEndpointResult result = pushClient.createPlatformEndpoint(platformEndpointRequest);

                    Log.d("autherror", "Amazon Push reg result: " + result);

                    //device info update




                    WebClient webClient = new WebClient();
                    String getRes = null;
                    try {
                        Log.d("rer","https://u2mrf96skg.execute-api.ap-southeast-1.amazonaws.com/mobiro/device?uname="+Constant.username+"&device_id="+gcmregisteridd+"&device_model=Android&version="+osversion+osname+"&imei="+imeinumber+"&os=Android");
                        getRes = webClient.doGetReq("https://u2mrf96skg.execute-api.ap-southeast-1.amazonaws.com/mobiro/device?uname="+Constant.username+"&device_id="+gcmregisteridd+"&device_model=Android&version="+osversion+osname+"&imei="+imeinumber+"&os=Android");
                        Log.d("Home_page_Array :", getRes);

                        Log.e("post response", getRes);


                        try {
                            JSONArray respons = new JSONArray(getRes);
                            Log.d("res", String.valueOf(respons));






                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    //  Log.d("namadhu top Size : ", "" + Constant.namadhu_top_news_Array.size());


                } catch (Exception ex) {
                    ex.printStackTrace();

                }
            }
        }).start();


    }



    private void sendlocationinfo(final String latitude, final String longitude, final String deviceidval){

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    WebClient webClient = new WebClient();
                    String getRes = null;
                    try {
                        getRes = webClient.doGetReq("https://u2mrf96skg.execute-api.ap-southeast-1.amazonaws.com/mobiro/device/geo?email="+Constant.username+"@mobi-ro.com&deviceid="+gcmregisterid+"&Latitude="+latitude+"&Longitude="+longitude);
                        Log.d("locationapi", "https://u2mrf96skg.execute-api.ap-southeast-1.amazonaws.com/mobiro/device/geo?email="+Constant.username+"@mobi-ro.com&deviceid="+gcmregisterid+"&Latitude="+latitude+"&Longitude="+longitude);



                        Log.e("locationlog", getRes);


                        try {
                            JSONArray respons = new JSONArray(getRes);
                            Log.d("res", String.valueOf(respons));






                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    //  Log.d("namadhu top Size : ", "" + Constant.namadhu_top_news_Array.size());


                } catch (Exception ex) {
                    ex.printStackTrace();

                }
            }
        }).start();

    }



    private void getnotification(){

        Constant.notificationlist.clear();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    WebClient webClient = new WebClient();
                    String getRes = null;
                    try {
                        //https://u2mrf96skg.execute-api.ap-southeast-1.amazonaws.com/mobiro/notify/list?deviceid=dc5afb4269e79563d692ea386c28dfd9a6aa2190c0a8cf4164ec26406c8e6211
                        getRes = webClient.doGetReq("https://u2mrf96skg.execute-api.ap-southeast-1.amazonaws.com/mobiro/notify/list?deviceid="+gcmregisterid);
                        Log.d("Home_page_Array :", getRes);

                        Log.e("post response", getRes);


                        try {
                            JSONArray respons = new JSONArray(getRes);


                            for (int i = 0; i < respons.length(); i++) {
                                HashMap<String, String> map = new HashMap<String, String>();
                                JSONObject jsonobject = respons.getJSONObject(i);
                                String notification = jsonobject.getString("messsage");
                                String date =jsonobject.getString("date");
                                String time=jsonobject.getString("time");


                                map.put("notification",notification);
                                map.put("date",date);
                                map.put("time",time);


                                Constant.notificationlist.add(map);

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d("post response", "Finnish");

                        Intent inten=new Intent(getApplicationContext(),Notificationlist.class);
                        startActivity(inten);

                        if(noti_count.getVisibility()==View.VISIBLE){
                            noti_count.setVisibility(View.GONE);
                        }else{

                        }



                        if(prg_bar.isShowing()){
                            prg_bar.dismiss();
                            prg_bar.cancel();
                        }else{

                        }

                        // setList();

                        //adapter = new NotificationAdapter(Notificationlist.this, allList);
                        // mRecyclerView.setAdapter(adapter);

                        //setupSearchView();


                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Log.d("namadhu top Size : ", "" + Constant.namadhu_top_news_Array.size());


                } catch (Exception ex) {
                    ex.printStackTrace();

                }
            }
        }).start();


    }



    private void getLocation() {




        AsyncHttpClient clien = new AsyncHttpClient();
        clien.post("https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyDjmM0FLyUX14RIFq8O5ZU5KvB01BFYHJU", new AsyncHttpResponseHandler()

        {


            @Override
            public void onSuccess(String arg0) {
                super.onSuccess(arg0);


                Log.e("post response", arg0);
                JSONObject respons = null;
                try {
                    respons = new JSONObject(arg0);
                    JSONObject response2=respons.getJSONObject("location");
                    latitude=response2.getString("lat");
                    longtidude=response2.getString("lng");
                } catch (JSONException e) {
                    e.printStackTrace();
                }




            }


            @Override
            public void onFinish() {
                super.onFinish();
                //-- Toast.makeText(getApplicationContext(), "finish", Toast.LENGTH_SHORT).show();
                Log.d("post response", "Finnish");
                sendlocationinfo(latitude, longtidude, gcmregisterid);

            }

            @Override
            public void onFailure(Throwable arg0, String arg1) {
                super.onFailure(arg0, arg1);
                //Toast.makeText(getApplicationContext(), "failure", Toast.LENGTH_SHORT).show();
                Log.d("post response", "failure");
            }

        });






        */
/*new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    WebClient webClient = new WebClient();
                    String getRes = null;
                    try {
                        //https://u2mrf96skg.execute-api.ap-southeast-1.amazonaws.com/mobiro/notify/list?deviceid=dc5afb4269e79563d692ea386c28dfd9a6aa2190c0a8cf4164ec26406c8e6211
                        getRes = webClient.doGetReq("https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyCZpuliwNnVNbGIrOv9lZ2kYeVtxgPawuA");
                        Log.d("Home_page_Array :", getRes);

                        Log.e("post response", getRes);
                        String latitude = null;
                        String longtidude = null;


                        try {
                            JSONObject respons = new JSONObject(getRes);

                            JSONObject response2=respons.getJSONObject("location");
                           latitude=response2.getString("lat");
                            longtidude=response2.getString("lng");





                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        sendlocationinfo(latitude, longtidude, gcmregisterid);


                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                } catch (Exception ex) {
                    ex.printStackTrace();

                }
            }
        }).start();*//*


    }

    private void getvalues() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    WebClient webClient = new WebClient();
                    String getRes = null;
                    try {
                        getRes = webClient.doGetReq("https://u2mrf96skg.execute-api.ap-southeast-1.amazonaws.com/mobiro/videolist?fname="+Constant.username);
                        Log.d("Home_page_Array :", getRes);

                        Log.e("post response", getRes);


                        try {
                            JSONArray respons = new JSONArray(getRes);


                            for (int i = 0; i < respons.length(); i++) {
                                HashMap<String, String> map = new HashMap<String, String>();
                                JSONObject jsonobject = respons.getJSONObject(i);
                                String name = jsonobject.getString("name");
                                String url = jsonobject.getString("stream_url");
                                String bitrate = jsonobject.getString("bit_rate");
                                String ratio = jsonobject.getString("aspect_ratio");
                                String thumb = jsonobject.getString("thumb_loc");
                                String videoid = jsonobject.getString("video_id");
                                String timevalue=jsonobject.getString("time_showing");


                                String date=jsonobject.getString("created_at");

                                String videotitle=null;
                                String videodescription=null;
                                String videotype=null;
                                String videotag="";

                                if(jsonobject.has("Tags")){
                                    videotag=jsonobject.getString("Tags");
                                }
                                if(jsonobject.has("Title"))
                                {
                                    videotitle=jsonobject.getString("Title");
                                }

                                if(jsonobject.has("Description"))
                                {
                                    videodescription = jsonobject.getString("Description");
                                }

                                if(jsonobject.has("VideoType"))
                                {
                                    videotype=jsonobject.getString("VideoType");
                                }




                                //  String videodescription=jsonobject.getString("Description");
                                // String videotag=jsonobject.getString("VideoType");
                                map.put("videoid",videoid);
                                map.put("thumb",thumb);
                                map.put("name",name);
                                map.put("vidurl",url);
                                map.put("time",timevalue);
                                map.put("date",date);



                                map.put("vidtitle",videotitle);
                                map.put("viddesc",videodescription);
                                map.put("vidtype",videotype);
                                map.put("vidtag",videotag);


                                Constant.videolist.add(map);


                                //Dataobject obj = new Dataobject(usernme, usrphone, userlt, userid, userproimg, pincode, "");
                                // Constant.results.add(obj);

                                // Toast.makeText(getApplicationContext(), "Data Found", Toast.LENGTH_LONG).show();
                                // mAdapter.notifyDataSetChanged();
                            }

                            // setList();
                            Intent inten=new Intent(getApplicationContext(),Vedio_list.class);
                            startActivity(inten);
                            prg_bar.dismiss();

                            //WZMediaConfig configs[] = getVideoConfigs(mGoCoderCameraView);

                            */
/*Intent intent = new Intent(getApplicationContext(), Profilepreference.class);
                            //intent.putExtra("header_resource", ConfigPrefsActivity.ALL_PREFS);
                            // intent.putExtra("configs", configs);
                            startActivity(intent);*//*



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Log.d("namadhu top Size : ", "" + Constant.namadhu_top_news_Array.size());


                } catch (Exception ex) {
                    ex.printStackTrace();

                }
            }
        }).start();


    }


    public void toastsettext(String string1) {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.altdialog_toast,
                (ViewGroup) findViewById(R.id.toast_rl));
        TextView txt = (TextView) layout.findViewById(R.id.toast_txt);
        txt.setText(string1);
        Toast tst = new Toast(getApplicationContext());
        tst.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        tst.setDuration(Toast.LENGTH_SHORT);
        tst.setView(layout);
        tst.show();
    }

    private void findViewbyid() {
        vediolist = (ImageView) findViewById(R.id.vedio_sett);
        notifilist = (ImageView) findViewById(R.id.notifi);
        noti_count=(TextView) findViewById(R.id.noticounttxt);

    }

    */
/**
     * Android Activity lifecycle methods
     *//*

    @Override
    protected void onResume() {
        super.onResume();

        if (sGoCoderSDK != null && mAutoFocusDetector == null) {
            mAutoFocusDetector = new GestureDetectorCompat(this, new AutoFocusListener(this, mWZCameraView));
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    */
/**
     * Click handler for the switch camera button
     *//*

    public void onSwitchCamera(View v) {
        if (mWZCameraView == null) return;

        WZCamera newCamera = mWZCameraView.switchCamera();
        if (newCamera != null)
            ConfigPrefs.setActiveCamera(PreferenceManager.getDefaultSharedPreferences(this), newCamera.getCameraId());

        boolean hasTorch = (newCamera != null && newCamera.hasCapability(WZCamera.TORCH));
        if (hasTorch)
            mBtnTorch.setState(newCamera.isTorchOn());
        else
            mBtnTorch.setState(false);
        mBtnTorch.setEnabled(hasTorch);
    }

    */
/**
     * Click handler for the torch/flashlight button
     *//*

    public void onToggleTorch(View v) {
        if (mWZCameraView == null) return;

        WZCamera activeCamera = mWZCameraView.getCamera();
        activeCamera.setTorchOn(mBtnTorch.toggleState());
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (mAutoFocusDetector != null)
            mAutoFocusDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    */
/**
     * Update the state of the UI controls
     *//*

    @Override
    protected boolean syncUIControlState() {
        return false;
    }

    public static void updatenoticount(int valuecount) {

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                noti_count.setVisibility(View.VISIBLE);
                noti_count.setText(String.valueOf(Constant.notificationcount));

                displaydialog();

            }
        });
    }


    private static void displaydialog() {



        // Showing Alert Message




        alertDialog.setTitle("Mobiro");
        alertDialog.setIcon(R.drawable.ic_launcher);
        alertDialog.setMessage(Constant.pushmessage);
        alertDialog.show();



        new CountDownTimer(2000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onFinish() {
                // TODO Auto-generated method stub

                alertDialog.dismiss();
            }
        }.start();


    }

}
*/
