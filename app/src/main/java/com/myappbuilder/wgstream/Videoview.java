package com.myappbuilder.wgstream;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.VideoView;

/**
 * Created by nua-android on 30/5/16.
 * This code and all components (c) Copyright 2016-2017, NuaTransMedia,. All rights reserved.
 */
public class Videoview extends Activity implements MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnInfoListener {

    private VideoView videoView;
    private ProgressDialog prg_bar;
    private RelativeLayout vid_cntin;
    private ViewGroup.LayoutParams originalContainerLayoutParams,videoparams;
    private Button btn_close;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.videoview);



       // this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        videoView = (VideoView) findViewById(R.id.videoview);
        vid_cntin=(RelativeLayout) findViewById(R.id.vid_cntin);
        btn_close=(Button) findViewById(R.id.btn_close);

        originalContainerLayoutParams = vid_cntin.getLayoutParams();

        prg_bar = new ProgressDialog(Videoview.this);
        prg_bar.setMessage("Please Wait");
        prg_bar.setIndeterminate(true);
        prg_bar.setCancelable(false);
        prg_bar.setMax(100);
        prg_bar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        prg_bar.show();

        Intent intent = getIntent();
        String videourl = intent.getExtras().getString("videourl");

        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Videoview.this.finish();
            }
        });


       /* if (Videoview.this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){




        } else {

            vid_cntin.setLayoutParams(Util.getLayoutParamsBasedOnParent(
                    vid_cntin,
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
            ));
        }
*/

        videoView.setVideoURI(Uri.parse(videourl));

        videoView.requestFocus();
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

            @Override
            public void onPrepared(MediaPlayer mp) {
                // TODO Auto-generated method stub
                //	progDailog.dismiss();

                if(prg_bar.isShowing()){
                    prg_bar.dismiss();
                }else{

                }

                mp.setOnBufferingUpdateListener(Videoview.this);
                mp.setOnInfoListener(Videoview.this);

                MediaController mediaController = new
                        MediaController(Videoview.this);
                mediaController.setAnchorView(videoView);
                videoView.setMediaController(mediaController);

                videoView.start();


            }
        });
    }



    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {

    }

    @Override
    public boolean onInfo(MediaPlayer mp, int what, int extra) {
        return false;
    }
}
