package com.myappbuilder.wgstream;


import android.app.Activity;
import android.content.SharedPreferences;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by nua-android on 30/5/16.
 * This code and all components (c) Copyright 2016-2017, NuaTransMedia,. All rights reserved.
 */
public class Constant
{


	public static String[] celebrities = {
			"CK Default 480x640",
			"CK Low 270x480",
			"CK Medium 360x640",
			"CK High 540x960",
			"CK HD 720 720x1280",
			"CK HD 1080 1080x1920",
			"Add Profile"

	};


	public static String[] profilenames=new String[]{};

	public static ArrayList<String> days = new ArrayList<String>();
	public static ArrayList<String> tabs = new ArrayList<String>();

	public static ArrayList<String> location = new ArrayList<>();

	public static ArrayList<HashMap<String, String>> videolist=new ArrayList<HashMap<String,String>>();
	public static ArrayList<HashMap<String, String>> profilelist=new ArrayList<HashMap<String,String>>();
	public static ArrayList<HashMap<String, String>> notificationlist=new ArrayList<HashMap<String,String>>();
	public static ArrayList<HashMap<String, String>> locationlist=new ArrayList<HashMap<String,String>>();

	public static String username="";

	public static boolean more_bool=false;


	public static ArrayList results = new ArrayList<Dataobject>();

	public static ArrayList<String> aList = new ArrayList<String>();


	public static ArrayList<HashMap<String, String>> New_database =new ArrayList<HashMap<String,String>>();


	public static ArrayList<HashMap<String, String>> namadhu_top_news_Array =new ArrayList<HashMap<String,String>>();


	//--public static ConnectionDetector cd1;
	public static ConnectionDetector cd;
	public static boolean isInternetPresent;

	public static SharedPreferences dayee = null;
	public static SharedPreferences.Editor daycom = null;

	public static SharedPreferences media = null;
	public static SharedPreferences.Editor mediacom = null;


	public static SharedPreferences notification_pref = null;
	public static SharedPreferences.Editor set_notification_permission = null;

	public static String registerationid;
	public static String deviceid = "";


	public static int vals=0;
	public static int notificationcount=0;



	public static String category="music";
	public static String allnames="Feed16";
	public static String password="";

	public static boolean initial_enter=true;
	public static String pushmessage;
	public static String locvalue="Not Available";


	public static void hideSoftKeyboard(Activity activity, View v) {
		InputMethodManager imm = (InputMethodManager) activity
				.getSystemService(Activity.INPUT_METHOD_SERVICE);
		if (v != null) {
			imm.hideSoftInputFromWindow(v.getApplicationWindowToken(),
					InputMethodManager.HIDE_NOT_ALWAYS);
		}

	}
}
