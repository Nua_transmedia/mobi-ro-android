package com.myappbuilder.wgstream;

import android.app.Activity;
import android.app.LauncherActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.DimenRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by nua-android on 30/5/16.
 * This code and all components (c) Copyright 2016-2017, NuaTransMedia,. All rights reserved.
 */
public class Vedio_list extends Activity implements  SearchView.OnQueryTextListener {


    private RecyclerView mRecyclerView,gridrecyclerview;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<ListItem> allList;
    private SearchView mSearchView;

    private Button back_edit;

    private ToggleButton listgrid;

    static Vedio_list videolist;

    private ImageLoader imageLoader;


    private MyRecyclerAdapter adapter;

    private MyGridAdapter gridadapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_list);

        mSearchView=(SearchView) findViewById(R.id.videosearch);
        listgrid=(ToggleButton) findViewById(R.id.gridchange);

        videolist = this;

        mSearchView.setFocusable(false);



        View view = Vedio_list.this.getCurrentFocus();
        if (view != null) {
            Constant.hideSoftKeyboard(Vedio_list.this, view);
        }else{
            Constant.hideSoftKeyboard(Vedio_list.this,view);
        }

       // Toast.makeText(getApplicationContext(), String.valueOf(Constant.videolist.size()), Toast.LENGTH_LONG).show();
        listgrid.setText(null);
        listgrid.setTextOn(null);
        listgrid.setTextOff(null);
        listgrid.setBackgroundResource(R.drawable.gridlist);

        listgrid.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked==true){
                  //  listgrid.setBackgroundResource(R.drawable.cal_img);
                   // listgrid.setText("");
                    mRecyclerView.setVisibility(View.VISIBLE);
                    gridrecyclerview.setVisibility(View.GONE);
                }else{
                   // listgrid.setBackgroundResource(R.drawable.clock);
                   // listgrid.setText("");
                    mRecyclerView.setVisibility(View.GONE);
                    gridrecyclerview.setVisibility(View.VISIBLE);
                }
            }
        });

        int spanCount = 3; // 3 columns
        int spacing = 20; // 50px
        boolean includeEdge = false;

        gridrecyclerview=(RecyclerView) findViewById(R.id.grid_recyclerview);

       // int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.spacing);
       // gridrecyclerview.addItemDecoration(new GridSpacingItemDecoration(spanCount,spacing,includeEdge));

       // mLayoutManager = new GridLayoutManager(Vedio_list.this,3,1, false);

       // gridrecyclerview.setLayoutManager(mLayoutManager);


        gridrecyclerview.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getApplicationContext(), R.dimen.item_offset);
        gridrecyclerview.addItemDecoration(itemDecoration);


        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_addnew);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        back_edit=(Button) findViewById(R.id.back_edit);

        back_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String searchword= String.valueOf(mSearchView.getQuery());

                if(searchword.length()!=0){
                    mSearchView.setQuery("",true);

                }else{
                    finish();
                }

               // Constant.hideSoftKeyboard(Vedio_list.this, v);
               // finish();

            }
        });

        setList();




        adapter = new MyRecyclerAdapter(Vedio_list.this, allList);
        mRecyclerView.setAdapter(adapter);

        gridadapter = new MyGridAdapter(Vedio_list.this,allList);
        gridrecyclerview.setAdapter(gridadapter);

        setupSearchView();





      // getvalues();
        //getvalues1();

     //   setupSearchView();

       // mAdapter = new Vedio_list.MyRecyclerViewAdapter(getDataSet());
     //   mRecyclerView.setAdapter(mAdapter);
        //-- RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(this, LinearLayoutManager.VERTICAL);
        //---mRecyclerView.addItemDecoration(itemDecoration);


    }


    public static Vedio_list getInstance(){
        return   videolist;
    }


    @Override
    public void onBackPressed() {
      //  super.onBackPressed();


        String searchword= String.valueOf(mSearchView.getQuery());

        if(searchword.length()!=0){
            mSearchView.setQuery("",true);

        }else{
            finish();
        }



    }

    private void setupSearchView() {
        mSearchView.setIconifiedByDefault(false);
        mSearchView.setOnQueryTextListener(Vedio_list.this);
        mSearchView.setSubmitButtonEnabled(true);
        mSearchView.setQueryHint("Search Here");
        mSearchView.clearFocus();
    }








    private void getvalues() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    WebClient webClient = new WebClient();
                    String getRes = null;
                    try {
                        getRes = webClient.doGetReq("https://u2mrf96skg.execute-api.ap-southeast-1.amazonaws.com/mobiro/videolist?fname="+Constant.username);
                        Log.d("Home_page_Array :", getRes);

                        Log.e("post response", getRes);


                        try {
                            JSONArray respons = new JSONArray(getRes);


                            for (int i = 0; i < respons.length(); i++) {
                                HashMap<String, String> map = new HashMap<String, String>();
                                JSONObject jsonobject = respons.getJSONObject(i);
                                String name = jsonobject.getString("name");
                                String url = jsonobject.getString("stream_url");
                                String bitrate = jsonobject.getString("bit_rate");
                                String ratio = jsonobject.getString("aspect_ratio");
                                String thumb = jsonobject.getString("thumb_loc");
                                String videoid = jsonobject.getString("video_id");
                                String videotitle=null;
                                String videodescription=null;
                                String videotag=null;
                                if(jsonobject.has("Title"))
                                {
                                    videotitle=jsonobject.getString("Title");
                                }

                                if(jsonobject.has("Description"))
                                {
                                    videodescription = jsonobject.getString("Description");
                                }

                                if(jsonobject.has("VideoType"))
                                {
                                    videotag=jsonobject.getString("VideoType");
                                }




                                //  String videodescription=jsonobject.getString("Description");
                                // String videotag=jsonobject.getString("VideoType");
                                map.put("videoid",videoid);
                                map.put("thumb",thumb);
                                map.put("name",name);
                                map.put("vidurl",url);

                                map.put("vidtitle",videotitle);
                                map.put("viddesc",videodescription);
                                map.put("vidtag",videotag);

                                Constant.videolist.add(map);


                                //Dataobject obj = new Dataobject(usernme, usrphone, userlt, userid, userproimg, pincode, "");
                                // Constant.results.add(obj);

                                // Toast.makeText(getApplicationContext(), "Data Found", Toast.LENGTH_LONG).show();
                                // mAdapter.notifyDataSetChanged();
                            }




                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Log.d("namadhu top Size : ", "" + Constant.namadhu_top_news_Array.size());




                } catch (Exception ex) {
                    ex.printStackTrace();

                }
            }
        }).start();


    }
    private void getvalues1() {
        //Constant.videolist.clear();
        AsyncHttpClient clien = new AsyncHttpClient();
        clien.get("https://u2mrf96skg.execute-api.ap-southeast-1.amazonaws.com/mobiro/videolist?fname=feed16", new AsyncHttpResponseHandler()

        {


            @Override
            public void onSuccess(String arg0) {
                super.onSuccess(arg0);


                Log.e("post response", arg0);


                try {
                    JSONArray respons = new JSONArray(arg0);


                    for (int i = 0; i < respons.length(); i++) {
                        HashMap<String, String> map = new HashMap<String, String>();
                        JSONObject jsonobject = respons.getJSONObject(i);
                        String name = jsonobject.getString("name");
                        String url = jsonobject.getString("stream_url");
                        String bitrate = jsonobject.getString("bit_rate");
                        String ratio = jsonobject.getString("aspect_ratio");
                        String thumb = jsonobject.getString("thumb_loc");
                        String videoid = jsonobject.getString("video_id");
                        String videotitle=null;
                        String videodescription=null;
                        String videotag=null;
                        if(jsonobject.has("Title"))
                        {
                            videotitle=jsonobject.getString("Title");
                        }

                        if(jsonobject.has("Description"))
                        {
                           videodescription = jsonobject.getString("Description");
                        }

                        if(jsonobject.has("VideoType"))
                        {
                            videotag=jsonobject.getString("VideoType");
                        }




                      //  String videodescription=jsonobject.getString("Description");
                       // String videotag=jsonobject.getString("VideoType");
                        map.put("videoid",videoid);
                        map.put("thumb",thumb);
                        map.put("name",name);
                        map.put("vidurl",url);

                        map.put("vidtitle",videotitle);
                        map.put("viddesc",videodescription);
                        map.put("vidtag",videotag);

                        Constant.videolist.add(map);


                        //Dataobject obj = new Dataobject(usernme, usrphone, userlt, userid, userproimg, pincode, "");
                       // Constant.results.add(obj);

                        // Toast.makeText(getApplicationContext(), "Data Found", Toast.LENGTH_LONG).show();
                       // mAdapter.notifyDataSetChanged();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


            @Override
            public void onFinish() {
                super.onFinish();
                //-- Toast.makeText(getApplicationContext(), "finish", Toast.LENGTH_SHORT).show();
                Log.d("post response", "Finnish");

                setList();

                adapter = new MyRecyclerAdapter(Vedio_list.this, allList);
                mRecyclerView.setAdapter(adapter);

                setupSearchView();




               // mAdapter = new Insidenewsadapterr();
               // mRecyclerView.setAdapter(mAdapter);

            }

            @Override
            public void onFailure(Throwable arg0, String arg1) {
                super.onFailure(arg0, arg1);
                //Toast.makeText(getApplicationContext(), "failure", Toast.LENGTH_SHORT).show();
                Log.d("post response", "failure");
            }

        });

    }

    public void setList() {

        allList = new ArrayList<ListItem>();
        allList.clear();

        ListItem item = new ListItem();

        for (int i = 0; i < Constant.videolist.size(); i++) {
            item = new ListItem();
            item.setData(Constant.videolist.get(i).get("time"),Constant.videolist.get(i).get("vidtitle"), Constant.videolist.get(i).get("viddesc"), Constant.videolist.get(i).get("thumb"),Constant.videolist.get(i).get("vidtype"),Constant.videolist.get(i).get("vidtag"),Constant.videolist.get(i).get("vidurl"),Constant.videolist.get(i).get("videoid"),Constant.videolist.get(i).get("date"),Constant.videolist.get(i).get("name"),Constant.videolist.get(i).get("vidsize"),Constant.videolist.get(i).get("viddur"),Constant.videolist.get(i).get("vidlat"),Constant.videolist.get(i).get("vidlong"),Constant.videolist.get(i).get("vidplace"));
            allList.add(item);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    private ArrayList<Dataobject> getDataSet() {
        ArrayList results = new ArrayList<Dataobject>();
        String banktype;

        AsyncHttpClient clien = new AsyncHttpClient();
        clien.get("https://u2mrf96skg.execute-api.ap-southeast-1.amazonaws.com/mobiro/videolist?fname=feed16", new AsyncHttpResponseHandler()

        {


            @Override
            public void onSuccess(String arg0) {
                super.onSuccess(arg0);


                Log.e("post response", arg0);


                try {
                    JSONArray respons = new JSONArray(arg0);


                    for (int i = 0; i < respons.length(); i++) {

                        JSONObject jsonobject = respons.getJSONObject(i);
                        String userid = jsonobject.getString("name");
                        String usernme = jsonobject.getString("stream_url");
                        String usrphone = jsonobject.getString("bit_rate");
                        String userlt = jsonobject.getString("aspect_ratio");
                        String userproimg = jsonobject.getString("thumb_loc");
                        String pincode = jsonobject.getString("video_id");


                        Dataobject obj = new Dataobject(usernme, usrphone, userlt, userid, userproimg, pincode, "");
                        Constant.results.add(obj);

                        // Toast.makeText(getApplicationContext(), "Data Found", Toast.LENGTH_LONG).show();
                        mAdapter.notifyDataSetChanged();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


            @Override
            public void onFinish() {
                super.onFinish();
                //-- Toast.makeText(getApplicationContext(), "finish", Toast.LENGTH_SHORT).show();
                Log.d("post response", "Finnish");

            }

            @Override
            public void onFailure(Throwable arg0, String arg1) {
                super.onFailure(arg0, arg1);
                //Toast.makeText(getApplicationContext(), "failure", Toast.LENGTH_SHORT).show();
                Log.d("post response", "failure");
            }

        });

        return Constant.results;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        adapter.filter(newText);
     //adapter.filter(newText);
        gridadapter.filter(newText);

        return true;
    }





    public class ItemOffsetDecoration extends RecyclerView.ItemDecoration {

        private int mItemOffset;

        public ItemOffsetDecoration(int itemOffset) {
            mItemOffset = itemOffset;
        }

        public ItemOffsetDecoration(@NonNull Context context, @DimenRes int itemOffsetId) {
            this(context.getResources().getDimensionPixelSize(itemOffsetId));
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                                   RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);
            outRect.set(mItemOffset, mItemOffset, mItemOffset, mItemOffset);
        }
    }

}
