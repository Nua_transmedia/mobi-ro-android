package com.myappbuilder.wgstream;


/**
 * Created by nua-android on 30/5/16.
 * This code and all components (c) Copyright 2016-2017, NuaTransMedia,. All rights reserved.
 */
public class ListItem {
    String  desc,title,vidtype,vidtag,time,vidurl,vidid,viddate,vidname,vidsize,viddur,vidlat,vidlong,vidplace;
    String thumb;

    public void setData(String time, String title, String desc, String thumb, String vidtype, String vidtag, String vidurl, String videoid, String date, String s, String vidsize, String viddur, String vidlat, String vidlong, String vidplace) {
        this.title = title;
        this.desc = desc;
        this.thumb = thumb;
        this.time=time;
        this.vidtype=vidtype;
        this.vidtag=vidtag;
        this.vidurl=vidurl;
        this.vidid=videoid;
        this.viddate=date;
        this.vidname=s;
        this.viddur=viddur;
        this.vidsize=vidsize;
        this.vidlat=vidlat;
        this.vidlong=vidlong;
        this.vidplace=vidplace;
    }
}
