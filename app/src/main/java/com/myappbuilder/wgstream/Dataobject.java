package com.myappbuilder.wgstream;

/**
 * Created by nua-android on 30/5/16.
 * This code and all components (c) Copyright 2016-2017, NuaTransMedia,. All rights reserved.
 */
public class Dataobject {
    private String mText1;
    private String mText2;
    private String mText3;
    private String mText4;
    private String mText5;
    private String pincode;
    private String banknam;

    public Dataobject(String samebank_count, String otherbank_count, String banktype, String id, String proimg, String pincodee, String bankname){
        mText1 = samebank_count;
        mText2 = otherbank_count;
        mText3 = banktype;
        mText4=id;
        mText5=proimg;
        pincode=pincodee;
        banknam=bankname;

    }

    public String getPincode() {

        return pincode;
    }

    public void setPincode(String pincode) {

        this.pincode = pincode;
    }

    public String getBanknam() {

        return banknam;
    }

    public void setBanknam(String banknam) {

        this.banknam = banknam;
    }

    public String getmText1() {

        return mText1;
    }

    public void setmText1(String mText1) {

        this.mText1 = mText1;
    }

    public String getmText2() {

        return mText2;
    }

    public void setmText2(String mText2) {

        this.mText2 = mText2;
    }

    public String getmText3() {

        return mText3;
    }

    public void setmText3(String mText3) {

        this.mText3 = mText3;
    }
    public String getmText4() {

        return mText4;
    }

    public void setmText4(String mText4) {

        this.mText4 = mText4;
    }


    public String getProimg() {

        return mText5;
    }

    public void setProimg(String proimg) {

        this.mText5 = proimg;
    }

}