package com.myappbuilder.wgstream;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

import com.google.android.gcm.GCMBaseIntentService;

import java.util.List;


/**
 * Created by nua-android on 30/5/16.
 * This code and all components (c) Copyright 2016-2017, NuaTransMedia,. All rights reserved.
 */
public class GCMIntentService extends GCMBaseIntentService {

    private String TAG = "GCMServices";
    private Context context;
    private String[] share = new String[] { "1", "21", "3", "41", "5", "61",
            "7", "81", "9", "11" };

    public GCMIntentService() {

        super("378610203051");
    }

    @Override
    protected boolean onRecoverableError(Context con, String errorId) {
        context = con;
        if (errorId != null)
            Log.i(TAG, "onRecoverableError" + errorId);
        return super.onRecoverableError(context, errorId);
    }

    @Override
    protected void onError(Context arg0, String arg1) {

        if (arg1 != null)
            Log.i(TAG, "onError" + arg1);
    }



    @Override
    protected void onMessage(Context arg0, Intent intent) {
        context = arg0;
        generateNotification(arg0, intent);
    }

    @Override
    protected void onRegistered(Context arg0, String registrationId) {
        context = arg0;
        if (registrationId != null)
            Log.i(TAG, "Device registered: regId = " + registrationId);

        Log.i(TAG, "Device registered: regId = " + registrationId);
        Constant.notification_pref = getSharedPreferences("notification", Context.MODE_PRIVATE);
        Constant.set_notification_permission = Constant.notification_pref.edit();
        Log.d("registrationId", registrationId);
        Constant.set_notification_permission.putString("register_id", registrationId);
        Constant.set_notification_permission.commit();


        String device = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Constant.registerationid=registrationId;
        Constant.deviceid=device;

        Log.d("device_id", String.valueOf(device));
        Constant.notification_pref = getSharedPreferences("notification", Context.MODE_PRIVATE);
        Constant.set_notification_permission = Constant.notification_pref.edit();
        Constant.set_notification_permission.putString("tamil_new_device_id", device);
        Constant.set_notification_permission.putString("tamil_new_register_id", registrationId);
        Constant.set_notification_permission.commit();
        Log.d("tamil_new_register_id", registrationId);

    }

    @Override
    protected void onUnregistered(Context arg0, String arg1) {
        context = arg0;
        if (arg1 != null)
            Log.i(TAG, "onUnregistered" + arg1);
    }

    private void generateNotification(Context arg0, Intent intent) {
        Context context = arg0;
        Intent notificationIntent = null;
        String message = null;
        //String newsid=intent.getExtras().getString("newsid");
        //String sound=intent.getExtras().getString("sound");


            if(intent.getExtras()!=null){

             //   for(String key: intent.getExtras().keySet()){
                   // message+= key + "=" + intent.getExtras().toString() + "\n";

                    String fullmessage=intent.getExtras().toString();


                    String splitmsg[]=fullmessage.split("default=");

                    String remainstr=splitmsg[1];

                    String messageval[]=remainstr.split(",");

                    String messages=messageval[0];

                    message=messages;

                Constant.pushmessage=message;





                   // message+=intent.getStringExtra("message");



            	/*String[] msg=message.split("message");
            	String str1=msg[0];
            	String str2=msg[1];
            	String[] msgg=message.split("type");
            	String mstr1=msgg[0];
            	String mstr2=msgg[1];*/
            	/*String[] separated = CurrentString.split(":");
            	separated[0]; // this will contain "Fruit"
            	separated[1];*/
                  //  message=str2;
                   // msg=message;
                   // Toast.makeText(context, "msg2recieve" + message, Toast.LENGTH_SHORT).show();
               // }
            }

        int valuecount=Constant.notificationcount++;
        Log.d("countvalue",String.valueOf(valuecount));

        Constant.dayee = getSharedPreferences("dayfun", MODE_PRIVATE);

        Boolean open=Constant.dayee.getBoolean("open",false);

        if(open==true) {
            Livestream.updatenoticount(valuecount);
        }else{

        }

       /* if(isNamedProcessRunning("com.myappbuilder")){

            Livestream.updatenoticount(valuecount);
           // Toast.makeText(this.getBaseContext(),"mobiro running", Toast.LENGTH_SHORT).show();
        }
        else{
          //  Toast.makeText(this.getBaseContext(),"mobiro Not running", Toast.LENGTH_SHORT).show();

        }*/

     //   Livestream.updatenoticount(valuecount);



        /*ActivityManager activityManager = (ActivityManager) this.getSystemService( ACTIVITY_SERVICE );
        List<ActivityManager.RunningAppProcessInfo> procInfos = activityManager.getRunningAppProcesses();
        for(int i = 0; i < procInfos.size(); i++)
        {
            if(procInfos.get(i).processName.equals("com.myappbuilder.wgstream"))
            {

                Livestream.updatenoticount(valuecount);
             //   Toast.makeText(getApplicationContext(), "Browser is running", Toast.LENGTH_LONG).show();
            }else{
                Log.d("no active","noactive");
            }
        }*/





        Constant.vals = Constant.vals + 1;
        if (share.length == Constant.vals) {
            Constant.vals = 0;
        }
        PendingIntent intents = null;
        int icon;
        long when = System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);


        String title = context.getString(R.string.app_name_short);
       // String type = intent.getExtras().getString("type");
        Bundle bun = new Bundle();
       /* if (type != null) {} else {
            icon = R.mipmap.ic_launcher;
            notificationIntent = new Intent(context, Splash_video.class);
            notificationIntent.addCategory(Intent.CATEGORY_HOME);
            notificationIntent.putExtra("newsid", String.valueOf(newsid));
            Bundle b = new Bundle();
            b.putString("newid", Constant.noti_news_id);
            b.putString("messages", message);
            notificationIntent.putExtras(b);

            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        }*/


        /*icon=R.mipmap.ic_launcher;


        Notification notification = new Notification(icon, message, when);
        //notification.setLatestEventInfo(context, title, message, intents);
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notification.tickerText = message;

        RemoteViews contentView1 = new RemoteViews(context.getPackageName(),
                R.layout.noty_txt);
        contentView1.setTextViewText(R.id.notyt_subtitle, message);
        contentView1.setTextViewText(R.id.notyt_time, "" + when);
        contentView1.setImageViewResource(R.id.notyt_icon, icon);
        notification.contentView = contentView1;*/


        icon = R.mipmap.ic_launcher;
        notificationIntent = new Intent(context, Livestream.class);
        //notificationIntent.putExtra("pushcount", Constant.vals);
        notificationIntent.setFlags(PendingIntent.FLAG_ONE_SHOT
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        intents = PendingIntent.getActivity(context,
                Integer.valueOf(message.length()), notificationIntent,
                0);


        RemoteViews expandedView = new RemoteViews(
                context.getPackageName(), R.layout.noty_img);
        expandedView.setImageViewResource(R.id.notyt_icon, icon);
        expandedView.setTextViewText(R.id.notyt_title, title);
        expandedView.setTextViewText(R.id.notyt_time, ""+when);
        expandedView.setTextViewText(R.id.notyt_subtitle, message);
        //expandedView.setImageViewResource(R.id.audio,icon_audio);



        Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
                R.mipmap.ic_launcher);

        setNotification(intents, notificationManager, expandedView,
                bitmap, message, title);





        // notification.defaults |= Notification.DEFAULT_SOUND;
        // "/raw/kalimba");
       /* if(sound.length() >0)
        {
            notification.sound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                    + "://" + context.getPackageName() + "/raw/" + sound);
            notification.defaults |= Notification.DEFAULT_VIBRATE;
            notification.number = Constant.notificationcount++;

        }
        else
        {
            notification.sound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                    + "://" + context.getPackageName() + "/raw/namadhu_f");
            notification.defaults |= Notification.DEFAULT_VIBRATE;
            notification.number = Constant.notificationcount++;

        }*/


        /*notificationManager.notify(
                message.length() + Integer.valueOf(share[Constant.vals]),
                notification);*/

    }
    boolean isNamedProcessRunning(String processName){
        if (processName == null)
            return false;

        ActivityManager manager =
                (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> processes = manager.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo process : processes)
        {
            if (processName.equals(process.processName))
            {
                return true;
            }
        }
        return false;
    }


    @SuppressLint("NewApi")
    private void setNotification(PendingIntent intents,
                                 NotificationManager notificationManager, RemoteViews expandedView,
                                 Bitmap bitmap, String message, String title) {
        Notification notification;
        notification = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.mobiro_icon).setAutoCancel(true)
                .setLargeIcon(bitmap).setContentIntent(intents)
                .setContentTitle(title).setContentText(message).build();



        notification.bigContentView = expandedView;
        SharedPreferences pt = context.getSharedPreferences("register",
                context.MODE_PRIVATE);
        if (pt.contains("psound")) {
            if (pt.getString("psound", null) != null) {
                String boo_sound = pt.getString("psound", null);
                if (boo_sound.contentEquals("true")) {
                    notification.sound = Uri
                            .parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                                    + "://" + context.getPackageName()
                                    + "/raw/noti");
                }
            }
        }

        if (pt.contains("pvibrate")) {
            if (pt.getString("pvibrate", null) != null) {
                String boo = pt.getString("pvibrate", null);
                if (boo.contentEquals("true")) {
                    notification.defaults = Notification.DEFAULT_VIBRATE;
                }
            }
        }

        notification.flags = Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(
                message.length() + Integer.valueOf(share[Constant.vals]),
                notification);

    }

}