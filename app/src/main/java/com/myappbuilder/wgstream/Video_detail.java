package com.myappbuilder.wgstream;

import android.*;
import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.VideoView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by nua-android on 30/5/16.
 * This code and all components (c) Copyright 2016-2017, NuaTransMedia,. All rights reserved.
 */
public class Video_detail extends FragmentActivity implements View.OnClickListener, MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnInfoListener {

    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS =0;
    private String vidid,vidtitle,viddesc,vidtag,viddate,vidtime,vidthumb,vidurl,vidname,viddur,vidsize,vidlat,vidlong;
    private TextView date,time,title,description,tags,addresstxt,timedur,videosize;
    private ImageView vid_img,vid_play;
    private Button back;
    private VideoView detail_video;
    private ProgressDialog prg_bar;
    private ViewGroup.LayoutParams originalContainerLayoutParams,videoparams;
    private RelativeLayout img_contain,navi_bar,desc_cntin;
    private LinearLayout date_cntin;

    private ImageView imageview;

    protected String[] mRequiredPermissions = {};

    private ScrollView scrolls;


    private boolean mapnull=false;
    //mapview
   private LocationManager locManager;
    private Drawable drawable;
    private Document document;
    private LatLng fromPosition;
    private LatLng toPosition;
    private GoogleMap mGoogleMap;
    private MarkerOptions markerOptions;
    private Location location ;

    Marker mPositionMarker;

    private static Geocoder geocoder;
    //mapview

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.videodetail);
        findViewbyid();


      geocoder = new Geocoder(Video_detail.this, Locale.getDefault());

        originalContainerLayoutParams = img_contain.getLayoutParams();
        videoparams=detail_video.getLayoutParams();
        prg_bar = new ProgressDialog(Video_detail.this);
        prg_bar.setMessage("Please Wait");
        prg_bar.setIndeterminate(true);
        prg_bar.setCancelable(false);
        prg_bar.setMax(100);
        prg_bar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        Intent intent = getIntent();
        vidid = intent.getExtras().getString("videoid");
        vidtitle=intent.getExtras().getString("vidtitle");
        viddesc=intent.getExtras().getString("viddesc");
        vidtag=intent.getExtras().getString("vidtag");
        vidthumb=intent.getExtras().getString("vidthumb");
        vidurl=intent.getExtras().getString("vidurl");

        vidname=intent.getExtras().getString("vidname");

        viddur=intent.getExtras().getString("viddur");
        vidsize= intent.getExtras().getString("vidsize");

        vidlat=intent.getExtras().getString("vidlat");
        vidlong=intent.getExtras().getString("vidlong");

       String timedate=intent.getExtras().getString("viddate");

        Log.d("vdvidlat",vidlat);
        Log.d("vdvidlong",vidlong);




        if(timedate.contains(" ")){
            String[] separate = timedate.split(" ");
            viddate =separate[0];
            vidtime=separate[1];
        }else{
            ;
        }

        mRequiredPermissions = new String[] {
                android.Manifest.permission.CAMERA,
                android.Manifest.permission.RECORD_AUDIO,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION

        };
        //mapview

        int currentVersion = android.os.Build.VERSION.SDK_INT;
        if (currentVersion >= Build.VERSION_CODES.M) {

            getcity(vidlat,vidlong,1);

            float curlant = 0;
            float curlongi = 0;

            if (!vidlat.contains(" null")) {
                //
                if (vidlat.contains("null")) {
                    mapnull = true;
                    vidlat = "-3.962901";
                    curlant = Float.parseFloat(vidlat);
                } else {
                    mapnull = false;
                    curlant = Float.parseFloat(vidlat);
                }

                    } else {
                mapnull = true;
                vidlat = "-3.962901";
                curlant = Float.parseFloat(vidlat);
            }

            if (!vidlong.contains(" null")) {
                if (vidlong.contains("null")) {
                    mapnull = true;
                    vidlong = "81.562500";
                    curlongi = Float.parseFloat(vidlong);
                } else {
                    mapnull = false;
                    curlongi = Float.parseFloat(vidlong);
                }



            } else {
                mapnull = true;
                vidlong = "81.562500";
                curlongi = Float.parseFloat(vidlong);
            }


            SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mGoogleMap = supportMapFragment.getMap();




            // Enabling MyLocation in Google Map
            mGoogleMap.setMyLocationEnabled(true);
            mGoogleMap.getUiSettings().setZoomControlsEnabled(true);
            mGoogleMap.getUiSettings().setCompassEnabled(true);
            mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
            mGoogleMap.getUiSettings().setAllGesturesEnabled(true);
            mGoogleMap.setTrafficEnabled(true);


            LatLng coordinate = new LatLng(curlant, curlongi);
            // CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(coordinate, 5);
            //  mGoogleMap.animateCamera(yourLocation);

            if (mPositionMarker == null) {

                mPositionMarker = mGoogleMap.addMarker(new MarkerOptions()
                        .flat(true)
                        .icon(BitmapDescriptorFactory
                                .fromResource(R.drawable.mapicon))
                        .anchor(0.5f, 0.5f)
                        .position(
                                new LatLng(curlant, curlongi)));
            }


            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(coordinate, 12));
            // mGoogleMap.addMarker(marker);
            markerOptions = new MarkerOptions();

            if (ContextCompat.checkSelfPermission(Video_detail.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(Video_detail.this,
                        Manifest.permission.ACCESS_FINE_LOCATION)) {

                } else {


                    ActivityCompat.requestPermissions(Video_detail.this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            MY_PERMISSIONS_REQUEST_READ_CONTACTS);


                }
            }

        }else{

            float curlant = 0;
            float curlongi = 0;

            if (!vidlat.contains(" null")) {
                //
                if (vidlat.contains("null")) {
                    mapnull = true;
                    vidlat = "-3.962901";
                    curlant = Float.parseFloat(vidlat);
                } else {
                    mapnull = false;
                    curlant = Float.parseFloat(vidlat);
                }

            } else {
                mapnull = true;
                vidlat = "-3.962901";
                curlant = Float.parseFloat(vidlat);
            }

            if (!vidlong.contains(" null")) {
                if (vidlong.contains("null")) {
                    mapnull = true;
                    vidlong = "81.562500";
                    curlongi = Float.parseFloat(vidlong);
                } else {
                    mapnull = false;
                    curlongi = Float.parseFloat(vidlong);
                }



            } else {
                mapnull = true;
                vidlong = "81.562500";
                curlongi = Float.parseFloat(vidlong);
            }


            SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mGoogleMap = supportMapFragment.getMap();


            // Geocoder geocoder;
            List<Address> addresses = null;
            geocoder = new Geocoder(this, Locale.getDefault());

            try {

                Log.d("curlat", String.valueOf(curlant));
                Log.d("curlng", String.valueOf(curlongi));
                addresses = geocoder.getFromLocation(curlant, curlongi, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            } catch (IOException e) {
                e.printStackTrace();
            }

            Log.d("addresseson", String.valueOf(addresses));
            String address = null, place = null, where, city, state = null, country = null, postalCode = null, knownName;

            if (addresses.size() != 0) {

                address = addresses.get(0).getAddressLine(1);
                place = addresses.get(0).getSubAdminArea();
                where = addresses.get(0).getSubLocality();// If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                city = addresses.get(0).getLocality();
                state = addresses.get(0).getAdminArea();
                country = addresses.get(0).getCountryName();
                postalCode = addresses.get(0).getPostalCode();
                knownName = addresses.get(0).getFeatureName();
            }



            if (mapnull == true) {
                addresstxt.setText("Not Available");

            } else {
                addresstxt.setText(address + " , " + place + " , " + state + " , " + country + " , " + postalCode);
            }



            // Enabling MyLocation in Google Map
            mGoogleMap.setMyLocationEnabled(true);
            mGoogleMap.getUiSettings().setZoomControlsEnabled(true);
            mGoogleMap.getUiSettings().setCompassEnabled(true);
            mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
            mGoogleMap.getUiSettings().setAllGesturesEnabled(true);
            mGoogleMap.setTrafficEnabled(true);


            LatLng coordinate = new LatLng(curlant, curlongi);
            // CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(coordinate, 5);
            //  mGoogleMap.animateCamera(yourLocation);

            if (mPositionMarker == null) {

                mPositionMarker = mGoogleMap.addMarker(new MarkerOptions()
                        .flat(true)
                        .icon(BitmapDescriptorFactory
                                .fromResource(R.drawable.mapicon))
                        .anchor(0.5f, 0.5f)
                        .position(
                                new LatLng(curlant, curlongi)));
            }


            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(coordinate, 12));
            // mGoogleMap.addMarker(marker);
            markerOptions = new MarkerOptions();

       }

        imageview.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        scrolls.requestDisallowInterceptTouchEvent(true);
                        // Disable touch on transparent view
                        return false;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        scrolls.requestDisallowInterceptTouchEvent(false);
                        return true;

                    case MotionEvent.ACTION_MOVE:
                        scrolls.requestDisallowInterceptTouchEvent(true);
                        return false;

                    default:
                        return true;
                }
            }
        });






        //mapview




        declaration();
    }



    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.



                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    private void declaration() {


        SimpleDateFormat fromUser = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat myFormat = new SimpleDateFormat("dd-MM-yyyy");

        String reformattedStr=null;
        //  Toast.makeText(getActivity().getApplicationContext(),"date"+String.valueOf(reformattedStr),Toast.LENGTH_SHORT).show();

        try {

            reformattedStr = myFormat.format(fromUser.parse(viddate));
        } catch (ParseException e) {
            e.printStackTrace();
        }

       Typeface type = Typeface.createFromAsset(getAssets(),"fonts/Trebuchet MS.ttf");
        date.setTypeface(type);
        time.setTypeface(type);
        title.setTypeface(type);
        description.setTypeface(type);
        tags.setTypeface(type);
        timedur.setTypeface(type);


        if(reformattedStr!=null) {
            date.setText(reformattedStr);
        }else{
            date.setText(viddate);
        }


        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
        SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a");
        Date dt;
        try {
            dt = sdfs.parse(vidtime);
            time.setText(String.valueOf(dt));
            //System.out.println("Time Display: " + sdfs.format(dt)); // <-- I got result here
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        //date.setText(viddate);
       // time.setText(vidtime);
        if(vidtitle.length()!=0) {
            title.setText("Title: "+vidtitle);
        }else{
            title.setText("Name: "+vidname);
        }
        description.setText("Description: "+viddesc+"                                                                         ");

        if(vidtag.contentEquals("(null)")){
            vidtag="";
        }
        tags.setText("Tags: "+vidtag+"                                                                                     ");

        timedur.setText(viddur);
        videosize.setText(vidsize);

        Picasso.with(getApplicationContext()).load(vidthumb).into(vid_img);


        vid_img.setOnClickListener(this);
        back.setOnClickListener(this);



    }

    private void findViewbyid() {
        date=(TextView) findViewById(R.id.date);
        time=(TextView) findViewById(R.id.time);
        title=(TextView) findViewById(R.id.vid_title);
        description=(TextView) findViewById(R.id.vid_desc);
        tags=(TextView) findViewById(R.id.vid_tag_desc);
        vid_img=(ImageView) findViewById(R.id.vid_image);
        back=(Button) findViewById(R.id.back_edit);
        detail_video=(VideoView) findViewById(R.id.detail_vidview);
        vid_play=(ImageView) findViewById(R.id.video_play);
        img_contain=(RelativeLayout) findViewById(R.id.img_cntin);
        navi_bar=(RelativeLayout) findViewById(R.id.detail_navi);
        date_cntin=(LinearLayout) findViewById(R.id.time_cal_cntin);
        desc_cntin=(RelativeLayout) findViewById(R.id.desc_cntin);

        timedur=(TextView) findViewById(R.id.vid_time);
        videosize=(TextView) findViewById(R.id.vid_size);

        addresstxt=(TextView) findViewById(R.id.addresstxt);

        imageview=(ImageView) findViewById(R.id.transparent_image);

        scrolls=(ScrollView) findViewById(R.id.scrolls);
         
    }



    @Override
    public void onClick(View v) {
        if(v==vid_img){

            Intent inten = new Intent(getApplicationContext(), Videoview.class);
            inten.putExtra("videourl", vidurl);
            startActivity(inten);


        }else if(v==back){
            finish();
        }
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {

    }

    @Override
    public boolean onInfo(MediaPlayer mp, int what, int extra) {
        return false;
    }







    private void getcity(final String latitudee, final String longitudee, final int i) {


        final String[] value = new String[1];
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    WebClient webClient = new WebClient();
                    String getRes = null;
                    try {
                        //https://u2mrf96skg.execute-api.ap-southeast-1.amazonaws.com/mobiro/notify/list?deviceid=dc5afb4269e79563d692ea386c28dfd9a6aa2190c0a8cf4164ec26406c8e6211
                        getRes = webClient.doGetReq("http://maps.googleapis.com/maps/api/geocode/json?latlng="+latitudee+","+longitudee+"&sensor=true");
                        Log.d("Home_page_Array :", getRes);

                        Log.e("post response", getRes);


                        try {
                            JSONObject locobj=new JSONObject(getRes);

                            JSONArray respons = locobj.getJSONArray("results");

                            if(respons.length()!=0) {

                                JSONObject add_obj = respons.getJSONObject(0);
                             String frmtaddr=  add_obj.getString("formatted_address");

                               /* JSONArray address_arr = add_obj.getJSONArray("address_components");

                                JSONObject area_obj = address_arr.getJSONObject(4);

                               // place = area_obj.getString("long_name");
                                String loc = area_obj.getString("long_name");*/

                                addresstxt.setText(frmtaddr);
                               // value[0] =area_obj.getString("long_name");

                              //  Constant.locvalue=loc;

                                /*HashMap<String, String> map = new HashMap<String, String>();
                                map.put("vidlocation",loc);



                                Constant.locationlist.add(map);*/


                            }else{
                                //  Constant.location.add(i, "Not Available");
                                addresstxt.setText("Not Available");
                            }






                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d("post response", "Finnish");



                    } catch (IOException e) {
                        e.printStackTrace();

                    }


                } catch (Exception ex) {
                    ex.printStackTrace();


                }
            }
        }).start();

    }

}
