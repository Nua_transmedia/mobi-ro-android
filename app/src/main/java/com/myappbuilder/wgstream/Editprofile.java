/*
package com.myappbuilder.wgstream;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

*/
/**
 * Created by nua-android on 11/7/16.
 *//*

public class Editprofile extends Activity{

private EditText profile_name,custom_width,custom_height,video_bit,frame_rate,keyframe,sample_rate,audio_bit;
    private Spinner audiotype;
    private Button add,cancel,back_btn;

    private ProgressDialog prg_bar;

    public static Typeface type;

    private TextView titletxt,resolutiontxt,videobittxt,frameratetxt,keyframetxt;

    private String profilename,width,height,videobitrate,framerate,keyframeinterval,samplerate,audiovalue,audiobitrate;

    private LoginDataBaseAdapter loginDataBaseAdapter;

    String[] audio = {
            "Mono",
            "Stereo"
  };
    String[] widthvalues = {
            "Mono",
            "Stereo"

    };

    String[] heightvalues = {
            "Mono",
            "Stereo"

    };

    String[] bitratevalues = {
            "Mono",
            "Stereo"

    };

    String[] frameratevalues = {
            "Mono",
            "Stereo"

    };

    String[] keyintervals = {
            "Mono",
            "Stereo"

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.editprofile);
        findViewbyid();

        loginDataBaseAdapter = new LoginDataBaseAdapter(this);
        loginDataBaseAdapter = loginDataBaseAdapter.open();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, R.layout.spinnertext, audio);

        audiotype.setBackgroundResource(R.drawable.btn_dropdown);

        audiotype.setAdapter(adapter);
        prg_bar = new ProgressDialog(Editprofile.this);
        prg_bar.setMessage("Please Wait");
        prg_bar.setIndeterminate(true);
        prg_bar.setCancelable(false);
        prg_bar.setMax(100);
        prg_bar.setProgressStyle(ProgressDialog.STYLE_SPINNER);

       String values= String.valueOf(loginDataBaseAdapter.getprofilename("5"));

        if(loginDataBaseAdapter.getcountofval(6)>0) {
         //   Toast.makeText(getApplicationContext(), String.valueOf(loginDataBaseAdapter.getcountofval(6))+values, Toast.LENGTH_SHORT).show();
        }else{

        }


        type = Typeface.createFromAsset(getAssets(),"fonts/Trebuchet MS.ttf");
        titletxt.setTypeface(type);
        resolutiontxt.setTypeface(type);
        videobittxt.setTypeface(type);
                frameratetxt.setTypeface(type);
                keyframetxt.setTypeface(type);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Editprofile.this.finish();
                Intent inten=new Intent(getApplicationContext(),Livestream.class);
                startActivity(inten);
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                profilename=profile_name.getText().toString();
                width=custom_width.getText().toString();
                height=custom_height.getText().toString();
                videobitrate=video_bit.getText().toString();
                framerate=frame_rate.getText().toString();
                keyframeinterval=keyframe.getText().toString();
                */
/*samplerate=sample_rate.getText().toString();
                audiobitrate= audio_bit.getText().toString();
                audiovalue="sterio";*//*


                samplerate="41000";
                audiobitrate= "64000";
                audiovalue="sterio";


                if(profilename.length()!=0){
                    if(width.length()!=0){

                        if(height.length()!=0){

                            if(videobitrate.length()!=0){

                                if(framerate.length()!=0){

                                    if(keyframeinterval.length()!=0){


                                        loginDataBaseAdapter = new LoginDataBaseAdapter(Editprofile.this);
                                        loginDataBaseAdapter = loginDataBaseAdapter.open();


                                        loginDataBaseAdapter.insertCustomprof(1,profilename,width,height,videobitrate,framerate,keyframeinterval,samplerate,audiovalue,audiobitrate);

                                        toastsettext("Profile Added");
                                        Editprofile.this.finish();
                                        */
/*Intent inten=new Intent(getApplicationContext(),Livestream.class);
                                        startActivity(inten);*//*



                                    }else{
                                        toastsettext("Please enter Key Frame Interval");
                                    }

                                }else{
                                    toastsettext("Please enter Framerate");
                                }

                            }else{
                                toastsettext("Please enter Video bitrate");
                            }

                        }else{
                            toastsettext("Please enter Height");
                        }

                    }else{
                        toastsettext("Please enter Width");
                    }

                }else {
toastsettext("Please enter Profile name");
                }


            }
        });

      */
/*  cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });*//*







    }


    @Override
    public void onBackPressed() {
        Editprofile.this.finish();
        Intent inten=new Intent(getApplicationContext(),Livestream.class);
        startActivity(inten);
    }

    public void toastsettext(String string1) {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.altdialog_toast,
                (ViewGroup) findViewById(R.id.toast_rl));
        TextView txt = (TextView) layout.findViewById(R.id.toast_txt);
        txt.setText(string1);
        Toast tst = new Toast(getApplicationContext());
        tst.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        tst.setDuration(Toast.LENGTH_SHORT);
        tst.setView(layout);
        tst.show();
    }


    private void findViewbyid() {
        profile_name=(EditText) findViewById(R.id.profilename_edit);
        custom_width=(EditText) findViewById(R.id.framewidth_edit);
        custom_height=(EditText) findViewById(R.id.frame_height);
        video_bit=(EditText) findViewById(R.id.bitrate_edit);
        frame_rate=(EditText) findViewById(R.id.framerate_edit);
        keyframe=(EditText) findViewById(R.id.frameinterval_edit);
        sample_rate=(EditText) findViewById(R.id.samplerate_edit);
        audio_bit=(EditText) findViewById(R.id.audiobitrate_edit);
        audiotype=(Spinner) findViewById(R.id.spinner_sterip);
        back_btn=(Button) findViewById(R.id.back_edit);

        add=(Button) findViewById(R.id.save_btn);
        titletxt=(TextView) findViewById(R.id.prof_title);
        resolutiontxt=(TextView) findViewById(R.id.prof_wh);
        videobittxt=(TextView) findViewById(R.id.prof_vbr);
                frameratetxt=(TextView) findViewById(R.id.prof_fr);
        keyframetxt=(TextView) findViewById(R.id.prof_kfi);




      //  cancel=(Button) findViewById(R.id.btn_cancel);
    }
}
*/
