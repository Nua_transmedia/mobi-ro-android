package com.myappbuilder.wgstream;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;


/*
 * This service is designed to run in the background and receive messages from gcm. If the app is in the foreground
 * when a message is received, it will immediately be posted. If the app is not in the foreground, the message will be saved
 * and a notification is posted to the NotificationManager.
 */

/**
 * Created by nua-android on 30/5/16.
 * This code and all components (c) Copyright 2016-2017, NuaTransMedia,. All rights reserved.
 */
public class MessageReceivingService extends Service{
    private GoogleCloudMessaging gcm;
    public static SharedPreferences savedValues;
    public static String msg=null;

    public static void sendToApp(Bundle extras, Context context){
        Intent newIntent = new Intent();
       //--- newIntent.setClass(context, AndroidMobilePushApp.class);
        newIntent.setClass(context, Livestream.class);
        newIntent.putExtras(extras);
        newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(newIntent);
        //---postNotification(new Intent(context, AndroidMobilePushApp.class), context);
        postNotification(new Intent(context, Livestream.class), context);


        String message=null;
        if(extras!=null){

            for(String key: extras.keySet()){
                //message+= key + "=" + extras.getString(key) + "\n";

                String fullmessage=extras.toString();


                String splitmsg[]=fullmessage.split("default=");

                String remainstr=splitmsg[1];

                String messageval[]=remainstr.split(",");

                String messages=messageval[0];

                message=messages;


                //message+=extras.getString(key);
            	/*String[] msg=message.split("message");
            	String str1=msg[0];
            	String str2=msg[1];
            	String[] msgg=message.split("type");
            	String mstr1=msgg[0];
            	String mstr2=msgg[1];*/
            	/*String[] separated = CurrentString.split(":");
            	separated[0]; // this will contain "Fruit"
            	separated[1];*/

                msg=message;
                Toast.makeText(context, "msg2recieve" + message, Toast.LENGTH_SHORT).show();
            }
        }

    }

    public void onCreate(){
        super.onCreate();
        final String preferences = getString(R.string.preferences);
        savedValues = getSharedPreferences(preferences, Context.MODE_PRIVATE);
        // In later versions multi_process is no longer the default
        if(VERSION.SDK_INT >  9){
            savedValues = getSharedPreferences(preferences, Context.MODE_MULTI_PROCESS);
        }
        gcm = GoogleCloudMessaging.getInstance(getBaseContext());
        SharedPreferences savedValues = PreferenceManager.getDefaultSharedPreferences(this);
        if(savedValues.getBoolean(getString(R.string.first_launch), true)){
            register();
            SharedPreferences.Editor editor = savedValues.edit();
            editor.putBoolean(getString(R.string.first_launch), false);
            editor.commit();
            
        }
        
      // Toast.makeText(getApplicationContext(), str, Toast.LENGTH_SHORT).show();
        // Let AndroidMobilePushApp know we have just initialized and there may be stored messages
        sendToApp(new Bundle(), this);
        saveToLog(new Bundle(), this);
        
    }

    protected static void saveToLog(Bundle extras, Context context){
        SharedPreferences.Editor editor=savedValues.edit();
        String numOfMissedMessages = context.getString(R.string.num_of_missed_messages);
        int linesOfMessageCount = 0;
        for(String key : extras.keySet()){
            String line = String.format("%s=%s", key, extras.getString(key));
            editor.putString("MessageLine" + linesOfMessageCount, line);
            linesOfMessageCount++;
        }
        editor.putInt(context.getString(R.string.lines_of_message_count), linesOfMessageCount);
        editor.putInt(context.getString(R.string.lines_of_message_count), linesOfMessageCount);
        editor.putInt(numOfMissedMessages, savedValues.getInt(numOfMissedMessages, 0) + 1);
        editor.commit();
        //--postNotification(new Intent(context, AndroidMobilePushApp.class), context);
        postNotification(new Intent(context, Livestream.class), context);
    }

    protected static void postNotification(Intent intentAction, Context context){
        final NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        //---final PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intentAction, Notification.DEFAULT_LIGHTS | Notification.FLAG_AUTO_CANCEL);
        final PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intentAction, Intent.FILL_IN_ACTION);
        final Notification notification = new NotificationCompat.Builder(context).setSmallIcon(R.drawable.ic_cast_dark)
                .setContentTitle(msg)
                .setContentText(msg)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .getNotification();

        mNotificationManager.notify(R.string.notification_number, notification);
        

    }

    @SuppressWarnings("rawtypes")
	private void register() {
        new AsyncTask(){
            protected Object doInBackground(final Object... params) {
                String token;
                try {
                     token = gcm.register(getString(R.string.project_number));
                   //-- token = gcm.register("961701430859");
                    Log.i("registrationId", token);
                    Log.d("token ID from MRS", token);
                    Log.e("token ID from MRS", token);
                } 
                catch (IOException e) {
                    Log.i("Registration Error", e.getMessage());
                }
                return true;
            }
        }.execute(null, null, null);
    }

    public IBinder onBind(Intent arg0) {
        return null;
    }
    
    
}