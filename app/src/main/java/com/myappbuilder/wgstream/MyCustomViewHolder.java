package com.myappbuilder.wgstream;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by nua-android on 30/5/16.
 * This code and all components (c) Copyright 2016-2017, NuaTransMedia,. All rights reserved.
 */

public class MyCustomViewHolder extends RecyclerView.ViewHolder {

    protected ImageView imageView;
    protected TextView tvName, tvPlace,edit,time,moreless,less,date,size,duration,city;
    protected ImageView imgThumb;
    protected RelativeLayout video_click,editclick,moreclick,detail_click;

    public MyCustomViewHolder(View view) {
        super(view);
        this.tvName = (TextView) view.findViewById(R.id.tvName);
        this.tvPlace = (TextView) view.findViewById(R.id.tvPlace);
        this.imgThumb = (ImageView) view.findViewById(R.id.imgThumb);
        this.video_click=(RelativeLayout) view.findViewById(R.id.vid_rel);
        this.edit=(TextView) view.findViewById(R.id.edit_video);
        this.editclick=(RelativeLayout) view.findViewById(R.id.editclick);
        this.time=(TextView) view.findViewById(R.id.time);
        this.moreclick=(RelativeLayout) view.findViewById(R.id.more_rel);
        this.moreless=(TextView) view.findViewById(R.id.more);
        this.less=(TextView) view.findViewById(R.id.less);
        this.detail_click=(RelativeLayout) view.findViewById(R.id.video_click);
        this.date=(TextView) view.findViewById(R.id.date);
        this.size=(TextView) view.findViewById(R.id.aspect);
        this.city=(TextView) view.findViewById(R.id.addresstxt);



    }

}