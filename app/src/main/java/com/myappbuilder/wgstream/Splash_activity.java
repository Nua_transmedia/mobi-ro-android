package com.myappbuilder.wgstream;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

/**
 * Created by nua-android on 30/5/16.
 * This code and all components (c) Copyright 2016-2017, NuaTransMedia,. All rights reserved.
 */
public class Splash_activity extends Activity {

	private int duration = 800;
	private Boolean splash_boo = false;
	private boolean login;

	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash_activity);

		startService(new Intent(this, MessageReceivingService.class));


		Constant.dayee = getSharedPreferences("dayfun",
				MODE_WORLD_WRITEABLE);

		login=Constant.dayee.getBoolean("login",false);
		
		Handler hand = new Handler();
		hand.postDelayed(new Runnable() {
			
			public void run() {
				
				finish();
				if(!splash_boo){
					if(login==true){
						Intent inten=new Intent(Splash_activity.this,Livestream.class);
						startActivity(inten);
					}else {
						Intent intent = new Intent(Splash_activity.this, Login.class);
						startActivity(intent);
					}
				}
			}
		}, duration);
	}
	
	@Override
	public void onBackPressed() {
		splash_boo = true;
		super.onBackPressed();
	}
	
}
