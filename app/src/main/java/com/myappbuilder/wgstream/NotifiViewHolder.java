package com.myappbuilder.wgstream;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
/**
 * Created by nua-android on 30/5/16.
 * This code and all components (c) Copyright 2016-2017, NuaTransMedia,. All rights reserved.
 */
public class NotifiViewHolder extends RecyclerView.ViewHolder {

    protected ImageView imageView;
    protected TextView tvName,date,time;

    public NotifiViewHolder(View view) {
        super(view);
        this.tvName = (TextView) view.findViewById(R.id.notifi_title);
        this.date=(TextView) view.findViewById(R.id.date);
        this.time=(TextView) view.findViewById(R.id.time);

    }

}