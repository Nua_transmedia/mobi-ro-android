

package com.myappbuilder.wgstream;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.v4.view.GestureDetectorCompat;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.amazonmobileanalytics.AnalyticsEvent;
import com.amazonaws.mobileconnectors.amazonmobileanalytics.InitializationException;
import com.amazonaws.mobileconnectors.amazonmobileanalytics.MobileAnalyticsManager;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.CreatePlatformEndpointRequest;
import com.amazonaws.services.sns.model.CreatePlatformEndpointResult;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.myappbuilder.wgstream.config.ConfigPrefs;
import com.myappbuilder.wgstream.ui.AutoFocusListener;
import com.myappbuilder.wgstream.ui.MultiStateButton;
import com.myappbuilder.wgstream.ui.StatusView;
import com.myappbuilder.wgstream.ui.TimerView;
import com.wowza.gocoder.sdk.api.WowzaGoCoder;
import com.wowza.gocoder.sdk.api.configuration.WZMediaConfig;
import com.wowza.gocoder.sdk.api.devices.WZCamera;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by nua-android on 30/5/16.
 * This code and all components (c) Copyright 2016-2017, NuaTransMedia,. All rights reserved.
 */

public class Livestream extends CameraActivityBase implements AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener {
    private final static String TAG = Livestream.class.getSimpleName();

    // UI controls
    protected MultiStateButton mBtnSwitchCamera  = null;
    protected MultiStateButton      mBtnTorch         = null;
    protected MultiStateButton mBtnMic = null;
    protected TimerView mTimerView        = null;

    protected StatusView statusview;

    String place="Not Available";

    // Gestures are used to toggle the focus modes
    protected GestureDetectorCompat mAutoFocusDetector = null;

    private Geocoder geocoder;


    private ImageView vediolist, notifilist,profilelist;
    private static TextView noti_count;
    private ProgressDialog prg_bar;
    private String gcmregisterid;
    private String deviceidval, osversion, devicename, imeinumber,osname;
    private String latitude,longtidude;
    private Spinner spnr;
    private LoginDataBaseAdapter loginDataBaseAdapter;
    private Context context;
    private TextView mTxtFrameSize,feedname;

    private Button popup;

    private PopupWindow popupWindow;

    private RelativeLayout rel_cntin,rel_topmenu;

    private static AlertDialog alertDialog;

    private ViewGroup.LayoutParams originalContainerLayoutParams;
    private static MobileAnalyticsManager analytics;

    private boolean mapnull=false;

   //private GoogleApiClient mGoogleApiClient;
 //  private Location mLastLocation;

   // final Dialog dialog = new Dialog(Livestream.this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);


        //initilaization
        findViewbyid();



        //get login info and login boolean changing
        Constant.dayee = getSharedPreferences("dayfun", MODE_PRIVATE);
        Constant.daycom = Constant.dayee.edit();

        Constant.daycom.putBoolean("open",true);

        Constant.daycom.commit();


        Constant.dayee = getSharedPreferences("dayfun", MODE_PRIVATE);

        String username=Constant.dayee.getString("username","");
        String password=Constant.dayee.getString("password", "");
        Constant.username=username;
        Constant.password=password;
     //get login info and login boolean changing


        alertDialog = new AlertDialog.Builder(Livestream.this
        ).create();

        prg_bar = new ProgressDialog(Livestream.this);
        prg_bar.setMessage("Please Wait");
        prg_bar.setIndeterminate(true);
        prg_bar.setCancelable(false);
        prg_bar.setMax(100);
        prg_bar.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        mRequiredPermissions = new String[] {
                Manifest.permission.CAMERA,
                Manifest.permission.RECORD_AUDIO
        };

        // Initialize the UI controls
        mBtnTorch           = (MultiStateButton) findViewById(R.id.ic_torch);
        mBtnSwitchCamera    = (MultiStateButton) findViewById(R.id.ic_switch_camera);
        mBtnMic=(MultiStateButton) findViewById(R.id.ic_mic);
        mTimerView          = (TimerView) findViewById(R.id.txtTimer);


        //audiorec
        mRequiredPermissions = new String[] {
                Manifest.permission.CAMERA,
                Manifest.permission.RECORD_AUDIO
        };

        mBtnMic             = (MultiStateButton) findViewById(R.id.ic_mic);
        //audiorec


        context=Livestream.this;

        mTxtFrameSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // spnr.performClick();
                profilelist.performClick();
            }
        });




        notificationmain();//show notification count
        if(Constant.notificationcount!=0){
            noti_count.setVisibility(View.VISIBLE);
            noti_count.setText(String.valueOf(Constant.notificationcount));
           // notifilist.setBackgroundResource(R.drawable.noticount);

        }else{
            noti_count.setVisibility(View.GONE);
          //  notifilist.setBackgroundResource(R.drawable.nitifi_icon);
        }

        Constant.aList.clear();

        //Local Database for maintaining customly created Profile list

        loginDataBaseAdapter = new LoginDataBaseAdapter(this);
        loginDataBaseAdapter = loginDataBaseAdapter.open();









        int dbcount=loginDataBaseAdapter.getcountofval(6);

        if(dbcount==0) {
            loginDataBaseAdapter.insertCustomprof(1, "CK Default (480x640)", "480", "640", "400", "15", "2", "30", "sterio", "64000");
            loginDataBaseAdapter.insertCustomprof(1, "CK Low (270x480)", "270", "480", "400", "15", "2", "30", "sterio", "64000");
            loginDataBaseAdapter.insertCustomprof(1, "CK Medium (360x640)", "480", "640", "400", "15", "2", "30", "sterio", "64000");
            loginDataBaseAdapter.insertCustomprof(1, "CK High (540x960)", "480", "640", "400", "15", "2", "30", "sterio", "64000");
            loginDataBaseAdapter.insertCustomprof(1, "CK HD 720 (720x1280)", "480", "640", "400", "15", "2", "30", "sterio", "64000");
            loginDataBaseAdapter.insertCustomprof(1, "CK HD 1080 (1080x1920)", "480", "640", "400", "15", "2", "30", "sterio", "64000");
        }else{

        }



        originalContainerLayoutParams = rel_cntin.getLayoutParams();


        feedname.setText(Constant.username);//showing logged in user name



        loginDataBaseAdapter.getallids();//used for move profile list from database to arraylist with hashmap(Constant.New_database)


//get device id and mobile infomations
        String device_id = android.provider.Settings.Secure.getString(getContentResolver(),
                android.provider.Settings.Secure.ANDROID_ID);
        Log.d("device id", device_id);
        deviceidval=device_id;


        int sdkVersion = android.os.Build.VERSION.SDK_INT;

        StringBuilder builder = new StringBuilder();


        Field[] fields = Build.VERSION_CODES.class.getFields();
        for (Field field : fields) {
            String fieldName = field.getName();
            int fieldValue = -1;

            try {
                fieldValue = field.getInt(new Object());
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            if (fieldValue == Build.VERSION.SDK_INT) {
                builder.append(fieldName);
            }
        }


        osname=builder.toString();


        osversion=String.valueOf(sdkVersion);

        devicename = android.os.Build.MANUFACTURER;

        Log.d("osversion",osversion+osname);




        imeinumber="";


//get device id and mobile infomations



        Constant.cd = new ConnectionDetector(Livestream.this);
        Constant.isInternetPresent = Constant.cd.isConnectingToInternet();
        if (Constant.isInternetPresent) {



            //aws analytics
            try {
                analytics = MobileAnalyticsManager.getOrCreateInstance(
                        Livestream.this,
                        "dc6a313d22004de994c1fd81540e9992", //Amazon Mobile Analytics App ID
                        "us-east-1:11a1ae4f-18c0-45af-8528-de03b04760d0" //Amazon Cognito Identity Pool ID
                );
                this.onLevelComplete("Live Streaming Screen", 0, 0);
            } catch(InitializationException ex) {
                Log.e(this.getClass().getName(), "Failed to initialize Amazon Mobile Analytics", ex);
            }
            //aws analytics


            //get gcm token for push message

            GCMClientManager pushClientManager = new GCMClientManager(this, "378610203051");
            pushClientManager.registerIfNeeded(new GCMClientManager.RegistrationCompletedHandler() {
                @Override
                public void onSuccess(String registrationId, boolean isNewRegistration) {

                    Log.d("Registration id", registrationId);

                    gcmregisterid = registrationId;


                    if(gcmregisterid!=null){

                        //send device information
                        senddeviceinfo(gcmregisterid,deviceidval,devicename,osversion,imeinumber,osname);
                        //send device information


                        //get user location coordinates
                        GPSTracker  gps = new GPSTracker(Livestream.this);

                        // check if GPS enabled
                        if(gps.canGetLocation()){

                            double latitude = gps.getLatitude();
                            double longitude = gps.getLongitude();

                            String currentlatitude=String.valueOf(latitude);
                            String currentlongitude=String.valueOf(longitude);

                            // toastsettext(currentlatitude+"#"+currentlongitude);

                            Log.d("curenty",currentlatitude+"#"+currentlongitude);




                            if(currentlongitude!=null || currentlatitude!=null){

                                if(currentlatitude!="0.0" && currentlongitude!="0.0") {
                                    sendlocationinfo(currentlatitude, currentlongitude, gcmregisterid);
                                }
                                else{
                                    getLocation();
                                }


                            }else{

                            }


                            // \n is for new line
                            //  Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                        }else{
                            // can't get location
                            // GPS or Network is not enabled
                            // Ask user to enable GPS/network in settings
                            gps.showSettingsAlert();
                        }

              //get user location coordinates

                       // getLocation();
                    }





                    //manually insert device tokken
                    if(gcmregisterid!=null)
                    {

                        AsyncHttpClient clien = new AsyncHttpClient();
                        clien.get("http://nuaworks.com/church/sample/token.php?d_type=1&d_token="+gcmregisterid+"&d_id="+deviceidval, new AsyncHttpResponseHandler()

                        {


                            @Override
                            public void onSuccess(String arg0) {
                                super.onSuccess(arg0);


                                Log.e("post response", arg0);


                            }


                            @Override
                            public void onFinish() {
                                super.onFinish();
                                //-- Toast.makeText(getApplicationContext(), "finish", Toast.LENGTH_SHORT).show();
                                Log.d("post response", "Finnish");

                            }

                            @Override
                            public void onFailure(Throwable arg0, String arg1) {
                                super.onFailure(arg0, arg1);
                                //Toast.makeText(getApplicationContext(), "failure", Toast.LENGTH_SHORT).show();
                                Log.d("post response", "failure");
                            }

                        });

                    }


                    //manually insert device token
                }

                @Override
                public void onFailure(String ex) {
                    super.onFailure(ex);
                    //   Toast.makeText(getApplicationContext(),"failure:"+ex,Toast.LENGTH_SHORT).show();
                }
            });


            //get gcm token for push message

        }else{

        }



        profilelist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                    int dbcount = loginDataBaseAdapter.getcountofval(6);

                if (dbcount == 0) {
                    loginDataBaseAdapter.insertCustomprof(1, "CK Default (480x640)", "480", "640", "400", "15", "2", "30", "sterio", "64000");
                    loginDataBaseAdapter.insertCustomprof(1, "CK Low (270x480)", "270", "480", "400", "15", "2", "30", "sterio", "64000");
                    loginDataBaseAdapter.insertCustomprof(1, "CK Medium (360x640)", "480", "640", "400", "15", "2", "30", "sterio", "64000");
                    loginDataBaseAdapter.insertCustomprof(1, "CK High (540x960)", "480", "640", "400", "15", "2", "30", "sterio", "64000");
                    loginDataBaseAdapter.insertCustomprof(1, "CK HD 720 (720x1280)", "480", "640", "400", "15", "2", "30", "sterio", "64000");
                    loginDataBaseAdapter.insertCustomprof(1, "CK HD 1080 (1080x1920)", "480", "640", "400", "15", "2", "30", "sterio", "64000");
                } else {

                }


              /*  originalContainerLayoutParams = rel_cntin.getLayoutParams();


                feedname.setText(Constant.username);*/



                loginDataBaseAdapter.getallids();//move all profile list from database to Arraylist with hashmap(Constant.New_database)

                Constant.profilenames = new String[Constant.New_database.size()+1];
                for (int i = 0; i <= Constant.New_database.size()+1; i++) {
                    if (i != Constant.New_database.size()+1) {
                        if(i==Constant.New_database.size()){
                            Constant.profilenames[i]="Add Profile";
                            Log.d("valueinsert1",Constant.profilenames[i]+String.valueOf(i));
                        }else {
                            Constant.profilenames[i] = Constant.New_database.get(i).get("name");
                            //  Constant.profilenames[i] = String.valueOf(loginDataBaseAdapter.getprofilename(String.valueOf(i+1)));
                            Log.d("valueinsert2",Constant.profilenames[i]+String.valueOf(i));
                        }
                    } else {

                    }
                }




                final MyCustomAdapter mycustom = new MyCustomAdapter(Livestream.this, R.layout.profile_listitem, Constant.profilenames);

                LayoutInflater layoutInflater =
                        (LayoutInflater) getBaseContext()
                                .getSystemService(LAYOUT_INFLATER_SERVICE);
                View popupView = layoutInflater.inflate(R.layout.popup, null);
                popupWindow = new PopupWindow(
                        popupView, RelativeLayout.LayoutParams.MATCH_PARENT, 1200);


                Button btnDismiss = (Button) popupView.findViewById(R.id.dismiss);

                ListView list = (ListView) popupView.findViewById(R.id.list);


                list.setAdapter(mycustom);

                btnDismiss.setOnClickListener(new Button.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        popupWindow.dismiss();
                    }
                });


                popupWindow.showAsDropDown(popup, 50, 50, Gravity.CENTER);


            }
        });






        vediolist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constant.cd = new ConnectionDetector(getApplicationContext());
                Constant.isInternetPresent = Constant.cd.isConnectingToInternet();
                if (Constant.isInternetPresent) {
                    runOnUiThread(new Runnable() {
                                      @Override
                                      public void run() {
                                          prg_bar.show();
                                      }
                                  });
                    Constant.videolist.clear();
                    getvalues();//get videolist details from json
                }else{
                    toastsettext("No Internet Connection");
                }
            }
        });


        notifilist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Intent inten=new Intent(getApplicationContext(),Notificationlist.class);
                //startActivity(inten);
                Constant.cd = new ConnectionDetector(getApplicationContext());
                Constant.isInternetPresent = Constant.cd.isConnectingToInternet();
                if (Constant.isInternetPresent) {
                    runOnUiThread(new Runnable() {
                                      @Override
                                      public void run() {
                                          prg_bar.show();
                                      }
                                  });
                    getnotification();//get notificationlist details from json
                }  else{
                    toastsettext("No Internet Connection");
                }
            }
        });


    //changing screen based on screen orientation
        if (Livestream.this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){

            rel_cntin.setLayoutParams(originalContainerLayoutParams);
            rel_topmenu.setVisibility(View.VISIBLE);

        } else {

            Display mDisplay = Livestream.this.getWindowManager().getDefaultDisplay();
            final int width  = mDisplay.getWidth();
            final int height = mDisplay.getHeight();
            rel_topmenu.setVisibility(View.GONE);


            rel_cntin.setLayoutParams(Util.getLayoutParamsBasedOnParent(
                    rel_cntin,
                    width,
                    height
            ));


        }
        //changing screen based on screen orientation




    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        Constant.dayee = getSharedPreferences("dayfun", MODE_PRIVATE);
        Constant.daycom = Constant.dayee.edit();

        Constant.daycom.putBoolean("open",false);

        Constant.daycom.commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Constant.dayee = getSharedPreferences("dayfun", MODE_PRIVATE);
        Constant.daycom = Constant.dayee.edit();

        Constant.daycom.putBoolean("open",false);

        Constant.daycom.commit();
    }

    //aws analytics
    private void onLevelComplete(String levelName, double timeToComplete, int playerState) {

        AnalyticsEvent levelCompleteEvent = analytics.getEventClient().createEvent("Live Streaming Screen")
                .withAttribute("ScreenName", levelName)
                .withMetric("TimeToComplete", timeToComplete);
        //Record the Level Complete event
        Log.d("Live Streaming Screen", levelName);
        levelCompleteEvent.addAttribute("State", "Active");
        analytics.getEventClient().recordEvent(levelCompleteEvent);

    }
//aws analytics

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {

            Display mDisplay = Livestream.this.getWindowManager().getDefaultDisplay();
            final int width  = mDisplay.getWidth();
            final int height = mDisplay.getHeight();
            rel_topmenu.setVisibility(View.GONE);


            rel_cntin.setLayoutParams(Util.getLayoutParamsBasedOnParent(
                    rel_cntin,
                    width,
                    height
            ));


        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {

            rel_cntin.setLayoutParams(originalContainerLayoutParams);
            rel_topmenu.setVisibility(View.VISIBLE);

        }


    }

    private void notificationmain() {


        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    WebClient webClient = new WebClient();
                    String getRes = null;
                    try {
                        getRes = webClient.doGetReq("https://u2mrf96skg.execute-api.ap-southeast-1.amazonaws.com/mobiro/notify/list?deviceid="+gcmregisterid);
                        Log.d("Home_page_Array :", getRes);

                        Log.e("post response", getRes);


                        try {
                            final JSONArray respons = new JSONArray(getRes);

                            if(respons.length()!=0){
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                      /*   noti_count.setVisibility(View.VISIBLE);
                                         noti_count.setText(String.valueOf(respons.length()));
                                        notifilist.setBackgroundResource(R.drawable.noticount);*/
                                    }
                                });

                            }else{
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                      /*   noti_count.setVisibility(View.GONE);
                                         notifilist.setBackgroundResource(R.drawable.nitifi_icon);*/
                                    }
                                });
                            }





                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d("post response", "Finnish");






                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                } catch (Exception ex) {
                    ex.printStackTrace();

                }
            }
        }).start();


    }

    private void findViewbyid() {
        vediolist = (ImageView) findViewById(R.id.vedio_sett);
        notifilist = (ImageView) findViewById(R.id.notifi);
        profilelist=(ImageView) findViewById(R.id.profile_list);
        noti_count=(TextView) findViewById(R.id.noticounttxt);
        spnr = (Spinner)findViewById(R.id.spinner);
        mTxtFrameSize = (TextView) findViewById(R.id.txtFrameSize);
        feedname=(TextView) findViewById(R.id.userfeed);

        rel_cntin=(RelativeLayout) findViewById(R.id.rel_cntin);
        rel_topmenu=(RelativeLayout) findViewById(R.id.rel_topmenu);
        statusview=(StatusView) findViewById(R.id.statusView);
        popup=(Button) findViewById(R.id.popup) ;

    }


    //toggle button for mute audio while recording
    public void onToggleMute(View v) {
        mBtnMic.toggleState();

        if (getBroadcast().getStatus().isRunning()) {
            mWZAudioDevice.setAudioPaused(!mBtnMic.isOn());
          //  Toast.makeText(getApplicationContext(),String.valueOf(mBtnMic.isOn()),Toast.LENGTH_SHORT).show();
         //   Toast.makeText(this, "Audio stream " + (mWZAudioDevice.isAudioPaused() ? "muted" : "enabled"), Toast.LENGTH_SHORT).show();
        } else {
            // mAudioLevelMeter.setVisibility(mBtnMic.isOn() ? View.VISIBLE : View.GONE);

            if (mBtnMic.isOn())
                mWZAudioDevice.startAudioSampler();
            else
                mWZAudioDevice.stopAudioSampler();
        }
    }


    //send device information
    private void senddeviceinfo(final String gcmregisteridd, final String deviceidval, final String devicename, final String osversion, final String imeinumber, final String osname) {


        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    //device info update on aws server

                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                    AWSCredentials awsCredentials = new BasicAWSCredentials("AKIAJQWIJHZX4MASWLHQ", "Gpj/qODpd1ShHBi3kHfBk5laAMjENLUnuSD5AkMV");

                    String platformApplicationArn = "arn:aws:sns:ap-southeast-1:684719585714:app/GCM/serverkey";
                    AmazonSNSClient pushClient = new AmazonSNSClient(awsCredentials);
                    pushClient.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_1));

                    String customPushData = Constant.username+timeStamp;
                    CreatePlatformEndpointRequest platformEndpointRequest = new CreatePlatformEndpointRequest();
                    platformEndpointRequest.setCustomUserData(customPushData);
                    platformEndpointRequest.setToken(gcmregisteridd);

                    platformEndpointRequest.setPlatformApplicationArn(platformApplicationArn);
                    pushClient.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_1));
                    CreatePlatformEndpointResult result = pushClient.createPlatformEndpoint(platformEndpointRequest);

                    Log.d("autherror", "Amazon Push reg result: " + result);

                    //device info update




                    WebClient webClient = new WebClient();
                    String getRes = null;
                    try {
                        Log.d("rer","https://u2mrf96skg.execute-api.ap-southeast-1.amazonaws.com/mobiro/device?uname="+Constant.username+"&device_id="+gcmregisteridd+"&device_model=Android&version="+osversion+osname+"&imei="+imeinumber+"&os=Android");
                        getRes = webClient.doGetReq("https://u2mrf96skg.execute-api.ap-southeast-1.amazonaws.com/mobiro/device?uname="+Constant.username+"&device_id="+gcmregisteridd+"&device_model=Android&version="+osversion+osname+"&imei="+imeinumber+"&os=Android");
                        Log.d("Home_page_Array :", getRes);

                        Log.e("post response", getRes);


                        try {
                            JSONArray respons = new JSONArray(getRes);
                            Log.d("res", String.valueOf(respons));






                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    //  Log.d("namadhu top Size : ", "" + Constant.namadhu_top_news_Array.size());


                } catch (Exception ex) {
                    ex.printStackTrace();

                }
            }
        }).start();


    }

    //get video list details from json
    private void getvalues() {

        Constant.videolist.clear();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    WebClient webClient = new WebClient();
                    String getRes = null;
                    try {
                        getRes = webClient.doGetReq("https://u2mrf96skg.execute-api.ap-southeast-1.amazonaws.com/mobiro/videolist?fname="+Constant.username);
                        Log.d("Home_page_Array :", getRes);

                        Log.e("post response", getRes);




                        try {
                            JSONArray respons = new JSONArray(getRes);


                            for (int i = 0; i < respons.length(); i++) {
                                HashMap<String, String> map = new HashMap<String, String>();
                                JSONObject jsonobject = respons.getJSONObject(i);
                                String name = jsonobject.getString("name");
                                String url = jsonobject.getString("stream_url");
                                String bitrate = jsonobject.getString("bit_rate");
                                String ratio = jsonobject.getString("aspect_ratio");
                                String thumb = jsonobject.getString("thumb_loc");
                                String videoid = jsonobject.getString("video_id");
                                String timevalue=jsonobject.getString("time_showing");
                                String size=jsonobject.getString("size");
                                String duration=jsonobject.getString("duration");
                                String longitudee=jsonobject.getString("longitude");
                                String latitudee=jsonobject.getString("latitude");


                                String date=jsonobject.getString("created_at");

                               // String addresscity=getcity(latitudee,longitudee,i);
                                String addresscity="Not Available";

                                String videotitle=null;
                                String videodescription=null;
                                String videotype=null;
                                String videotag="";

                                if(jsonobject.has("Tags")){
                                    videotag=jsonobject.getString("Tags");
                                }
                                if(jsonobject.has("Title"))
                                {
                                    videotitle=jsonobject.getString("Title");
                                }

                                if(jsonobject.has("Description"))
                                {
                                    videodescription = jsonobject.getString("Description");
                                }

                                if(jsonobject.has("VideoType"))
                                {
                                    videotype=jsonobject.getString("VideoType");
                                }



                                map.put("videoid",videoid);
                                map.put("thumb",thumb);
                                map.put("name",name);
                                map.put("vidurl",url);
                                map.put("time",timevalue);
                                map.put("date",date);
                                map.put("vidsize",size);
                                map.put("viddur",duration);
                                map.put("vidlat",latitudee);
                                map.put("vidlong",longitudee);



                                map.put("vidtitle",videotitle);
                                map.put("viddesc",videodescription);
                                map.put("vidtype",videotype);
                                map.put("vidtag",videotag);
                                map.put("vidplace",addresscity);


                                Constant.videolist.add(map);

                            }


                            int currentVersion = android.os.Build.VERSION.SDK_INT;
                            if (currentVersion >= Build.VERSION_CODES.M) {
                               //get city name and navigate video list screen
                                Intent inten=new Intent(getApplicationContext(),Vedio_list.class);//navigate to Video list screen
                                startActivity(inten);
                                prg_bar.dismiss();

                            }else{
                                new MyAsyncTask().execute();
                            }



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                } catch (Exception ex) {
                    ex.printStackTrace();

                }
            }
        }).start();


    }


    //get place for show city in video list
    private void getlocations() {

        Constant.locationlist.clear();
        //map.put("vidlat",latitudee);
       // map.put("vidlong",longitudee);
        for(int it = 0; it<=Constant.videolist.size(); it++){
            if(it!=Constant.videolist.size()) {
                final HashMap<String, String> map = new HashMap<String, String>();
              // final String value= getcity(Constant.videolist.get(it).get("vidlat"), Constant.videolist.get(it).get("vidlong"), it);

                final int finalIt = it;

                float curlant =0;
                float curlongi=0;

                String vidlat= Constant.videolist.get(it).get("vidlat");
                String vidlong= Constant.videolist.get(it).get("vidlong");

                if(!vidlat.contains(" null")) {
                    //
                    if(vidlat.contains("null")) {
                        mapnull=true;
                        vidlat="-3.962901";
                        curlant = Float.parseFloat(vidlat);
                    }else{
                        mapnull=false;
                        curlant = Float.parseFloat(vidlat);
                    }
                  }else{
                    mapnull=true;
                    vidlat="-3.962901";
                    curlant = Float.parseFloat(vidlat);
                }

                if(!vidlong.contains(" null")){
                    if(vidlong.contains("null")){
                        mapnull=true;
                        vidlong="81.562500";
                        curlongi = Float.parseFloat(vidlong);
                    }else{
                        mapnull=false;
                        curlongi = Float.parseFloat(vidlong);
                    }


                }else{
                    mapnull=true;
                    vidlong="81.562500";
                    curlongi = Float.parseFloat(vidlong);
                }




if(curlongi!=Double.parseDouble("81.562500")) {

    List<Address> addresses = null;

    //  Geocoder geocoder = new Geocoder(this, Locale.getDefault());;
    geocoder = new Geocoder(this, Locale.getDefault());

    try {

        Log.d("curlat", String.valueOf(curlant));
        Log.d("curlng", String.valueOf(curlongi));
        addresses = geocoder.getFromLocation(Double.parseDouble(String.valueOf(curlant)), Double.parseDouble(String.valueOf(curlongi)), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
    } catch (IOException e) {
        e.printStackTrace();
    }

    Log.d("addresseson", String.valueOf(addresses));
    String address = null, place = null, where, city, state = null, country = null, postalCode = null, knownName;

    if (addresses.size() != 0) {

        place = addresses.get(0).getSubAdminArea();

    }



    map.put("vidlocation", place);

}else{
    map.put("vidlocation","Not Available");
}





               Constant.locationlist.add(map);

            }else{

            }
        }

        Intent inten=new Intent(getApplicationContext(),Vedio_list.class);//navigate to Video list screen
        startActivity(inten);
        prg_bar.dismiss();

    }



//get notification list from json
    private void getnotification(){

        Constant.notificationlist.clear();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    WebClient webClient = new WebClient();
                    String getRes = null;
                    try {
                        getRes = webClient.doGetReq("https://u2mrf96skg.execute-api.ap-southeast-1.amazonaws.com/mobiro/notify/list?deviceid="+gcmregisterid);
                        Log.d("Home_page_Array :", getRes);

                        Log.e("post response", getRes);


                        try {
                            JSONArray respons = new JSONArray(getRes);


                            for (int i = 0; i < respons.length(); i++) {
                                HashMap<String, String> map = new HashMap<String, String>();
                                JSONObject jsonobject = respons.getJSONObject(i);
                                String notification = jsonobject.getString("messsage");
                                String date =jsonobject.getString("date");
                                String time=jsonobject.getString("time");


                                map.put("notification",notification);
                                map.put("date",date);
                                map.put("time",time);


                                Constant.notificationlist.add(map);

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d("post response", "Finnish");
                        if(prg_bar.isShowing()){
                            prg_bar.dismiss();
                            prg_bar.cancel();
                        }else{

                        }

                        Intent inten=new Intent(getApplicationContext(),Notificationlist.class);
                        startActivity(inten);
                        Constant.notificationcount=0;
                        prg_bar.dismiss();

                        if(noti_count.getVisibility()==View.VISIBLE){
                            noti_count.setVisibility(View.GONE);
                        }else{

                        }





                        // setList();

                        //adapter = new NotificationAdapter(Notificationlist.this, allList);
                        // mRecyclerView.setAdapter(adapter);

                        //setupSearchView();


                    } catch (IOException e) {
                        e.printStackTrace();
                        if(prg_bar.isShowing()){
                            prg_bar.dismiss();
                            prg_bar.cancel();
                        }else{

                        }
                    }
                    Log.d("namadhu top Size : ", "" + Constant.namadhu_top_news_Array.size());


                } catch (Exception ex) {
                    ex.printStackTrace();
                    if(prg_bar.isShowing()){
                        prg_bar.dismiss();
                        prg_bar.cancel();
                    }else{

                    }

                }
            }
        }).start();


    }

    //get location using google api if gcm fails to retrive location
    private void getLocation() {




        AsyncHttpClient clien = new AsyncHttpClient();
        clien.post("https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyDjmM0FLyUX14RIFq8O5ZU5KvB01BFYHJU", new AsyncHttpResponseHandler()

        {


            @Override
            public void onSuccess(String arg0) {
                super.onSuccess(arg0);


                Log.e("post response", arg0);
                JSONObject respons = null;
                try {
                    respons = new JSONObject(arg0);
                    JSONObject response2=respons.getJSONObject("location");
                    latitude=response2.getString("lat");
                    longtidude=response2.getString("lng");

                    Log.d("lati",latitude);
                    Log.d("longi",longtidude);

                  //  toastsettext(String.valueOf(latitude +"#"+longtidude));

                } catch (JSONException e) {
                    e.printStackTrace();
                }




            }


            @Override
            public void onFinish() {
                super.onFinish();
                //-- Toast.makeText(getApplicationContext(), "finish", Toast.LENGTH_SHORT).show();
                Log.d("post response", "Finnish");
                sendlocationinfo(latitude, longtidude, gcmregisterid);

            }

            @Override
            public void onFailure(Throwable arg0, String arg1) {
                super.onFailure(arg0, arg1);
                //Toast.makeText(getApplicationContext(), "failure", Toast.LENGTH_SHORT).show();
                Log.d("post response", "failure");
            }

        });




    }

//send location coordinates to server
    private void sendlocationinfo(final String latitude, final String longitude, final String deviceidval){

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    WebClient webClient = new WebClient();
                    String getRes = null;
                    try {
                        getRes = webClient.doGetReq("https://u2mrf96skg.execute-api.ap-southeast-1.amazonaws.com/mobiro/device/geo?email="+Constant.username+"@mobi-ro.com&deviceid="+gcmregisterid+"&Latitude="+latitude+"&Longitude="+longitude);
                        Log.d("locationapi", "https://u2mrf96skg.execute-api.ap-southeast-1.amazonaws.com/mobiro/device/geo?email="+Constant.username+"@mobi-ro.com&deviceid="+gcmregisterid+"&Latitude="+latitude+"&Longitude="+longitude);



                        Log.e("locationlog", getRes);


                        try {
                            JSONArray respons = new JSONArray(getRes);
                            Log.d("res", String.valueOf(respons));






                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    //  Log.d("namadhu top Size : ", "" + Constant.namadhu_top_news_Array.size());


                } catch (Exception ex) {
                    ex.printStackTrace();

                }
            }
        }).start();

    }


    public void toastsettext(String string1) {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.altdialog_toast,
                (ViewGroup) findViewById(R.id.toast_rl));
        TextView txt = (TextView) layout.findViewById(R.id.toast_txt);
        txt.setText(string1);
        Toast tst = new Toast(getApplicationContext());
        tst.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        tst.setDuration(Toast.LENGTH_SHORT);
        tst.setView(layout);
        tst.show();
    }


    public void onLogout(View v){

        Constant.dayee = getSharedPreferences("dayfun", MODE_PRIVATE);
        Constant.daycom = Constant.dayee.edit();
        Constant.daycom.putBoolean("login",false);

        Constant.daycom.commit();

        Intent inten =new Intent(getApplicationContext(),Login.class);
        startActivity(inten);
        Livestream.this.finish();
    }

    /**
     * Android Activity lifecycle methods
     */
    @Override
    protected void onResume() {
        super.onResume();

        if (mWZAudioDevice != null) {
            //  mWZAudioDevice.registerAudioSampleListener(mAudioLevelMeter);

            mBtnMic.setState(true);
            //   mAudioLevelMeter.setVisibility(View.VISIBLE);
            mWZAudioDevice.startAudioSampler();

          //  Toast.makeText(this, getString(R.string.audio_meter_help), Toast.LENGTH_LONG).show();
        } else {
            mBtnMic.setEnabled(false);
            //  mAudioLevelMeter.setVisibility(View.GONE);
        }

        if (sGoCoderSDK != null && mAutoFocusDetector == null) {
            mAutoFocusDetector = new GestureDetectorCompat(this, new AutoFocusListener(this, mWZCameraView));
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mWZAudioDevice != null) {
            if (mWZAudioDevice.isSamplingAudio())
                mWZAudioDevice.stopAudioSampler();
            //  mWZAudioDevice.unregisterAudioSampleListener(mAudioLevelMeter);
        }
    }

    /**
     * Click handler for the switch camera button
     */
    public void onSwitchCamera(View v) {
        if (mWZCameraView == null) return;

        WZCamera newCamera = mWZCameraView.switchCamera();
        if (newCamera != null)
            ConfigPrefs.setActiveCamera(PreferenceManager.getDefaultSharedPreferences(this), newCamera.getCameraId());

        boolean hasTorch = (newCamera != null && newCamera.hasCapability(WZCamera.TORCH));
        if (hasTorch)
            mBtnTorch.setState(newCamera.isTorchOn());
        else
            mBtnTorch.setState(false);
        mBtnTorch.setEnabled(hasTorch);
    }

    /**
     * Click handler for the torch/flashlight button
     */
    public void onToggleTorch(View v) {
        if (mWZCameraView == null) return;

        WZCamera activeCamera = mWZCameraView.getCamera();
        activeCamera.setTorchOn(mBtnTorch.toggleState());
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (mAutoFocusDetector != null)
            mAutoFocusDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    /**
     * Update the state of the UI controls
     */
    @Override
    protected boolean syncUIControlState() {
        boolean disableControls = super.syncUIControlState();

        if (disableControls) {
            mBtnMic.setEnabled(false);
        } else if (mWZAudioDevice != null ) {
            boolean isStreaming = getBroadcast().getStatus().isRunning();
            boolean isStreamingAudio = (isStreaming && getBroadcastConfig().isAudioEnabled());
            boolean isSamplingAudio = (mWZAudioDevice.isSamplingAudio()||isStreamingAudio);

            mBtnMic.setEnabled(!isStreaming||isStreamingAudio);
            mBtnMic.setState(isSamplingAudio);
            // mAudioLevelMeter.setVisibility(isSamplingAudio ? View.VISIBLE : View.GONE);
        } else {
            mBtnMic.setEnabled(false);
            //  mAudioLevelMeter.setVisibility(View.GONE);
        }



        if (disableControls) {
            mBtnSwitchCamera.setEnabled(false);
            mBtnTorch.setEnabled(false);
            mBtnMic.setEnabled(false);
        } else {
            boolean isDisplayingVideo = (getBroadcastConfig().isVideoEnabled() && mWZCameraView.getCameras().length > 0);
            boolean isStreaming = getBroadcast().getStatus().isRunning();

            if (isDisplayingVideo) {
                WZCamera activeCamera = mWZCameraView.getCamera();

                boolean hasTorch = (activeCamera != null && activeCamera.hasCapability(WZCamera.TORCH));
                mBtnTorch.setEnabled(hasTorch);
                if (hasTorch) {
                    mBtnTorch.setState(activeCamera.isTorchOn());
                }

                mBtnSwitchCamera.setEnabled(mWZCameraView.isSwitchCameraAvailable());


            } else {
                mBtnSwitchCamera.setEnabled(false);
                mBtnTorch.setEnabled(false);
            }

            if (isStreaming && !mTimerView.isRunning()) {
                mTimerView.startTimer();
            } else if (getBroadcast().getStatus().isIdle() && mTimerView.isRunning()) {
                mTimerView.stopTimer();
            } else if (!isStreaming) {
                mTimerView.setVisibility(View.VISIBLE);
            }
        }

        return disableControls;
    }


    //choose custom profile
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {



        loginDataBaseAdapter = new LoginDataBaseAdapter(Livestream.this);
        loginDataBaseAdapter = loginDataBaseAdapter.open();




       // int position = spnr.getSelectedItemPosition();
        //  Toast.makeText(getApplicationContext(), "You have selected " + celebrities[+position], Toast.LENGTH_LONG).show();

        if (position == 0) {

            Constant.cd = new ConnectionDetector(getApplicationContext());
            Constant.isInternetPresent = Constant.cd.isConnectingToInternet();
            if (Constant.isInternetPresent) {

                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(Livestream.this);
                SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
                prefsEditor.putInt("wz_video_frame_width", WZMediaConfig.DEFAULT_VIDEO_FRAME_WIDTH);
                prefsEditor.putInt("wz_video_frame_height", WZMediaConfig.DEFAULT_VIDEO_FRAME_HEIGHT);
                prefsEditor.putString("wz_video_frame_rate", String.valueOf(WZMediaConfig.DEFAULT_VIDEO_FRAME_RATE));
                prefsEditor.putString("wz_video_keyframe_interval", String.valueOf(WZMediaConfig.DEFAULT_VIDEO_KEYFRAME_INTERVAL));
                prefsEditor.putString("wz_video_bitrate", String.valueOf(WZMediaConfig.DEFAULT_VIDEO_BITRATE));
                prefsEditor.putBoolean("wz_audio_enabled", true);
                prefsEditor.putBoolean("wz_video_enabled", true);
                prefsEditor.putBoolean("wz_audio_stereo", true);
                prefsEditor.putString("wz_audio_sample_rate", String.valueOf(WZMediaConfig.DEFAULT_AUDIO_SAMPLE_RATE));
                prefsEditor.putString("wz_audio_bitrate", String.valueOf(WZMediaConfig.DEFAULT_AUDIO_BITRATE));

                prefsEditor.apply();
                prefsEditor.commit();
                ConfigPrefs.updateConfigFromPrefs(PreferenceManager.getDefaultSharedPreferences(Livestream.this), mWZBroadcastConfig);

                sGoCoderSDK.startCameraPreview(mWZBroadcastConfig);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //textchange
                        mTxtFrameSize.setText(WZMediaConfig.DEFAULT_VIDEO_FRAME_HEIGHT + "x" + WZMediaConfig.DEFAULT_VIDEO_FRAME_WIDTH);
                       // mGestureDetector = new GestureDetectorCompat(Livestream.this, new FocusGestureListener());

                        //textchange
                    }
                });

            }else{
                toastsettext("No Internet Connection");
            }

        } else if (position == 1) {
            Constant.cd = new ConnectionDetector(getApplicationContext());
            Constant.isInternetPresent = Constant.cd.isConnectingToInternet();
            if (Constant.isInternetPresent) {
                getprofile("https://u2mrf96skg.execute-api.ap-southeast-1.amazonaws.com/mobiro/profile/settings?pname=low");
            }else{
                toastsettext("No Internet Connection");
            }
        } else if (position == 2) {


            Constant.cd = new ConnectionDetector(getApplicationContext());
            Constant.isInternetPresent = Constant.cd.isConnectingToInternet();
            if (Constant.isInternetPresent) {
                getprofile("https://u2mrf96skg.execute-api.ap-southeast-1.amazonaws.com/mobiro/profile/settings?pname=medium");
            }else{
                toastsettext("No Internet connection");
            }
        } else if (position == 3) {
            Constant.cd = new ConnectionDetector(getApplicationContext());
            Constant.isInternetPresent = Constant.cd.isConnectingToInternet();
            if (Constant.isInternetPresent) {
                getprofile("https://u2mrf96skg.execute-api.ap-southeast-1.amazonaws.com/mobiro/profile/settings?pname=high");
            }else{
                toastsettext("No Internet Connection");
            }
        } else if (position == 4) {
            Constant.cd = new ConnectionDetector(getApplicationContext());
            Constant.isInternetPresent = Constant.cd.isConnectingToInternet();
            if (Constant.isInternetPresent) {
                getprofile("https://u2mrf96skg.execute-api.ap-southeast-1.amazonaws.com/mobiro/profile/settings?pname=HD%20720");

            }else{
                toastsettext("No Internet Connection");
            }
        } else if (position == 5) {
            Constant.cd = new ConnectionDetector(getApplicationContext());
            Constant.isInternetPresent = Constant.cd.isConnectingToInternet();
            if (Constant.isInternetPresent) {

                getprofile("https://u2mrf96skg.execute-api.ap-southeast-1.amazonaws.com/mobiro/profile/settings?pname=HD 1080");
            }else{
                toastsettext("No Internet Connection");
            }
        }else if(position==loginDataBaseAdapter.getcountofval(6)){
            popupWindow.dismiss();
            Intent inten =new Intent(getApplicationContext(),Neweditprofile.class);
            startActivity(inten);
        }else{
            getdatabasevalues(position);
        }

    }
    private void getdatabasevalues(int position) {

        loginDataBaseAdapter = new LoginDataBaseAdapter(Livestream.this);
        loginDataBaseAdapter = loginDataBaseAdapter.open();

        HashMap<String, String> map = new HashMap<String, String>();
        String width=loginDataBaseAdapter.getprofilewidth(String.valueOf(position + 1));
        String height=loginDataBaseAdapter.getprofileheight(String.valueOf(position + 1));

        String resolution = height+"x"+width ;
        String vid_brate = loginDataBaseAdapter.getprofilebitrate(String.valueOf(position + 1));
        String aud_brate = loginDataBaseAdapter.getprofileaudiobitrate(String.valueOf(position + 1));
        String fps = loginDataBaseAdapter.getprofileframerate(String.valueOf(position + 1));
        String finterval = loginDataBaseAdapter.getprofilekeyinterval(String.valueOf(position + 1));



        showresumee(resolution,vid_brate,aud_brate,fps,finterval);

    }

    private void showresumee(String resolution, String vid_brate, String aud_brate, String fps, String finterval) {

        if (sGoCoderSDK != null) {

            sGoCoderSDK.setCameraView(mWZCameraView);

            String ress=resolution;
            String vbr=vid_brate;
            String abr=aud_brate;
            String fpss=fps;
            String fintervall=finterval;

            String[] separated = ress.split("x");
            String strwid =separated[0];
            String strht=separated[1];

            final int width=Integer.parseInt(strwid);
            final int height=Integer.parseInt(strht);

            String videobit=null;
            if(vbr.contains("kbps")) {
                videobit =vbr.replace("kbps", "");
            }else{
                videobit=vbr;
            }



            String strstart=null;
            String strend=null;
            if(videobit.contains("-")){
                String[] separate = videobit.split("-");
                strstart =separate[0];
                strend=separate[1];
            }else{
                strend=videobit;
            }


            // int start=Integer.parseInt(strstart);
            int end=Integer.parseInt(strend);


            String audbit = null;
            if(abr.contains("kbps")) {
                audbit = abr.replace("kbps", "000");
            }else{
                audbit=abr;
            }
            int audiobitrate=Integer.parseInt(audbit);


            String val=null;
            if(fpss.contains("/")){
                String[] separate = fpss.split("/");
                val =separate[0];
            }else{
                if(fpss.contains("*")){
                    val=fpss.replace("*","");
                }else {
                    val = fpss;
                }
            }

            //  String[] separatedd = fps.split("/");
            //  String val =separatedd[0];

            int ifps= Integer.parseInt(val);

            String finter=null;
            if(fintervall.contains("seconds")) {
                finter = fintervall.replace("seconds", "");
            }else{
                finter=fintervall;
            }
            int frameinterval=Integer.parseInt(finter);








//oldchange

            //oldchange




            //orginalchange
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(Livestream.this);
            SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
            prefsEditor.putInt("wz_video_frame_width", width);
            prefsEditor.putInt("wz_video_frame_height", height);
            prefsEditor.putString("wz_video_frame_rate", String.valueOf(ifps));
            prefsEditor.putString("wz_video_keyframe_interval", String.valueOf(frameinterval));
            prefsEditor.putString("wz_video_bitrate", String.valueOf(end));
            prefsEditor.putBoolean("wz_audio_enabled", true);
            prefsEditor.putBoolean("wz_video_enabled", true);
            prefsEditor.putBoolean("wz_audio_stereo", true);
            prefsEditor.putString("wz_audio_sample_rate", "41000");
            prefsEditor.putString("wz_audio_bitrate", String.valueOf(audiobitrate));

            prefsEditor.apply();
            prefsEditor.commit();
            ConfigPrefs.updateConfigFromPrefs(PreferenceManager.getDefaultSharedPreferences(Livestream.this), mWZBroadcastConfig);


            //





            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //textchange
                    onResume();
                    sGoCoderSDK.startCameraPreview(mWZBroadcastConfig);
                   mTxtFrameSize.setText(String.valueOf(height) + "x" + String.valueOf(width));
                  //  mGestureDetector = new GestureDetectorCompat(CameraActivity.this, new FocusGestureListener());
                    //textchange

                }
            });
            //orginalchange






            //old

            //oldchange

            //oldchange





//old



        } else {
            mStatusView.setErrorMessage(String.valueOf(WowzaGoCoder.getLastError()));

        }

    }


    private void getprofile(final String s) {

     //   onPause();

        Constant.profilelist.clear();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    WebClient webClient = new WebClient();
                    String getRes = null;
                    try {
                        getRes = webClient.doGetReq(s);
                        Log.d("Home_page_Array :", getRes);

                        Log.e("post response", getRes);


                        try {
                            JSONArray respons = new JSONArray(getRes);


                            HashMap<String, String> map = new HashMap<String, String>();

                            /// for(int i=0;i<respons.length();i++) {

                            String res = respons.getString(0);

                            JSONObject obj = new JSONObject(res);

                            String resolution = obj.getString("resolution");
                            String vid_brate = obj.getString("video_bit_rate");
                            String aud_brate = obj.getString("audio_bit_rate");
                            String fps = obj.getString("FPS");
                            String finterval = obj.getString("key_frame_interval");

                            map.put("resolution", resolution);
                            map.put("vid_brate", vid_brate);
                            map.put("aud_brate", aud_brate);
                            map.put("fps", fps);
                            map.put("finterval", finterval);

                            Constant.profilelist.add(map);
                            // }


                           // showresume();

                            showresumee(resolution,vid_brate,aud_brate,fps,finterval);








                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    //  Log.d("namadhu top Size : ", "" + Constant.namadhu_top_news_Array.size());


                } catch (Exception ex) {
                    ex.printStackTrace();

                }
            }
        }).start();



    }

    private void showresume() {
        if (sGoCoderSDK != null) {

            sGoCoderSDK.setCameraView(mWZCameraView);

            String ress=Constant.profilelist.get(0).get("resolution");
            String vbr=Constant.profilelist.get(0).get("vid_brate");
            String abr=Constant.profilelist.get(0).get("aud_brate");
            String fpss=Constant.profilelist.get(0).get("fps");
            String fintervall=Constant.profilelist.get(0).get("finterval");

            String[] separated = ress.split("x");
            String strwid =separated[0];
            String strht=separated[1];

            final int width=Integer.parseInt(strwid);
            final int height=Integer.parseInt(strht);

            String videobit=null;
            if(vbr.contains("kbps")) {
                videobit =vbr.replace("kbps", "");
            }else{
                videobit=vbr;
            }



            String strstart=null;
            String strend=null;
            if(videobit.contains("-")){
                String[] separate = videobit.split("-");
                strstart =separate[0];
                strend=separate[1];
            }else{
                strend=videobit;
            }


            // int start=Integer.parseInt(strstart);
            int end=Integer.parseInt(strend);


            String audbit = null;
            if(abr.contains("kbps")) {
                audbit = abr.replace("kbps", "000");
            }else{
                audbit=abr;
            }
            int audiobitrate=Integer.parseInt(audbit);


            String val=null;
            if(fpss.contains("/")){
                String[] separate = fpss.split("/");
                val =separate[0];
            }else{
                if(fpss.contains("*")){
                    val=fpss.replace("*","");
                }else {
                    val = fpss;
                }
            }

            //  String[] separatedd = fps.split("/");
            //  String val =separatedd[0];

            int ifps= Integer.parseInt(val);

            String finter=null;
            if(fintervall.contains("seconds")) {
                finter = fintervall.replace("seconds", "");
            }else{
                finter=fintervall;
            }
            int frameinterval=Integer.parseInt(finter);








//oldchange

            //oldchange




            //orginalchange
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(Livestream.this);
            SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
            prefsEditor.putInt("wz_video_frame_width", width);
            prefsEditor.putInt("wz_video_frame_height", height);
            prefsEditor.putString("wz_video_frame_rate", String.valueOf(ifps));
            prefsEditor.putString("wz_video_keyframe_interval", String.valueOf(frameinterval));
            prefsEditor.putString("wz_video_bitrate", String.valueOf(end));
            prefsEditor.putBoolean("wz_audio_enabled", true);
            prefsEditor.putBoolean("wz_video_enabled", true);
            prefsEditor.putBoolean("wz_audio_stereo", true);
            prefsEditor.putString("wz_audio_sample_rate", "41000");
            prefsEditor.putString("wz_audio_bitrate", String.valueOf(audiobitrate));

            prefsEditor.apply();
            prefsEditor.commit();
            ConfigPrefs.updateConfigFromPrefs(PreferenceManager.getDefaultSharedPreferences(Livestream.this), mWZBroadcastConfig);





            sGoCoderSDK.startCameraPreview(mWZBroadcastConfig);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //textchange
                   mTxtFrameSize.setText(String.valueOf(height) + "x" + String.valueOf(width));

                }
            });
            //orginalchange



            //oldchange





//old



        } else {
            mStatusView.setErrorMessage(String.valueOf(WowzaGoCoder.getLastError()));

        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public static void updatenoticount(int valuecount) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                noti_count.setVisibility(View.VISIBLE);
                noti_count.setText(String.valueOf(Constant.notificationcount));

                displaydialog();

            }
        });
    }


    //showing push alert when push message arrived
    private static void displaydialog() {


        // Showing Alert Message




        alertDialog.setTitle("Mobiro");
        alertDialog.setIcon(R.drawable.ic_launcher);
        alertDialog.setMessage(Constant.pushmessage);
        alertDialog.show();



        new CountDownTimer(2000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onFinish() {
                // TODO Auto-generated method stub

                alertDialog.dismiss();
            }
        }.start();


    }


    //custom profile list dialog box
    public class MyCustomAdapter extends ArrayAdapter<String>{

        public MyCustomAdapter(Livestream context, int textViewResourceId,
                               String[] objects) {
            super((Context) context, textViewResourceId, objects);
// TODO Auto-generated constructor stub
        }

        @Override
        public View getDropDownView(int position, View convertView,
                                    ViewGroup parent) {
// TODO Auto-generated method stub
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
// TODO Auto-generated method stub
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(final int position, View convertView, ViewGroup parent) {
// TODO Auto-generated method stub
//return super.getView(position, convertView, parent);

            LayoutInflater inflater=getLayoutInflater();
            View row=inflater.inflate(R.layout.profile_listitem, parent, false);
            final TextView label=(TextView)row.findViewById(R.id.profile_name);
            RelativeLayout rel=(RelativeLayout)row.findViewById(R.id.rel_prof);


            rel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    popupWindow.dismiss();

                    if (position == 0) {

                        Constant.cd = new ConnectionDetector(getApplicationContext());
                        Constant.isInternetPresent = Constant.cd.isConnectingToInternet();
                        if (Constant.isInternetPresent) {

                            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(Livestream.this);
                            SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
                            prefsEditor.putInt("wz_video_frame_width", WZMediaConfig.DEFAULT_VIDEO_FRAME_WIDTH);
                            prefsEditor.putInt("wz_video_frame_height", WZMediaConfig.DEFAULT_VIDEO_FRAME_HEIGHT);
                            prefsEditor.putString("wz_video_frame_rate", String.valueOf(WZMediaConfig.DEFAULT_VIDEO_FRAME_RATE));
                            prefsEditor.putString("wz_video_keyframe_interval", String.valueOf(WZMediaConfig.DEFAULT_VIDEO_KEYFRAME_INTERVAL));
                            prefsEditor.putString("wz_video_bitrate", String.valueOf(WZMediaConfig.DEFAULT_VIDEO_BITRATE));
                            prefsEditor.putBoolean("wz_audio_enabled", true);
                            prefsEditor.putBoolean("wz_video_enabled", true);
                            prefsEditor.putBoolean("wz_audio_stereo", true);
                            prefsEditor.putString("wz_audio_sample_rate", String.valueOf(WZMediaConfig.DEFAULT_AUDIO_SAMPLE_RATE));
                            prefsEditor.putString("wz_audio_bitrate", String.valueOf(WZMediaConfig.DEFAULT_AUDIO_BITRATE));

                            prefsEditor.apply();
                            prefsEditor.commit();
                            ConfigPrefs.updateConfigFromPrefs(PreferenceManager.getDefaultSharedPreferences(Livestream.this), mWZBroadcastConfig);

                            sGoCoderSDK.startCameraPreview(mWZBroadcastConfig);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    //textchange
                                    mTxtFrameSize.setText(WZMediaConfig.DEFAULT_VIDEO_FRAME_HEIGHT + "x" + WZMediaConfig.DEFAULT_VIDEO_FRAME_WIDTH);
                                    //  mGestureDetector = new GestureDetectorCompat(CameraActivity.this, new FocusGestureListener());
                                    //textchange

                                }
                            });

                        }else{
                            toastsettext("No Internet Connection");
                        }

                    } else if (position == 1) {
                        Constant.cd = new ConnectionDetector(getApplicationContext());
                        Constant.isInternetPresent = Constant.cd.isConnectingToInternet();
                        if (Constant.isInternetPresent) {
                            getprofile("https://u2mrf96skg.execute-api.ap-southeast-1.amazonaws.com/mobiro/profile/settings?pname=low");
                        }else{
                            toastsettext("No Internet Connection");
                        }
                    } else if (position == 2) {


                        Constant.cd = new ConnectionDetector(getApplicationContext());
                        Constant.isInternetPresent = Constant.cd.isConnectingToInternet();
                        if (Constant.isInternetPresent) {
                            getprofile("https://u2mrf96skg.execute-api.ap-southeast-1.amazonaws.com/mobiro/profile/settings?pname=medium");
                        }else{
                            toastsettext("No Internet connection");
                        }
                    } else if (position == 3) {
                        Constant.cd = new ConnectionDetector(getApplicationContext());
                        Constant.isInternetPresent = Constant.cd.isConnectingToInternet();
                        if (Constant.isInternetPresent) {
                            getprofile("https://u2mrf96skg.execute-api.ap-southeast-1.amazonaws.com/mobiro/profile/settings?pname=high");
                        }else{
                            toastsettext("No Internet Connection");
                        }
                    } else if (position == 4) {
                        Constant.cd = new ConnectionDetector(getApplicationContext());
                        Constant.isInternetPresent = Constant.cd.isConnectingToInternet();
                        if (Constant.isInternetPresent) {
                            getprofile("https://u2mrf96skg.execute-api.ap-southeast-1.amazonaws.com/mobiro/profile/settings?pname=HD%20720");

                        }else{
                            toastsettext("No Internet Connection");
                        }
                    } else if (position == 5) {
                        Constant.cd = new ConnectionDetector(getApplicationContext());
                        Constant.isInternetPresent = Constant.cd.isConnectingToInternet();
                        if (Constant.isInternetPresent) {

                            getprofile("https://u2mrf96skg.execute-api.ap-southeast-1.amazonaws.com/mobiro/profile/settings?pname=HD 1080");
                        }else{

                        }
                    }else if(position==loginDataBaseAdapter.getcountofval(6)){
                        //Livestream.this.finish();
                        popupWindow.dismiss();
                        Intent inten =new Intent(getApplicationContext(),Neweditprofile.class);
                        startActivity(inten);
                    }else{
                        getdatabasevalues(position);
                    }


                }
            });


            if(position!=Constant.profilenames.length) {
                label.setText(Constant.profilenames[position]);
            }else{

            }
            Typeface type = Typeface.createFromAsset(getAssets(),"fonts/Trebuchet MS.ttf");
            label.setTypeface(type);



            ImageView icon=(ImageView)row.findViewById(R.id.profile_delete);





            if (position>5){
                if(position==loginDataBaseAdapter.getcountofval(6)){
                    icon.setVisibility(View.VISIBLE);
                    icon.setBackgroundResource(R.drawable.addprofile);
                }else {
                    icon.setVisibility(View.VISIBLE);
                }
            }
            else{
                icon.setVisibility(View.GONE);
            }


            icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(position==loginDataBaseAdapter.getcountofval(6)) {

                    }else {

                        new AlertDialog.Builder(Livestream.this)
                                .setTitle("Mobiro")
                                .setMessage("Do you want to remove this Profile?")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        popupWindow.dismiss();
                                        // continue with delete


                                       // mTxtFrameSize.callOnClick();
                                        loginDataBaseAdapter = new LoginDataBaseAdapter(Livestream.this);
                                        loginDataBaseAdapter = loginDataBaseAdapter.open();
                                        loginDataBaseAdapter.deleteprofile(Constant.profilenames[position],position);

                                        toastsettext("Profile Deleted");

                                        Constant.profilenames = new String[loginDataBaseAdapter.getcountofval(6) + 1];
                                        for (int i = 0; i <= loginDataBaseAdapter.getcountofval(6) + 1; i++) {
                                            if (i != loginDataBaseAdapter.getcountofval(6) + 1) {
                                                if (i == loginDataBaseAdapter.getcountofval(6)) {
                                                    Constant.profilenames[i] = "Add Profile";
                                                } else {
                                                    Constant.profilenames[i] = String.valueOf(loginDataBaseAdapter.getprofilename(String.valueOf(i + 1)));
                                                }
                                            } else {

                                            }
                                        }


                                        /*MyCustomAdapter mycustom = new MyCustomAdapter(Livestream.this, R.layout.profile_listitem, Constant.profilenames);


                                        mycustom.notifyDataSetChanged();
                                        spnr.setAdapter(mycustom);*/


                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                        dialog.dismiss();
                                        dialog.cancel();
                                        popupWindow.dismiss();
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();


                    }


                }
            });

            return row;
        }
    }




    private class MyAsyncTask extends AsyncTask<Void, Void, Void>
    {
        @Override
        protected Void doInBackground(Void... params) {
            //hostPhoto();
            getlocations();


            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            //post(text+" "+link);
            Log.d("asyncrslt",String.valueOf(result));

        }
    }
}
