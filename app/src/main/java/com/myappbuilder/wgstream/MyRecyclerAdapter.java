package com.myappbuilder.wgstream;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by nua-android on 30/5/16.
 * This code and all components (c) Copyright 2016-2017, NuaTransMedia,. All rights reserved.
 */

public class MyRecyclerAdapter extends RecyclerView.Adapter<MyCustomViewHolder> {

    private List<ListItem> listItems, filterList;
    private Context mContext;
    private boolean mapnull=false;

    public MyRecyclerAdapter(Context context, List<ListItem> listItems) {
        this.listItems = listItems;
        this.mContext = context;
        this.filterList = new ArrayList<ListItem>();

        // we copy the original list to the filter list and use it for setting row values
        this.filterList.addAll(this.listItems);
    }

    @Override
    public MyCustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.video_item, null);
        MyCustomViewHolder viewHolder = new MyCustomViewHolder(view);
        return viewHolder;

    }



    @Override
    public void onBindViewHolder(final MyCustomViewHolder customViewHolder, final int position) {




        final ListItem listItem = filterList.get(position);
        String sub_description =listItem.desc;
        if(listItem.desc.length()>70) {
            sub_description = listItem.desc.substring(0, 70);
           // customViewHolder.moreclick.setVisibility(View.VISIBLE);
        }else{
            sub_description =listItem.desc;
           // customViewHolder.moreclick.setVisibility(View.GONE);
        }
        Typeface type = Typeface.createFromAsset(mContext.getAssets(),"fonts/Trebuchet MS.ttf");
        customViewHolder.tvName.setTypeface(type);
        customViewHolder.tvPlace.setTypeface(type);
        customViewHolder.date.setTypeface(type);
        customViewHolder.time.setTypeface(type);
        customViewHolder.size.setTypeface(type);


        int currentVersion = android.os.Build.VERSION.SDK_INT;
        if (currentVersion >= Build.VERSION_CODES.M) {
            customViewHolder.city.setText("");
        }else {

            if (!(position >= Constant.locationlist.size())) {
                Log.d("arraysize", String.valueOf(Constant.locationlist.size()));
                Log.d("arrayvalue", String.valueOf(Constant.locationlist));
                customViewHolder.city.setText(Constant.locationlist.get(position).get("vidlocation"));
                // customViewHolder.city.setText(Constant.location.get(position));
            } else {
                customViewHolder.city.setText("Not Available");
            }
        }


        if(listItem.title.length()!=0) {
            customViewHolder.tvName.setText(listItem.title);
        }else{
            customViewHolder.tvName.setText(listItem.vidname);
        }
        customViewHolder.tvPlace.setText(listItem.desc);
        String kbmb;
        String time = null;

        Log.d("sizedur",listItem.vidsize+","+listItem.viddur);

        if(String.valueOf(listItem.vidsize).equals("null")){

        }else {
            kbmb = humanReadableByteCount(Long.parseLong(listItem.vidsize), true);
        }
        if(String.valueOf(listItem.viddur).equals("null")){

        }else {
            time = convertSecondsToHMmSs(Long.parseLong(listItem.viddur));
        }



        customViewHolder.size.setText(time);
        /*if(Constant.more_bool==true){
            customViewHolder.tvPlace.setText(listItem.place);
        }else {
            customViewHolder.tvPlace.setText(sub_description);

        }*/






        if(listItem.viddate.contains(" ")){
            String[] separate = listItem.viddate.split(" ");
           String viddate =separate[0];
            String vidtime=separate[1];


            SimpleDateFormat fromUser = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat myFormat = new SimpleDateFormat("dd-MM-yyyy");

            String reformattedStr=null;
            //  Toast.makeText(getActivity().getApplicationContext(),"date"+String.valueOf(reformattedStr),Toast.LENGTH_SHORT).show();

            try {
                reformattedStr = myFormat.format(fromUser.parse(viddate));
            } catch (ParseException e) {
                e.printStackTrace();
            }


            if(reformattedStr!=null) {
                customViewHolder.date.setText(reformattedStr);
            }else{
                customViewHolder.date.setText(viddate);
            }


        }else{

        }


        customViewHolder.time.setText(listItem.time);
        Picasso.with(mContext).load(listItem.thumb).into(customViewHolder.imgThumb);

        customViewHolder.tvPlace.setText(sub_description + "...");

        customViewHolder.less.setVisibility(View.GONE);
        customViewHolder.moreless.setVisibility(View.VISIBLE);





        customViewHolder.video_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String urlvideo = listItem.vidurl;//Constant.videolist.get(position).get("vidurl");
                Intent inten = new Intent(mContext, Videoview.class);
                inten.putExtra("videourl", urlvideo);
                mContext.startActivity(inten);
            }
        });

        customViewHolder.detail_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String videoid= listItem.vidid;//Constant.videolist.get(position).get("videoid");
                String videotitle=listItem.title;//Constant.videolist.get(position).get("vidtitle");
                String videodesc=listItem.desc;//Constant.videolist.get(position).get("viddesc");
                String videotag=listItem.vidtag;//Constant.videolist.get(position).get("vidtag");
                String videodate=listItem.viddate;//Constant.videolist.get(position).get("date");
                String videothumb=listItem.thumb;//Constant.videolist.get(position).get("thumb");
                String urlvideo = listItem.vidurl;
                String videoname=listItem.vidname;//Constant.videolist.get(position).get("vidurl");
                String vidlat=listItem.vidlat;
                String vidlong=listItem.vidlong;

                String kbmb=humanReadableByteCount(Long.parseLong(listItem.vidsize),true);
                String time=convertSecondsToHMmSs(Long.parseLong(listItem.viddur));

                Intent inten=new Intent(mContext,Video_detail.class);
                inten.putExtra("videoid", videoid);
                inten.putExtra("vidtitle",videotitle);
                inten.putExtra("viddesc",videodesc);
                inten.putExtra("vidtag",videotag);
                inten.putExtra("viddate",videodate);
                inten.putExtra("vidthumb",videothumb);
                inten.putExtra("vidurl",urlvideo);
                inten.putExtra("vidname",videoname);
                inten.putExtra("viddur",time);
                inten.putExtra("vidsize",kbmb);
                inten.putExtra("vidlat",vidlat);
                inten.putExtra("vidlong",vidlong);
                mContext.startActivity(inten);
            }
        });

        customViewHolder.editclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String videoid= listItem.vidid;//Constant.videolist.get(position).get("videoid");
                String videotitle=listItem.title;//Constant.videolist.get(position).get("vidtitle");
                String videodesc=listItem.desc;//Constant.videolist.get(position).get("viddesc");
                String videotag=listItem.vidtag;
                String videotype=listItem.vidtype;//Constant.videolist.get(position).get("vidtag");
               // ((Activity)mContext).finish();
                Intent inten=new Intent(mContext,Videoedit.class);
                inten.putExtra("videoid", videoid);
                inten.putExtra("vidtitle",videotitle);
                inten.putExtra("viddesc",videodesc);
                inten.putExtra("vidtag",videotag);
                inten.putExtra("vidtype",videotype);
                mContext.startActivity(inten);

            }
        });

        customViewHolder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String videoid= listItem.vidid;//Constant.videolist.get(position).get("videoid");
                String videotitle=listItem.title;//Constant.videolist.get(position).get("vidtitle");
                String videodesc=listItem.desc;//Constant.videolist.get(position).get("viddesc");
                String videotag=listItem.vidtag;
                String videotype=listItem.vidtype;//Constant.videolist.get(position).get("vidtag");
                //((Activity)mContext).finish();

                Intent inten=new Intent(mContext,Videoedit.class);
                inten.putExtra("videoid", videoid);
                inten.putExtra("vidtitle",videotitle);
                inten.putExtra("viddesc",videodesc);
                inten.putExtra("vidtag",videotag);
                inten.putExtra("vidtype",videotype);
                mContext.startActivity(inten);
            }
        });








    }


    public static String convertSecondsToHMmSs(long seconds) {
        long s = seconds % 60;
        long m = (seconds / 60) % 60;
        long h = (seconds / (60 * 60)) % 24;
        return String.format("%02d:%02d:%02d", h,m,s);
    }


    public static String humanReadableByteCount(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp-1) + (si ? "" : "i");
        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }

    @Override
    public int getItemCount() {
        return (null != filterList ? filterList.size() : 0);
    }

    public void filter(final String text) {

        // Searching could be complex..so we will dispatch it to a different thread...
        new Thread(new Runnable() {
            @Override
            public void run() {

                // Clear the filter list
                filterList.clear();

                // If there is no search value, then add all original list items to filter list
                if (TextUtils.isEmpty(text)) {

                    filterList.addAll(listItems);

                } else {
                    // Iterate in the original List and add it to filter list...
                    for (ListItem item : listItems) {

                        //filterList.add(item);
                        if (((item.title).toString()).toLowerCase().contains(text.toLowerCase()) ||
                                ((item.desc).toString()).toLowerCase().contains(text.toLowerCase()) || ((item.vidtype).toString()).toLowerCase().contains(text.toLowerCase())
                                || ((item.vidtag).toString()).toLowerCase().contains(text.toLowerCase())) {
                            // Adding Matched items
                            filterList.add(item);
                        }
                    }
                }

                // Set on UI Thread
                ((Activity) mContext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // Notify the List that the DataSet has changed...
                        notifyDataSetChanged();
                    }
                });

            }
        }).start();

    }



}
