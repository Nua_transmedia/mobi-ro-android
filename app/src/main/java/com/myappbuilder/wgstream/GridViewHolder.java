package com.myappbuilder.wgstream;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
/**
 * Created by nua-android on 30/5/16.
 * This code and all components (c) Copyright 2016-2017, NuaTransMedia,. All rights reserved.
 */
public class GridViewHolder extends RecyclerView.ViewHolder {

    protected ImageView gridimage;
    protected TextView gridtxt,time,duration,date,griddesc;
    protected RelativeLayout rel_cntin;


    public GridViewHolder(View view) {
        super(view);
        this.gridimage = (ImageView) view.findViewById(R.id.gridimage);
        this.gridtxt=(TextView) view.findViewById(R.id.grid_title);
        this.time=(TextView) view.findViewById(R.id.time);
        this.duration=(TextView) view.findViewById(R.id.duration);
        this.date=(TextView) view.findViewById(R.id.timehours);
        this.rel_cntin=(RelativeLayout) view.findViewById(R.id.rel_cntn);
        this.griddesc=(TextView) view.findViewById(R.id.grid_desc);

    }

}